#include "assetdb.h"
#include "texturecompiler.h"
#include "meshcompiler.h"
#include "shadercompiler.h"

Buffer ImportPNG(Buffer file)
{

    Texture texture = CompileTexture(file);
    Buffer_Free(&file);

    TextureHeader header =
    {
        .width = texture.width,
        .height = texture.height,
        .format = texture.format,
        .filterMode = texture.filterMode,
        .size = texture.buffer.size,
    };

    Stream stream = Stream_Create(GetAllocInfo(sizeof(TextureHeader) + texture.buffer.size));
    Stream_Write(&stream, ToBuffer(header));
    Stream_Write(&stream, texture.buffer);

    Buffer_Free(&texture.buffer);

    return stream.buffer;
}

Buffer ImportFBX(Buffer fileBuffer)
{
    Mesh mesh = CompileMesh(fileBuffer);
    Buffer_Free(&fileBuffer);

    MeshHeader header = {
        .vertexCount = mesh.vertexCount,
        .indexCount = mesh.indexCount,
        .channels = mesh.channels,
    };

    Buffer output = BUFFER_COMBINE(
        ToBuffer(header),
        mesh.vertexBuffer,
        mesh.uv1Buffer,
        mesh.normalBuffer,
        mesh.indexBuffer);

    Mesh_Destroy(&mesh);

    return output;
}

Buffer ImportShader(Buffer fileBuffer, ShaderCompileTarget target)
{
    Buffer vertexShader, fragmentShader;
    ShaderConstantLayout vertexInfo, fragmentInfo;

    CompileShader(fileBuffer, target, ShaderType_Vertex, &vertexShader, &vertexInfo);
    CompileShader(fileBuffer, target, ShaderType_Fragment, &fragmentShader, &fragmentInfo);

    Buffer_Free(&fileBuffer);

    ShaderHeader header = {
        .vertexShaderLength = vertexShader.size,
        .fragmentShaderLength = fragmentShader.size,
    };

    Buffer output = BUFFER_COMBINE(
        ToBuffer(header),
        ToBuffer(vertexInfo),
        ToBuffer(fragmentInfo),
        vertexShader,
        fragmentShader);

    Buffer_Free(&vertexShader);
    Buffer_Free(&fragmentShader);

    //PrintBuffer(compiledShader);

    return output;
}
