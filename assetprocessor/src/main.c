#include <stdio.h>
#include <stdlib.h>
#include "filesystem.h"
#include "assetprocessor.h"

typedef struct AssetProcessorParams_t
{
    ShaderCompileTarget shaderCompileTarget;

} AssetProcessorParams;

typedef struct AssetFile_t
{
    String filePath;
    String assetName;
} AssetFile;

Buffer ImportAsset(String fileName, const AssetProcessorParams params)
{
    Buffer fileBuffer;
    File_ReadAll(fileName, &fileBuffer);

    String fileExt = Path_GetExtension(fileName);

    Buffer retBuffer;

    if (UString_Equals(fileExt, UString("png")))
    {  
        DebugFormat("Load PNG %s", fileName.str);
        retBuffer = ImportPNG(fileBuffer);
    }
    else if (UString_Equals(fileExt, UString("fbx")))
    {
        DebugFormat("Load FBX %s", fileName.str);
        retBuffer = ImportFBX(fileBuffer);
    }
    else if (UString_Equals(fileExt, UString("shader")))
    {
        DebugFormat("Compile shader %s", fileName.str);
        retBuffer = ImportShader(fileBuffer, params.shaderCompileTarget);
    }
    else
    {
        retBuffer = fileBuffer;
        ASSERT(0, "Unknown file extension '%s'", fileExt.str);
    }

    UString_Free(&fileExt);

    return retBuffer;
}

void AssetDB_Create(String assetdbFileName, String headerFileName, AssetFile* files, int count, const AssetProcessorParams params)
{
    DebugFormat("Creating asset database to file %s %s", assetdbFileName.str, headerFileName.str);

    File_Clear(headerFileName);

    AssetDBHeader header;
    header.assetCount = count;
    int headerSize = sizeof(uint32) + header.assetCount * sizeof(AssetInfo);
    header.assets = Memory_Alloc(header.assetCount * sizeof(AssetInfo));

    Buffer compressionBuffer = { 0 };
    Buffer fileBuffer = { 0 };
    ZSTD_CCtx* zstd = ZSTD_createCCtx();
    long location = headerSize;

    for (int i = 0; i < count; ++i)
    {
        String filePath = files[i].filePath;

        Buffer importedBuffer = ImportAsset(filePath, params);

        size_t zSize = ZSTD_compressBound(importedBuffer.size);
        if (zSize > compressionBuffer.size)
            compressionBuffer = Buffer_Resize(compressionBuffer, GetAllocInfo((uint32)zSize));

        size_t compressedSize = ZSTD_compressCCtx(zstd, compressionBuffer.data, compressionBuffer.size, importedBuffer.data, importedBuffer.size, 1);

        File_Write(assetdbFileName, location, compressionBuffer.data, (uint32)compressedSize);
        header.assets[i].location = location;
        header.assets[i].sizeBytes = (uint32)compressedSize;

        DebugFormat("Asset location %i, Compressed %i => %i", location, importedBuffer.size, compressedSize);

        location += (int)compressedSize;

        Buffer_Free(&importedBuffer);

        String assetName = files[i].assetName;
        UString_Replace(assetName, '.', '_');
        //UString_Replace(fileName, '/', '_');
        //UString_Replace(fileName, '\\', '_');
        File_AppendFormat(headerFileName, "const uint32 Asset_%s = %i;\n", assetName.str, i);
    }

    ZSTD_freeCCtx(zstd);

    Buffer_Free(&compressionBuffer);

    File_Write(assetdbFileName, 0, &header.assetCount, sizeof(uint32));
    File_Write(assetdbFileName, sizeof(uint32), header.assets, header.assetCount * sizeof(AssetInfo));

    Memory_Free(header.assets);
}

void AssetDB_CreateFromFile(String fileName, const AssetProcessorParams params)
{
    String directory = Path_GetDirectory(fileName);
    DebugFormat("Directory: %s", directory.str);

    Buffer fileList;
    File_ReadAll(fileName, &fileList);

    unsigned int count = 0;

    for (unsigned int i = 0; i < fileList.size; ++i)
        if (fileList.data[i] == '\n')
            count++;

    DebugFormat("Count: %i", count);
    AssetFile* files = STACK_ARRAY(AssetFile, count);

    unsigned int lineStart = 0;
    unsigned int index = 0;
    int lineEnd = 0;
    for (unsigned int i = 0; i < fileList.size; ++i)
    {
        if (lineEnd)
        {
            if (fileList.data[i] == ' ' || fileList.data[i] == '\n' || fileList.data[i] == '\r' || fileList.data[i] == '\t')
                continue;
            else
            {
                lineEnd = 0;
                lineStart = i;
            }
        }
        if (fileList.data[i] == '\r' || fileList.data[i] == '\n')
        {
            files[index].assetName = UString_CreateFormat("%.*s", i - lineStart, fileList.data + lineStart);
            files[index].filePath = UString_CreateFormat("%s%s", directory.str, files[index].assetName.str);
            DebugFormat("%s %s", files[index].assetName.str, files[index].filePath.str);
            index++;
            lineEnd = 1;
        }
    }

    Buffer_Free(&fileList);

    String assetdbFileName = Path_ChangeExtension(fileName, UString("zstd"));
    String headerFileName = Path_ChangeExtension(fileName, UString("h"));

    AssetDB_Create(assetdbFileName, headerFileName, files, count, params);

    UString_Free(&assetdbFileName);
    UString_Free(&headerFileName);

    for (unsigned int i = 0; i < count; ++i)
    {
        //DebugFormat("%s %i %i", files[i].str, files[i].length, files[i].buffer.size);
        UString_Free(&(files[i].assetName));
        UString_Free(&(files[i].filePath));
    }

    UString_Free(&directory);
}

void PrintHelp()
{
    DebugFormat("assetprocessor <target> <file1> <file2> ...");
    DebugFormat("Targets:");
    DebugFormat("    opengl");
    DebugFormat("    d3d11");
}

int main(int argc, char** argv)
{
    if(argc <= 1)
    {
        DebugFormat("No arguments given");
        PrintHelp();
        return 1;
    }

    AssetProcessorParams params = { 0 };
    String target = UString_Cstr(argv[1]);

    if(UString_Equals(target, UString("opengl")))
    {
        params.shaderCompileTarget = ShaderCompileTarget_OpenGL;
    }
    else if(UString_Equals(target, UString("d3d11")))
    {
        params.shaderCompileTarget = ShaderCompileTarget_D3D11;
    }
    else
    {
        DebugFormat("Unknown target %s", target.str);
        PrintHelp();
        return 1;
    }

    if(argc <= 2)
    {
        DebugFormat("No input files given");
        PrintHelp();
        return 1;
    }

    String str;
    for (int i = 2; i < argc; ++i)
    {
        str.str = argv[i];
        str.length = (uint32)strlen(str.str);
        str.buffer.size = str.length + 1;
        AssetDB_CreateFromFile(str, params);
    }
    //AssetDB_CreateFromFile(UString("triangle.txt"), params);
    //AssetDB_CreateFromFile(UString("texture.txt"), params);
    //AssetDB_CreateFromFile(UString("assets.txt"));

    DebugFormat("Completed");
    //getchar();
    return 0;
}

#include "print.c"
#include "string.c"
#include "util.c"
#include "memory.c"
#include "math.c"
#include "mesh.c"
#include "filesystem.c"
#include "platform/windows/platform_windows.c"