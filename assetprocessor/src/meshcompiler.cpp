#include "meshcompiler.h"
#include "math2.h"
#include "fbx\ofbx.h"
#include <stdio.h>
#include "util.h"
#include "mesh.h"

Mesh CompileMesh(Buffer buffer)
{
    Mesh mesh = { 0 };

    ofbx::IScene* scene = ofbx::load((ofbx::u8*)buffer.data, buffer.size);

    int meshCount = scene->getMeshCount();

    printf("meshCount %i\n", meshCount);

    for (int i = 0; i < meshCount; ++i)
    {
        const ofbx::Mesh* fbxmesh = scene->getMesh(i);
    
        const ofbx::Geometry* geom = fbxmesh->getGeometry();
        ofbx::Vec3 scale = fbxmesh->getLocalScaling();
        //printf("mesh scale %f %f %f\n", scale.x, scale.y, scale.z);
        scale = geom->getLocalScaling();
        //printf("geom scale %f %f %f\n", scale.x, scale.y, scale.z);
    
        //printf("name %s\n", fbxmesh->name);
    
        //ofbx::Matrix matrix = fbxmesh->getGeometricMatrix();
        //double* mat = fbxmesh->getGeometricMatrix().m;
        //printf("transform\n%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n%f %f %f %f\n",
        //    mat[0], mat[1], mat[2], mat[3],
        //    mat[4], mat[5], mat[6], mat[7],
        //    mat[8], mat[9], mat[10], mat[11],
        //    mat[12], mat[13], mat[14], mat[15]);
    
        int vertexCount = geom->getVertexCount();
        const ofbx::Vec3* vertices = geom->getVertices();
        const ofbx::Vec2* uv1s = geom->getUVs(0);
        const ofbx::Vec3* normals = geom->getNormals();
        Vector3 pos;
        Vector2 uv1;
        Vector3 normal;

        mesh.channels = MeshChannel_Position;
        if (uv1s != NULL)
            mesh.channels |= MeshChannel_UV1;
        if (normals != NULL)
            mesh.channels |= MeshChannel_Normal;

        for (int j = 0; j < vertexCount; ++j)
        {
            pos = { (float)vertices[j].x, (float)vertices[j].y, (float)vertices[j].z };
            if(uv1s != NULL)
                uv1 = { (float)uv1s[j].x, 1.0f - (float)uv1s[j].y };
            if (normals != NULL)
                normal = { (float)normals[j].x, (float)normals[j].y, (float)normals[j].z };

            uint16 index;
            for (index = 0; index < mesh.vertexCount; ++index)
            {
                if (Vector3Length(Vector3Subtract(pos, mesh.vertices[index])) > 0.0000001f)
                    continue;
                if (uv1s != NULL && Vector2Length(Vector2Subtract(uv1, mesh.uv1[index])) > 0.0000001f)
                    continue;
                if (normals != NULL && Vector3Length(Vector3Subtract(normal, mesh.normals[index])) > 0.0000001f)
                    continue;
                break;
            }

            Buffer_AddGrow(&mesh.indexBuffer, mesh.indexCount, ToBuffer(index));
            mesh.indexCount++;

            if (index == mesh.vertexCount)
            {
                Buffer_AddGrow(&mesh.vertexBuffer, mesh.vertexCount, ToBuffer(pos));
                if(uv1s != NULL)
                    Buffer_AddGrow(&mesh.uv1Buffer, mesh.vertexCount, ToBuffer(uv1));
                if (normals != NULL)
                    Buffer_AddGrow(&mesh.normalBuffer, mesh.vertexCount, ToBuffer(normal));
                mesh.vertexCount++;
            }

        }
        
        break;
    }

    Mesh_Trim(&mesh);

    printf("vertices: %i indices: %i\n", mesh.vertexCount, mesh.indexCount);

    Vector3 min = { 100000, 100000, 100000 };
    Vector3 max = { -100000, -100000, -100000 };
    for (int i = 0; i < mesh.vertexCount; ++i)
    {
        Vector3 v = mesh.vertices[i];
        if (v.x < min.x)
            min.x = v.x;
        if (v.y < min.y)
            min.y = v.y;
        if (v.z < min.z)
            min.z = v.z;
        if (v.x > max.x)
            max.x = v.x;
        if (v.y > max.y)
            max.y = v.y;
        if (v.z > max.z)
            max.z = v.z;
    }

    printf("bounds: %f %f %f\n", max.x - min.x, max.y - min.y, max.z - min.z);

    Vector3 center = Vector3Multiply(Vector3Add(min, max), 0.5f);

    for (int i = 0; i < mesh.vertexCount; ++i)
    {
        mesh.vertices[i] = Vector3Subtract(mesh.vertices[i], center);
    }

    return mesh;
}
