#ifndef _MESHCOMPILER_H_
#define _MESHCOMPILER_H_

#include "memory.h"
#include "mesh.h"

CAPI_START

Mesh CompileMesh(Buffer buffer);

CAPI_END

#endif