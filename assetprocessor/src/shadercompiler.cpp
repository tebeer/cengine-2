#include "shader.h"
#include "shadercompiler.h"
#include "hlslcc.h"
#include <assert.h>
#include <d3dcompiler.h>


#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxguid.lib")

int CompileD3D(Buffer buffer, ID3DBlob** output, ShaderType type)
{
    LPCSTR target = NULL;
    LPCSTR entry = NULL;
    if (type == ShaderType_Vertex)
    {
        target = "vs_5_0";
        entry = "vert";
    }
    else if (type == ShaderType_Fragment)
    {
        target = "ps_5_0";
        entry = "frag";
    }
    else
        assert(0 && "Unknown ShaderType");

    ID3DBlob* errors = NULL;

    HRESULT result = D3DCompile(
        buffer.data,    // LPCVOID pSrcData
        buffer.size,    // SIZE_T SrcDataSize
        NULL,           // LPCSTR pSourceName
        NULL,           // CONST D3D_SHADER_MACRO* pDefines
        NULL,           // ID3DInclude* pInclude
        entry,          // LPCSTR pEntrypoint
        target,         // LPCSTR pTarget,
        NULL,           // UINT Flags1,
        NULL,           // UINT Flags2,
        output,         // ID3DBlob** ppCode,
        &errors);       // ID3DBlob** ppErrorMsgs
    
    if (result != S_OK)
    {
        printf("Failed to compile shader: %i\n", result);
        if (errors)
        {
            printf("Errors:\n%s\n", errors->GetBufferPointer());
            
        }
        return 0;
    }

    if (errors)
    {
        printf("Errors:\n");
        errors->Release();
    }

    printf("Compile successful\n");

    return 1;
}

void GetLocation(ID3D11ShaderReflection* reflection, const char* name)
{
    ID3D11ShaderReflectionVariable* var = reflection->GetVariableByName(name);
    D3D11_SHADER_VARIABLE_DESC desc = { 0 };
    var->GetDesc(&desc);

    printf("LOC: %i, NAME: %s\n", desc.StartOffset, desc.Name);
}

void CompileShader(Buffer buffer, ShaderCompileTarget target, ShaderType type, Buffer* output, ShaderConstantLayout* info)
{
    ID3DBlob* compiled;

    int compileResult = CompileD3D(buffer, &compiled, type);

    assert(compileResult && "Failed to compile shader");

    //ID3D11ShaderReflection* reflection;
    //D3DReflect(compiled->GetBufferPointer(), compiled->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&reflection);
    //D3D11_SHADER_DESC shaderDesc;
    //reflection->GetDesc(&shaderDesc);
    //
    //for (UINT i = 0; i < shaderDesc.ConstantBuffers; ++i)
    //{
    //    ID3D11ShaderReflectionConstantBuffer* cbuf = reflection->GetConstantBufferByIndex(i);
    //    D3D11_SHADER_BUFFER_DESC cbufDesc;
    //    cbuf->GetDesc(&cbufDesc);
    //
    //    D3D11_SHADER_INPUT_BIND_DESC bindDesc;
    //    reflection->GetResourceBindingDescByName(cbufDesc.Name, &bindDesc);
    //
    //    printf("CBUFFER: %i NAME: %s BINDING %i\n", i, cbufDesc.Name, bindDesc.BindPoint);
    //
    //    for (UINT j = 0; j < cbufDesc.Variables; ++j)
    //    {
    //        ID3D11ShaderReflectionVariable* var = cbuf->GetVariableByIndex(j);
    //        D3D11_SHADER_VARIABLE_DESC varDesc;
    //        var->GetDesc(&varDesc);
    //        printf("    VAR: %i NAME: %s OFFSET: %i SIZE: %i\n", j, varDesc.Name, varDesc.StartOffset, varDesc.Size);
    //    }
    //}
    //for (UINT i = 0; i < shaderDesc.BoundResources; ++i)
    //{
    //    D3D11_SHADER_INPUT_BIND_DESC bindDesc;
    //    reflection->GetResourceBindingDesc(i, &bindDesc);
    //
    //    printf("BINDING: %i NAME: %s BindPoint: %i\n", i, bindDesc.Name, bindDesc.BindPoint);
    //}

    //GetLocation(reflection, "worldMatrix");
    //GetLocation(reflection, "viewMatrix");
    //GetLocation(reflection, "projectionMatrix");

    GlExtensions ext = { 0 };
    HLSLccSamplerPrecisionInfo precisions;
    HLSLccReflection reflections;
    GLSLShader result = { 0 };
    GLSLCrossDependencyData deps;
    
    int success = TranslateHLSLFromMem(
        (char*)compiled->GetBufferPointer(),
        HLSLCC_FLAG_DISABLE_GLOBALS_STRUCT,
        LANG_330,
        &ext,
        &deps,
        precisions,
        reflections,
        &result);
    
    if (!success)
    {
        printf("Failed to translate shader");
        assert(0 && "Failed to translate shader");
    }
    printf("shader type %i\n", result.shaderType);
    
    assert(result.reflection.psConstantBuffers.size() <= 1);

    *info = { 0 };

    for (uint8 i = 0; i < result.reflection.psInputSignatures.size(); ++i)
    {
        ShaderInfo::InOutSignature signature = result.reflection.psInputSignatures[i];
        printf("INPUT: %s %i\n", signature.semanticName.c_str(), signature.ui32Register);
    }

    if (result.reflection.psConstantBuffers.size() == 1)
    {
        ConstantBuffer buffer = result.reflection.psConstantBuffers[0];
        info->varCount = (uint8)buffer.asVars.size();
        printf("CBUFFER: %s %i\n", buffer.name.c_str(), buffer.ui32TotalSizeInBytes);
        for (uint8 i = 0; i < info->varCount; ++i)
        {
            ShaderVar var = buffer.asVars[i];
            strcpy_s(info->vars[i].name, 32, var.name.c_str());
            info->vars[i].location = var.ui32StartOffset;
            info->vars[i].size = var.ui32Size;
            printf("    VAR: %s %i %i\n", var.name.c_str(), var.ui32Size, var.ui32StartOffset);
        }
    }

    for (uint8 i = 0; i < result.reflection.psResourceBindings.size(); ++i)
    {
        ResourceBinding resource = result.reflection.psResourceBindings[i];
        if (resource.eType != ResourceType::RTYPE_TEXTURE)
            continue;
        uint32 resIndex = info->resourceCount++;
        strcpy_s(info->resources[resIndex].name, 32, resource.name.c_str());
        info->resources[resIndex].location = resource.ui32BindPoint;
        printf("RESOURCE: %s %i %i %i %i\n", resource.name.c_str(), resource.ui32BindPoint, resource.ui32BindCount, resource.ui32Space, resource.ui32RangeID);
    }
    
    if (target == ShaderCompileTarget_D3D11)
    {
        output->size = compiled->GetBufferSize();
        output->data = (unsigned char*)malloc(output->size);
        memcpy(output->data, compiled->GetBufferPointer(), output->size);
        compiled->Release();
    }
    else
    {
        compiled->Release();
        const char* str = result.sourceCode.c_str();
        output->size = strlen(str) + 1;
        output->data = (unsigned char*)malloc(output->size);
        memcpy(output->data, str, output->size);
    }
}