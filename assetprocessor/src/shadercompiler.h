#ifndef _SHADERCOMPILER_H_
#define _SHADERCOMPILER_H_

#include "memory.h"
//#include "gfx.h"

typedef enum
{
    ShaderCompileTarget_D3D11,
    ShaderCompileTarget_OpenGL,
} ShaderCompileTarget;

typedef enum
{
    ShaderType_Vertex,
    ShaderType_Fragment,
} ShaderType;

CAPI_START

void CompileShader(Buffer buffer, ShaderCompileTarget target, ShaderType type, Buffer* output, ShaderConstantLayout* info);

CAPI_END

#endif