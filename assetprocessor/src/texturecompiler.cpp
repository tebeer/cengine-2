#include "texturecompiler.h"
#include <cuttlefish/Image.h>
#include <cuttlefish/Texture.h>

#include <assert.h>
#include <string.h>
#include <stdio.h>

Texture CompileTexture(Buffer buffer)
{
    cuttlefish::Image image;
    bool success = image.load(buffer.data, buffer.size, cuttlefish::ColorSpace::sRGB);
    assert(success && "Failed to load image");

    printf ("Format: %i\n", image.format());
    

    auto texture = cuttlefish::Texture(cuttlefish::Texture::Dimension::Dim2D, image.width(), image.height(), 0, 1, cuttlefish::ColorSpace::sRGB);
    texture.setImage(image, 0, 0);
    
    success = texture.convert(
        cuttlefish::Texture::Format::R8G8B8A8,
        cuttlefish::Texture::Type::UNorm,
        cuttlefish::Texture::Quality::Normal,
        cuttlefish::Texture::Alpha::Standard,
        cuttlefish::Texture::ColorMask::ColorMask());

    assert(success && "Failed to convert texture");

    Texture tex;
    tex.buffer.size = texture.dataSize();
    tex.buffer.data = (unsigned char*)malloc(tex.buffer.size);
    memcpy(tex.buffer.data, (unsigned char*)texture.data(), tex.buffer.size);
    tex.format = TextureFormat_RGBA;
    tex.width = texture.width();
    tex.height = texture.height();
    tex.filterMode = TextureFilter_Bilinear;

    return tex;
}
