#ifndef _TEXTURECOMPILER_H_
#define _TEXTURECOMPILER_H_

#include "texture.h"
#include "memory.h"

CAPI_START

Texture CompileTexture(Buffer buffer);

CAPI_END

#endif