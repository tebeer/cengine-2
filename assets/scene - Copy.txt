shader standard
 vert shaders_gl/vert.shader
 frag shaders_gl/frag.shader

shader cutout
 vert shaders_gl/vert.shader
 frag shaders_gl/frag_cutout.shader

texture foliage_sheet-1 sceneassets/foliage_sheet-1.png

texture palm_bark-1 sceneassets/palm_bark-1.png

texture weapon_crate-1 sceneassets/weapon_crate-1.png

texture car-1 sceneassets/car-1.png

texture grass-1 sceneassets/grass-1.png

texture car-2 sceneassets/car-2.png

mesh foliage_dwarf_palm-1-bottom sceneassets/foliage_dwarf_palm-1-bottom.mesh

mesh foliage_dwarf_palm-1-Cylinder001 sceneassets/foliage_dwarf_palm-1-Cylinder001.mesh

mesh foliage_palm-1-Cylinder001 sceneassets/foliage_palm-1-Cylinder001.mesh

mesh foliage_palm-1-bottom sceneassets/foliage_palm-1-bottom.mesh

mesh weapon_crate-1-weapon_crate_1a sceneassets/weapon_crate-1-weapon_crate_1a.mesh

mesh weapon_crate-1-weapon_crate_rope_1 sceneassets/weapon_crate-1-weapon_crate_rope_1.mesh

mesh car-1-Panda sceneassets/car-1-Panda.mesh

mesh waypoint-waypoint_plane sceneassets/waypoint-waypoint_plane.mesh

mesh car-2-Hijet sceneassets/car-2-Hijet.mesh

object 0
 shaderref cutout
 textureref foliage_sheet-1
 meshref foliage_dwarf_palm-1-bottom

object 1
 shaderref standard
 textureref palm_bark-1
 meshref foliage_dwarf_palm-1-Cylinder001

object 2
 shaderref standard
 textureref palm_bark-1
 meshref foliage_palm-1-Cylinder001

object 3
 shaderref cutout
 textureref foliage_sheet-1
 meshref foliage_palm-1-bottom

object 4
 shaderref standard
 textureref weapon_crate-1
 meshref weapon_crate-1-weapon_crate_1a

object 5
 shaderref standard
 textureref weapon_crate-1
 meshref weapon_crate-1-weapon_crate_rope_1

object 6
 shaderref standard
 textureref car-1
 meshref car-1-Panda

object 7
 shaderref standard
 textureref grass-1
 meshref waypoint-waypoint_plane

object 8
 shaderref standard
 textureref car-2
 meshref car-2-Hijet

instance 0
 position -3.357917 -0.3768291 -2.195474
 rotation -0.3423121 -0.6187266 -0.6187266 0.3423121
 scale 1.049142 1.049142 1.049142

instance 1
 position -3.014788 -1.8439 -1.497675
 rotation -0.3423121 -0.6187266 -0.6187266 0.3423121
 scale 0.2818572 0.3763523 0.6987346

instance 2
 position -5.611628 -1.831403 -3.695821
 rotation -0.37201 0.5835158 0.5825268 0.4263572
 scale 0.7151473 0.715147 1.086827

instance 3
 position -4.279675 2.134775 -3.334891
 rotation -0.37201 0.5835158 0.5825268 0.4263572
 scale 1.084208 1.084208 1.084207

instance 3
 position 0.9711469 3.826491 -4.496638
 rotation -0.09229738 0.7010573 0.7010573 0.09229738
 scale 1.516063 1.516063 1.516063

instance 2
 position -0.131915 -1.8439 -3.442505
 rotation -0.09229738 0.7010573 0.7010573 0.09229738
 scale 1 1 1.519726

instance 4
 position -1.4 -1.8439 3.69
 rotation 0 -0.4450527 0 0.8955044
 scale 1.485984 1.485984 1.485984

instance 5
 position -1.4 -1.8439 3.69
 rotation 0 -0.4450527 0 0.8955044
 scale 1.485984 1.485984 1.485984

instance 4
 position -1.992 -1.845 3.115
 rotation 0 -0.326342 0 0.9452518
 scale 1.485984 1.485984 1.485985

instance 5
 position -1.992 -1.845 3.115
 rotation 0 -0.326342 0 0.9452518
 scale 1.485984 1.485984 1.485985

instance 4
 position -1.364 -1.467 3.599
 rotation 0 -0.3665789 0 0.930387
 scale 1.485984 1.485984 1.485985

instance 5
 position -1.364 -1.467 3.599
 rotation 0 -0.3665789 0 0.930387
 scale 1.485984 1.485984 1.485985

instance 4
 position -2.436 -1.62 2.387
 rotation 0.04248981 -0.9436433 0.1337719 0.2997284
 scale 1.485984 1.485984 1.485985

instance 5
 position -2.436 -1.62 2.387
 rotation 0.04248981 -0.9436433 0.1337719 0.2997284
 scale 1.485984 1.485984 1.485985

instance 6
 position -5.89 -2 5.8
 rotation 0.6463106 -0.2868496 -0.2868496 -0.6463106
 scale 1.212426 1.212426 1.212426

instance 6
 position -5.1 -1.92 2.44
 rotation 0.6801462 -0.193394 -0.1933939 -0.6801462
 scale 1 1 1

instance 6
 position 3.45 -2 4.4
 rotation 0.456121 0.5403274 0.5403274 -0.456121
 scale 1 1 1

instance 7
 position 0 -2 0
 rotation 0 0 0 1
 scale 10 10 10

instance 6
 position 0.42 -1.89 6.35
 rotation 0.2447876 0.6633846 0.6633844 -0.2447875
 scale 0.9999998 0.9999999 0.9999999

instance 6
 position 1.132 -1.886 0.657
 rotation 0.6332965 0.3145402 0.3145403 -0.6332965
 scale 1 1 1

instance 8
 position -2.66 -1.89 5.98
 rotation -0.6878239 -0.1640074 -0.1640073 0.6878238
 scale 1 1 1

