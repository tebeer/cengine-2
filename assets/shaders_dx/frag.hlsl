Texture2D shaderTexture;
SamplerState SampleType;

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD0;
    float4 color : COLOR;
};

float4 main(PixelInputType input) : SV_TARGET
{
    float4 tex = shaderTexture.Sample(SampleType, input.uv);

    return input.color * tex;
}
