
//cbuffer MatrixBuffer
//{
//    matrix worldMatrix;
//    matrix viewMatrix;
//    matrix projectionMatrix;
//};

struct VertexInputType
{
    float3 position : V_POSITION;
    float2 uv : V_UV;
    float4 color : V_COLOR;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD0;
    float4 color : COLOR;
};

PixelInputType main(VertexInputType input)
{
    PixelInputType output;
    

    // Change the position vector to be 4 units for proper matrix calculations.
    //input.position.w = 1.0f;

    // Calculate the position of the vertex against the world, view, and projection matrices.
    //output.position = mul(input.position, worldMatrix);
    //output.position = mul(output.position, viewMatrix);
    //output.position = mul(output.position, projectionMatrix);
    output.position = float4(input.position, 1);
    
    output.uv = input.uv;

    // Store the input color for the pixel shader to use.
    output.color = input.color;
    
    return output;
}
