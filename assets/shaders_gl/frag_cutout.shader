out vec4 FragColor;
    
in vec2 v2f_uv;
in vec3 v2f_color;

uniform sampler2D _MainTex;

void main()
{
    vec4 tex = texture(_MainTex, v2f_uv);
    if (tex.a < .5)
        discard;
        
    vec3 rgb = tex.rgb * v2f_color;
    rgb = mix(vec3(0.0, 0.03, 0.08), vec3(1.12, 1.03, .9), rgb);
    FragColor = vec4(rgb, 1);    
}
