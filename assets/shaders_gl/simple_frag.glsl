in vec2 v2f_uv;
in vec4 v2f_color;

uniform sampler2D _MainTex;

out vec4 FragColor;

void main()
{
    vec4 tex = texture(_MainTex, v2f_uv);
    FragColor = tex * v2f_color;
}
