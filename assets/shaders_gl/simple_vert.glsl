varying out vec2 v2f_uv;
varying out vec4 v2f_color;

in vec3 V_POSITION;
in vec2 V_UV;
in vec4 V_COLOR;

void main()
{
    v2f_uv = V_UV;
    v2f_color = V_COLOR;
    gl_Position = vec4(V_POSITION.xyz, 1.0);
}
