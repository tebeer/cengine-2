in vec3 in_pos; 
in vec2 in_uv;
in vec3 in_normal;

varying out vec2 v2f_uv;
varying out vec3 v2f_color;

void main()
{
    //gl_Position = mvpMatrix * vec4(in_pos, 1.0);
    
    mat4 mvp = projectionMatrix * viewMatrix * modelMatrix;
    
    gl_Position = mvp * vec4(in_pos, 1.0);
    
    v2f_uv = in_uv;
    
    vec3 worldNormal = normalize((modelMatrix * vec4(in_normal, 0.0)).xyz);
    vec3 lightDir = normalize(vec3(1, -1, -1));
    
    vec3 color = vec3(1,1,1);
    
    v2f_color = color * (.4 + 1.5*clamp(dot(-lightDir, worldNormal), 0, 1));
}
