matrix worldMatrix;
matrix viewMatrix;
matrix projectionMatrix;

Texture2D shaderTexture;
SamplerState SampleType;

struct VertInput
{
    float3 position : V_POSITION;
    float3 normal : V_NORMAL;
    float2 uv : V_UV;
};

struct FragInput
{
    float4 position : SV_POSITION;
    float3 color : COLOR;
    float2 uv : TEXCOORD0;
};

FragInput vert(VertInput input)
{
    FragInput output;
    
    float4 pos = float4(input.position.xyz, 1);

    pos = mul(pos, worldMatrix);
    pos = mul(pos, viewMatrix);
    pos = mul(pos, projectionMatrix);
    
    output.position = pos;
    output.uv = input.uv;
    
    float3 worldNormal = normalize(mul(float4(input.normal, 0), worldMatrix).xyz);
    float3 lightDir = normalize(float3(1, -1, 1));
    output.color = 1 * (1-dot(worldNormal, lightDir));
    
    return output;
}

float4 frag(FragInput input) : SV_TARGET
{
    float4 color = float4(input.color, 1);
    float4 tex = shaderTexture.Sample(SampleType, input.uv);
    color *= tex;
    return color;
}
