matrix worldMatrix;
matrix viewMatrix;
matrix projectionMatrix;

struct VertInput
{
    float3 position : V_POSITION;
};

struct FragInput
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

FragInput vert(VertInput input)
{
    FragInput output;
    
    float4 pos = float4(input.position.xyz, 1);

    pos = mul(pos, worldMatrix);
    pos = mul(pos, viewMatrix);
    pos = mul(pos, projectionMatrix);
    
    output.position = pos;
    output.color = pos.z / 20;
    
    return output;
}

float4 frag(FragInput input) : SV_TARGET
{
    return input.color;
}
