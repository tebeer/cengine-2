Texture2D shaderTexture;
SamplerState SampleType;

struct VertInput
{
    float3 position : V_POSITION;
    float2 uv : V_UV;
};

struct FragInput
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD0;
};

FragInput vert(VertInput input)
{
    FragInput output;
    
    output.position = float4(input.position, 1);
    output.uv = input.uv;
    
    return output;
}

float4 frag(FragInput input) : SV_TARGET
{
    float4 tex = shaderTexture.Sample(SampleType, input.uv);
    return tex;
}
