Texture2D shaderTexture;
SamplerState SampleType;

struct VertexInputType
{
    float3 position : V_POSITION;
    float4 color : V_COLOR;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

PixelInputType vert(VertexInputType input)
{
    PixelInputType output;

    output.position = float4(input.position, 1);
    output.color = input.color;
    
    return output;
}

float4 frag(PixelInputType input) : SV_TARGET
{
    return input.color;
}
