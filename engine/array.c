#include "array.h"
#include "memory.h"
#include <assert.h>

void Array_Resize_Generic(Array* arr, AllocInfo info)
{
    assert(arr->elementSize > 0);

    arr->length = info.bytes;

    info.bytes *= arr->elementSize;
    arr->buffer = Buffer_Resize(arr->buffer, info);
}

void Array_Free_Generic(Array* arr)
{
    Buffer_Free(&arr->buffer);
    *arr = (Array) { 0 };
}