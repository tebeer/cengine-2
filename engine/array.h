#ifndef _ARRAY_H_
#define _ARRAY_H_

#include "memory.h"

#define ArrayType(type) struct { union { Buffer buffer; type* data; }; unsigned int length; unsigned int elementSize; }
#define Array_Init(type) { .elementSize = sizeof(type) }
#define Array_Resize(arr, newLength) Array_Resize_Generic((Array*)&arr, GetAllocInfo(newLength))
#define Array_Free(arr) Array_Free_Generic((Array*)&arr)

typedef ArrayType(void) Array;

void Array_Resize_Generic(Array* arr, AllocInfo info);
void Array_Free_Generic(Array* arr);

#endif