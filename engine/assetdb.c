#include "assetdb.h"

AssetDBHeader AssetDB_LoadHeader(String fname)
{
    AssetDBHeader header;
    void* file = File_Open(fname);

    ASSERT(file != NULL, "Could not open file %s", fname.buffer.data);

    File_Read(file, 0, &header.assetCount, sizeof(uint32));

    int assetsSize = header.assetCount * sizeof(AssetInfo);
    header.assets = Memory_Alloc(assetsSize);
    File_Read(file, sizeof(uint32), header.assets, assetsSize);

    File_Close(file);

    return header;
}

Buffer AssetDB_LoadAsset(String fname, AssetInfo info)
{
    DebugFormat("AssetDB_LoadAsset %s %i %i", fname.buffer.data, info.location, info.sizeBytes);

    Buffer compressedBuffer = Buffer_Create(GetAllocInfo(info.sizeBytes));
    File_ReadSimple(fname, info.location, compressedBuffer.data, info.sizeBytes);

    uint32 size = (uint32)ZSTD_getFrameContentSize(compressedBuffer.data, compressedBuffer.size);
    ASSERT(size != ZSTD_CONTENTSIZE_ERROR, "%s: not compressed by zstd!", fname.buffer.data);
    ASSERT(size != ZSTD_CONTENTSIZE_UNKNOWN, "%s: original size unknown!", fname.buffer.data);

    Buffer buffer = Buffer_Create(GetAllocInfo(size));

    size_t const dSize = ZSTD_decompress(buffer.data, buffer.size, compressedBuffer.data, compressedBuffer.size);

    ASSERT(!ZSTD_isError(dSize), "%s", ZSTD_getErrorName(dSize));

    Buffer_Free(&compressedBuffer);

    return buffer;
}

Shader AssetDB_LoadShader(String fname, AssetInfo info)
{
    Buffer buffer = AssetDB_LoadAsset(fname, info);

    Shader shader = { 0 };

    uint32 pos = 0;

    ShaderHeader header = *(ShaderHeader*)(buffer.data + pos);
    pos += sizeof(ShaderHeader);

    shader.vertexConstantLayout = *(ShaderConstantLayout*)(buffer.data + pos);
    pos += sizeof(ShaderConstantLayout);

    shader.fragmentConstantLayout = *(ShaderConstantLayout*)(buffer.data + pos);
    pos += sizeof(ShaderConstantLayout);

    shader.vertexShader = (Buffer) {
        .size = header.vertexShaderLength,
            .data = (buffer.data + pos),
    };
    pos += shader.vertexShader.size;

    shader.fragmentShader = (Buffer) {
        .size = header.fragmentShaderLength,
        .data = (buffer.data + pos),
    };
    pos += shader.fragmentShader.size;

    //for (int i = 0; i < vertInfo.varCount; ++i)
    //{
    //    GLint loc = glGetUniformLocation(s_resourceCache.shaders[shaderId].program, vertInfo.vars[i].name);
    //    DebugFormat("%s %i", vertInfo.vars[i].name, loc);
    //}

    return shader;
}

Mesh AssetDB_LoadMesh(String fname, AssetInfo info)
{
    Buffer buffer = AssetDB_LoadAsset(fname, info);

    Mesh mesh = { 0 };

    uint32 pos = 0;
    MeshHeader header = *(MeshHeader*)(buffer.data + pos);
    DebugFormat("Load Mesh v: %i i: %i", header.vertexCount, header.indexCount);
    pos += sizeof(MeshHeader);

    mesh.vertexCount = header.vertexCount;
    mesh.indexCount = header.indexCount;
    mesh.channels = header.channels;

    mesh.vertexBuffer = Buffer_Create(GetAllocInfo(header.vertexCount * sizeof(Vector3)));
    Memory_Copy(buffer.data + pos, mesh.vertexBuffer.data, mesh.vertexBuffer.size);
    pos += mesh.vertexBuffer.size;

    if ((mesh.channels & MeshChannel_UV1) != 0)
    {
        mesh.uv1Buffer = Buffer_Create(GetAllocInfo(header.vertexCount * sizeof(Vector2)));
        Memory_Copy(buffer.data + pos, mesh.uv1Buffer.data, mesh.uv1Buffer.size);
        pos += mesh.uv1Buffer.size;
    }

    if ((mesh.channels & MeshChannel_Normal) != 0)
    {
        mesh.normalBuffer = Buffer_Create(GetAllocInfo(header.vertexCount * sizeof(Vector3)));
        Memory_Copy(buffer.data + pos, mesh.normalBuffer.data, mesh.normalBuffer.size);
        pos += mesh.normalBuffer.size;
    }

    mesh.indexBuffer = Buffer_Create(GetAllocInfo(header.indexCount * sizeof(uint16)));
    Memory_Copy(buffer.data + pos, mesh.indexBuffer.data, mesh.indexBuffer.size);
    pos += mesh.indexBuffer.size;

    return mesh;
}

Texture AssetDB_LoadTexture(String fname, AssetInfo info)
{
    Buffer buffer = AssetDB_LoadAsset(fname, info);

    Texture texture = { 0 };

    uint32 pos = 0;
    TextureHeader header = *(TextureHeader*)(buffer.data + pos);
    pos += sizeof(TextureHeader);

    texture.width = header.width;
    texture.height = header.height;
    texture.filterMode = header.filterMode;
    texture.format = header.format;

    texture.buffer = Buffer_Create(GetAllocInfo(header.size));
    Memory_Copy(buffer.data + pos, texture.buffer.data, header.size);

    return texture;
}