#ifndef _ASSETDB_H_
#define _ASSETDB_H_

#include "ustring.h"
#include "util.h"
#include "filesystem.h"
#include "print.h"
#include "zstd/zstd.h"

#include "shader.h"
#include "mesh.h"
#include "texture.h"

typedef struct AssetInfo_t
{
    uint32 location;
    uint32 sizeBytes;
} AssetInfo;

typedef struct AssetDBHeader_t
{
    uint32 assetCount;
    AssetInfo* assets;
} AssetDBHeader;

typedef struct ShaderHeader_t
{
    uint32 vertexShaderLength;
    uint32 fragmentShaderLength;
} ShaderHeader;

typedef struct MeshHeader_t
{
    uint32 vertexCount;
    uint32 indexCount;
    uint8 channels;
} MeshHeader;

typedef struct TextureHeader_t
{
    uint16 width;
    uint16 height;
    TextureFormat format;
    TextureFilterMode filterMode;
    uint32 size;
} TextureHeader;

AssetDBHeader AssetDB_LoadHeader(String fname);
Buffer AssetDB_LoadAsset(String fname, AssetInfo info);
Shader AssetDB_LoadShader(String fname, AssetInfo info);
Mesh AssetDB_LoadMesh(String fname, AssetInfo info);
Texture AssetDB_LoadTexture(String fname, AssetInfo info);

#endif