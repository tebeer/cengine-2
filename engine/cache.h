#ifndef _DATACACHE_T_
#define _DATACACHE_T_

#include "util.h"

typedef struct DataHandle_t
{
    uint16 index;
    uint16 version;
} DataHandle;

typedef struct DataIndex_t
{
    uint16 version;
    union
    {
        uint16 dataIndex;
        uint16 nextFreeIndex;
    };
} DataIndex;

static DataHandle DataHandle_Null = {0};

#endif

#ifdef CACHE_TYPE

#include "template_start.h"

#define T CACHE_TYPE
#define DataCache_T Template(DataCache, T)

typedef struct Template(DataCache_t, T)
{
    T* data;

    DataIndex* indexArray;

    uint16* reverseIndex;

    uint16 indexArrayLength;
    uint16 nextFreeIndex;

    uint16 dataCount;
    uint16 dataLength;

} DataCache_T;

static void Template(DataCache_T, InitFreeList)(DataCache_T* cache, uint16 min, uint16 max)
{
    for (uint32 i = min; i < max; ++i)
    {
        cache->indexArray[i].dataIndex = (uint16)(i + 1);
        cache->indexArray[i].version = 0;
    }
}

static uint8 Template(DataCache_T, Exists)(DataCache_T* cache, DataHandle handle)
{
    return cache->indexArray[handle.index].version == handle.version;
}

static T* Template(DataCache_T, GetRef)(DataCache_T* cache, DataHandle handle)
{
    ASSERT(handle.version != 0, "GetRef<%s>: Null reference", TO_STRING(T));
    ASSERT(handle.index < cache->indexArrayLength, "GetRef<%s>: Index out of bounds %i", TO_STRING(T), handle.index);

    DataIndex* index = &cache->indexArray[handle.index];

    ASSERT(index->version == handle.version, "GetRef<%s>: Invalid version %i != %i", TO_STRING(T), handle.version, index->version);

    return &cache->data[index->dataIndex];
}

static uint16 Template(DataCache_T, AddData)(DataCache_T* cache, const T data)
{
    uint16 dataIndex = cache->dataCount;
    if (cache->dataCount == cache->dataLength)
    {
        uint32 newSize = cache->dataLength * 2;
        if (newSize == 0)
            newSize = 1;
        ASSERT(newSize <= UINT16_MAX, "newSize(%i) > UINT_MAX", newSize);
        cache->dataLength = (uint16)newSize;
        cache->data = Memory_Realloc(cache->data, newSize * sizeof(T));
        cache->reverseIndex = Memory_Realloc(cache->reverseIndex, newSize * sizeof(uint16));
    }

    cache->dataCount++;
    cache->data[dataIndex] = data;

    return dataIndex;
}

static DataHandle Template(DataCache_T, Add)(DataCache_T* cache, const T data)
{
    uint16 index = cache->nextFreeIndex;
    uint16 size = cache->indexArrayLength;

    ASSERT(index <= size, "Add<%s>: Index(%i) > size(%i)", TO_STRING(T), index, size);

    if (index == size)
    {
        if (size == 0)
            cache->indexArrayLength = 1;
        else
            cache->indexArrayLength = 2 * size;
        cache->indexArray = Memory_Realloc(cache->indexArray, cache->indexArrayLength * sizeof(DataIndex));
        Template(DataCache_T, InitFreeList)(cache, size, cache->indexArrayLength);
    }

    cache->nextFreeIndex = cache->indexArray[index].nextFreeIndex;
    if (cache->indexArray[index].version == 0)
        cache->indexArray[index].version++;

    DataHandle handle;
    handle.index = index;
    handle.version = cache->indexArray[index].version;

    uint16 dataIndex = Template(DataCache_T, AddData)(cache, data);
    cache->indexArray[index].dataIndex = dataIndex;
    cache->reverseIndex[dataIndex] = index;

    return handle;
}

static T* Template(DataCache_T, AddEmpty)(DataCache_T* cache, DataHandle* handle)
{
    *handle = Template(DataCache_T, Add)(cache, (T) { 0 });
    return Template(DataCache_T, GetRef)(cache, *handle);
}

static void Template(DataCache_T, Remove)(DataCache_T* cache, DataHandle handle)
{
    ASSERT(handle.version != 0, "Remove<%s>: Null reference", TO_STRING(T));
    ASSERT(handle.index < cache->indexArrayLength, "Remove<%s>: Index out of bounds %i", TO_STRING(T), handle.index);

    DataIndex* index = &cache->indexArray[handle.index];

    ASSERT(index->version == handle.version, "Remove<%s>: Invalid version %i != %i", TO_STRING(T), handle.version, index->version);

    cache->dataCount--;

    if (index->dataIndex != cache->dataCount)
    {
        cache->data[index->dataIndex] = cache->data[cache->dataCount];
        cache->indexArray[cache->reverseIndex[cache->dataCount]].dataIndex = index->dataIndex;
        cache->reverseIndex[index->dataIndex] = cache->reverseIndex[cache->dataCount];
    }

    index->version++;
    index->nextFreeIndex = cache->nextFreeIndex;
    cache->nextFreeIndex = handle.index;
}

#include "template_end.h"

#undef T
#undef DataCache_T
#undef CACHE_TYPE

#endif

