
type
    Buffer* {.bycopy, header: "memory.h", importc.} = object
        data*: pointer
        size*: uint32

type
    DataHandle* {.bycopy, header: "cache.h", importc.} = object
        index: uint16
        version: uint16

type
    String* {.bycopy, header: "ustring.h", importc.} = object
        buffer: Buffer
        length: uint32


type
    Color* {.bycopy, header: "gfx.h", importc.} = object
        r*: cfloat
        g*: cfloat
        b*: cfloat
        a*: cfloat

    ClearFlags* {.pure, bycopy, header: "gfx.h", importc.} = enum
        Color = 1,
        Depth = 2,
        ColorAndDepth = 3,

    BufferType* {.pure, bycopy, header: "gfx.h", importc.} = enum
        Vertex,
        Index,

    VertexElementType* {.pure, bycopy, header: "gfx.h", importc.} = enum
        Float2,
        Float3,
        Float4,

    VertexBuffer* {.bycopy, header: "gfx.h", importc.} = object
        buffer*: pointer
        offset*: uint32
        stride*: uint32

    VertexElement* {.bycopy, header: "gfx.h", importc.} = object
        name: String 
        bufferIndex: uint32
        vtype: VertexElementType

    VertexLayout* {.bycopy, header: "gfx.h", importc.} = object
        elements: array[12, VertexElement]
        elementCount: uint32

    RenderState* {.bycopy, header: "gfx.h", importc.} = object
        shader*: DataHandle
        indexBuffer*: pointer
        vertexBufferCount*: uint32
        vertexBuffers*: array[4, VertexBuffer]

        vertexLayout*: DataHandle
        constantBuffer*: DataHandle
        texture*: DataHandle

    Mesh* {.bycopy, header: "mesh.h", importc.} = object
        vertexBuffer*: Buffer
        uv1Buffer*: Buffer
        normalBuffer*: Buffer 
        vertexCount*: uint16 
        channels*: uint8 
        indexBuffer*: Buffer
        indices*: ptr uint16
        indexCount*: uint32
    
    ShaderConstant* {.bycopy, header: "shader.h", importc.} = object
        name: array[32, uint8]
        location: uint8
        size: uint8

    ShaderResourceBinding* {.bycopy, header: "shader.h", importc.} = object
        name: array[32, uint8]
        location: uint8
    
    ShaderConstantLayout* {.bycopy, header: "shader.h", importc.} = object
        vars: array[16, ShaderConstant]
        varCount: uint8 
        resources: array[16, ShaderResourceBinding]
        resourceCount: uint8
    
    Shader* {.bycopy, header: "shader.h", importc.} = object
        vertexShader: Buffer
        vertexConstantLayout: ShaderConstantLayout
        fragmentShader: Buffer
        fragmentConstantLayout: ShaderConstantLayout

    TextureFilterMode {.pure, bycopy, header: "texture.h", importc.} = enum
        Point,
        Bilinear,
        Trilinear,
        
    TextureFormat* {.bycopy, header: "texture.h", importc.} = enum
        RGB,
        RGBA,
        
    Texture* {.bycopy, header: "texture.h", importc.} = object
        width: uint16
        height: uint16
        format: TextureFormat
        filterMode: TextureFilterMode
        buffer: Buffer
        
proc GFX_CreateBuffer*(data: pointer, size:uint32, bufferType:BufferType): pointer {.header: "gfx.h", importc.}
proc GFX_DeleteBuffer*(buffer: pointer) {.header: "gfx.h", importc.}
proc GFX_SetBufferData*(buffer: pointer, data:pointer, size:uint32, bufferType: BufferType) {.header: "gfx.h", importc.}

proc GFX_CreateShaderConstantBuffer*(size:uint32) {.header: "gfx.h", importc.}
proc GFX_SetShaderConstantBuffer(handle: DataHandle, data:Buffer) {.header: "gfx.h", importc.}
proc GFX_SetShaderConstantBuffer*[T](handle: DataHandle, data:T) =
    GFX_SetShaderConstantBuffer(handle, Buffer(
        data: unsafeAddr data,
        size: cuint(sizeof(data)),
    ))

proc GFX_CreateVertexShaderInputLayout*(shaderHandle: DataHandle, layout: ptr VertexLayout): DataHandle {.header: "gfx.h", importc.}
proc GFX_SetRenderState*(state: ptr RenderState) {.header: "gfx.h", importc.}

proc GFX_CreateShader*(shader: Shader): DataHandle {.header: "gfx.h", importc.}
proc GFX_DestroyShader*(shader: DataHandle) {.header: "gfx.h", importc.}
proc GFX_CreateTexture*(texture: Texture): DataHandle {.header: "gfx.h", importc.}

proc GFX_Clear*(flags: ClearFlags, color: Color) {.header: "gfx.h", importc.}
proc GFX_DrawIndexed*(indexCount: uint32, startIndex: uint32) {.header: "gfx.h", importc.}
#proc GFX_SetBlendFunc(BlendFunc srcBlend, BlendFunc dstBlend);


type AssetInfo {.bycopy, header: "assetdb.h", importc.} = object
    location: uint32
    sizeBytes: uint32

type AssetDBHeader {.bycopy, header: "assetdb.h", importc,.} = object
    assetCount: uint32
    assets: ptr UncheckedArray[AssetInfo]

proc AssetDB_LoadHeader(fname: String): AssetDBHeader {.header: "assetdb.h", importc.}
#proc AssetDB_LoadAsset(fname: String, info: AssetInfo): Buffer {.header: "gfx.h", importc.}
proc AssetDB_LoadShader(fname: String, info: AssetInfo): Shader {.header: "gfx.h", importc.}
proc AssetDB_LoadMesh(fname: String, info: AssetInfo): Mesh {.header: "gfx.h", importc.}
proc AssetDB_LoadTexture(fname: String, info: AssetInfo): Texture {.header: "gfx.h", importc.}

type AssetDB* = object
    header: AssetDBHeader
    fname: string

proc ToUString*(str: string): String = 
    result.buffer.data = cstring(str)
    result.buffer.size = uint32(str.len)

proc AssetDB_Load*(fname: string): AssetDB = 
    var db:AssetDB
    db.header = AssetDB_LoadHeader(fname.ToUString)
    db.fname = fname
    result = db

proc LoadShader*(db: var AssetDB, assetId: int): Shader = 
    result = AssetDB_LoadShader(db.fname.ToUString, db.header.assets[assetId])

proc LoadMesh*(db: var AssetDB, assetId: int): Mesh = 
    result = AssetDB_LoadMesh(db.fname.ToUString, db.header.assets[assetId])

proc LoadTexture*(db: var AssetDB, assetId: int): Texture = 
    result = AssetDB_LoadTexture(db.fname.ToUString, db.header.assets[assetId])


type
    KeyCode* {.bycopy, header: "input.h", importc.} = enum
        MouseLeft = 0x01,
        MouseRight = 0x02,
    
    InputState* {.bycopy, header: "input.h", importc.} = object
        mouse_x*: float64
        mouse_y*: float64
        buttons*: array[256, int32]
    
    AppContext* {.bycopy, header: "user.h", importc.} = object
        #unsigned char* fixedMemory;
        #unsigned int fixedMemorySize;
        #unsigned char* cacheMemory;
        #unsigned int cacheSize;
        deltaTime*: float64
        currentTime*: float64
        input*: InputState
    