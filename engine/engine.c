#include "array.c"
#include "math.c"
#include "memory.c"
#include "mesh.c"
#include "print.c"
#include "string.c"
#include "util.c"
#include "filesystem.c"
#include "assetdb.c"

#ifdef GFX_OPENGL
#include "platform/gl.c"
#include "platform/gfx_opengl.c"
#include "platform/windows/gfx_opengl_windows.c"
#elif GFX_D3D11
#include "platform/windows/gfx_d3d11.c"
#else
#error GFX NOT DEFINED
#endif

#include "platform/windows/platform_windows.c"
#include "platform/windows/application_windows.c"
