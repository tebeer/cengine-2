#include "filesystem.h"
#include "print.h"
#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#pragma warning( disable : 4996 )
#endif

int File_ReadAll(String name, Buffer* out)
{
    FILE *f = fopen(name.str, "rb");
    ASSERT(f != NULL, "File_ReadAll: Could not open file: %s", name.str);

    long pos = ftell(f);
    fseek(f, 0L, SEEK_END);
    long ret = ftell(f);
    fseek(f, pos, SEEK_SET);

    long size = ret - pos;

    *out = Buffer_Create(GetAllocInfo(size + 1));

    fread(out->data, 1, size, f);
    out->data[size] = '\0';

    fclose(f);
    return 0;
}

int File_Write(String fname, long pos, void* data, uint32 size)
{
    FILE *f = fopen(fname.str, "rb+");
    if (!f)
    {
        // File doesn't exist, create it 
        f = fopen(fname.str, "wb");
    }

    if(!f)
    {
        DebugFormat("File_Write Failed: %s", fname.str);
        return 1;
    }

    fseek(f, pos, SEEK_SET);

    fwrite(data, 1, size, f);

    fclose(f);

    return 0;
}

void File_Clear(String fname)
{
    FILE *f = fopen(fname.str, "w");
    if (!f)
        return;
    fclose(f);
    return;
}

int File_Append(String fname, void* data, uint32 size)
{
    FILE *f = fopen(fname.str, "ab");
    ASSERT(f != NULL, "Could not append to file %s", fname.str);
    fwrite(data, 1, size, f);
    fclose(f);
    return 0;
}

int File_AppendFormat(String fname, const char* format, ...)
{
    FILE *f = fopen(fname.str, "a");
    ASSERT(f != NULL, "Could not append to file %s", fname.str);

    va_list arglist;
    va_start(arglist, format);
    vfprintf(f, format, arglist);
    va_end(arglist);

    fclose(f);
    return 0;
}

void* File_Open(String fname)
{
    FILE *f = fopen(fname.str, "r");
    return f;
}

void File_Read(void* file, long pos, void* dst, uint32 size)
{
    fseek(file, pos, SEEK_SET);
    fread(dst, 1, size, file);
}

void File_Close(void* file)
{
    fclose(file);
}

void File_ReadSimple(String fname, long pos, void* dst, uint32 size)
{
    FILE *f = fopen(fname.buffer.data, "rb");
    ASSERT(f != NULL, "Could not open file %s", fname.buffer.data);
    fseek(f, pos, SEEK_SET);
    uint32 read = (uint32)fread(dst, 1, size, f);
    ASSERT(read == size, "Could not read all bytes from %s %i/%i %i", fname.buffer.data, read, size, pos);
    fclose(f);
}