#include "ustring.h"
#include "util.h"

int File_ReadAll(String fname, Buffer* out);
int File_Write(String fname, long pos, void* data, uint32 size);
void File_Clear(String fname);
int File_Append(String fname, void* data, uint32 size);
int File_AppendFormat(String fname, const char* format, ...);

void* File_Open(String fname);
void File_Read(void* file, long pos, void* dst, uint32 size);
void File_Close(void* file);
void File_ReadSimple(String fname, long pos, void* dst, uint32 size);
