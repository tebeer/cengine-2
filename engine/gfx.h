#ifndef _RENDERER_H_
#define _RENDERER_H_

#include "util.h"
#include "memory.h"
#include "ustring.h"
#include "shader.h"
#include "texture.h"
#include "cache.h"

#define Debug_AssertErrors(info) GFX_AssertErrors(info, __FILE__, __LINE__)

typedef struct Color_t
{
    float r, g, b, a;
} Color;

typedef enum ClearFlags_t
{
    ClearFlags_Color = 1,
    ClearFlags_Depth = 2,
    ClearFlags_ColorDepth = ClearFlags_Color | ClearFlags_Depth,
} ClearFlags;

typedef enum BlendFunc_t
{
    BlendFunc_Zero,
    BlendFunc_One,
    BlendFunc_SrcColor,
    BlendFunc_OneMinusSrcColor,
    BlendFunc_DstColor,
    BlendFunc_OneMinusDstColor,
    BlendFunc_SrcAlpha,
    BlendFunc_OneMinusSrcAlpha,
    BlendFunc_DstAlpha,
    BlendFunc_OneMinusDstAlpha,
    BlendFunc_ConstantColor,
    BlendFunc_OneMinusConstantColor,
    BlendFunc_ConstantAlpha,
    BlendFunc_OneMinusConstantAlpha
} BlendFunc;

typedef enum BufferType_t
{
    BufferType_Vertex,
    BufferType_Index,
    //BufferType_Constant,
} BufferType;

typedef enum VertexElementType_t
{
    VertexElementType_Float2,
    VertexElementType_Float3,
    VertexElementType_Float4,
} VertexElementType;

typedef struct VertexElement_t
{
    String name;
    uint32 bufferIndex;
    VertexElementType type;
} VertexElement;

typedef struct VertexLayout_t
{
    VertexElement elements[12];
    uint32 elementCount;
} VertexLayout;

typedef struct VertexBuffer_t
{
    void* buffer;
    uint32 offset;
    uint32 stride;
} VertexBuffer;

typedef struct RenderState_t
{
    DataHandle shader;
    void* indexBuffer;
    VertexBuffer vertexBuffers[4];
    uint32 vertexBufferCount;

    DataHandle vertexLayout;
    DataHandle constantBuffer;
    DataHandle texture;
} RenderState;

String GFX_GetGPUVendorName();
String GFX_GetGPUName();
void GFX_AssertErrors(const char* info, const char* file, const uint32 line);
void GFX_PrintErrors();

void GFX_SetViewport(int width, int height);
void GFX_SetVSync(int on);

int GFX_GetVertexCount();
void GFX_ResetVertexCount();

void* GFX_CreateBuffer(void* data, unsigned int size, BufferType type);
void GFX_DeleteBuffer(void* buffer);
void GFX_SetBufferData(void* buffer, void* data, unsigned int size, BufferType type);
DataHandle GFX_CreateShaderConstantBuffer(uint32 size);
void GFX_SetShaderConstantBuffer(DataHandle constantBuffer, Buffer data);
DataHandle GFX_CreateVertexShaderInputLayout(DataHandle shaderHandle, const VertexLayout* layout);
void GFX_SetRenderState(const RenderState* state);

DataHandle GFX_CreateShader(Shader shader);
void GFX_DestroyShader(DataHandle shader);

void GFX_Clear(const ClearFlags clearFlags, const Color color);
void GFX_DrawIndexed(unsigned int indexCount, unsigned int startIndex);
void GFX_SetBlendFunc(BlendFunc srcBlend, BlendFunc dstBlend);

DataHandle GFX_CreateTexture(Texture texture);

//void Draw_Mesh(Transform transform);
void Draw_Quad(float x, float y, float width, float height);
void Draw_Text(float x, float y, char* text);

#endif