#include "image.h"
#include "zpng.h"
#include "asset.h"
#include "print.h"
#include "platform.h"

#ifdef SUPPORT_ZPNG
int Image_LoadZPNG(Buffer rawData, Image* img)
{
    double time = Platform_GetTimeMs();

    //unsigned int error = lodepng_decode_memory(&img, &width, &height,
    //    buffer.data, buffer.size, LCT_RGBA, 8);
    //channels = 4;

    ZPNG_Buffer zpngBuffer;
    zpngBuffer.Data = rawData.data;
    zpngBuffer.Bytes = rawData.size;

    ZPNG_ImageData decompressResult = ZPNG_Decompress(zpngBuffer);
    img->buffer.data = decompressResult.Buffer.Data;
    img->buffer.size = decompressResult.Buffer.Bytes;

    if (img->buffer.data == 0)
    {
        DebugFormat("Decompress failed %i", decompressResult.Buffer.Bytes);
        return 0;
    }

    img->channels = (unsigned char)decompressResult.Channels;
    img->width = decompressResult.WidthPixels;
    img->height = decompressResult.HeightPixels;

    //String zpngPath = Path_ChangeExtension(fname, "zpng");
    //String fullPath = UString_Concatenate(UString("C:\\Users\\CFE\\Desktop\\dev\\cengine2\\assets\\"), zpngPath);
    //ZPNG_ImageData image;
    //image.Buffer.Data = img;
    //image.Buffer.Bytes = width * height * channels;
    //image.BytesPerChannel = 1;
    //image.Channels = channels;
    //image.HeightPixels = height;
    //image.WidthPixels = width;
    //image.StrideBytes = width * image.Channels;
    //ZPNG_Buffer zpng = ZPNG_Compress(&image);
    //Asset_Write(fullPath, zpng.Data, zpng.Bytes);
    //UString_Free(&fullPath);

    time = Platform_GetTimeMs() - time;

    if (img->channels == 0)
        return 0;

    return 1;
}
#endif

#define LODEPNG_NO_COMPILE_ALLOCATORS
#include "lodepng.c"
#include "lodepng.h"

static void* lodepng_malloc(size_t size)
{
    return Memory_Alloc(size);
//#ifdef LODEPNG_MAX_ALLOC
//    if (size > LODEPNG_MAX_ALLOC) return 0;
//#endif
//    return malloc(size);
}

static void* lodepng_realloc(void* ptr, size_t new_size)
{
    return Memory_Realloc(ptr, new_size);
//#ifdef LODEPNG_MAX_ALLOC
//    if (new_size > LODEPNG_MAX_ALLOC) return 0;
//#endif
//    return realloc(ptr, new_size);
}

static void lodepng_free(void* ptr)
{
    Memory_Free(ptr);
}

int Image_LoadPNG(Buffer buffer, Image* img)
{
    LodePNGState state;

    lodepng_state_init(&state);

    unsigned char* data;
    unsigned int error = lodepng_decode(&data, &img->width, &img->height, &state, buffer.data, buffer.size);

    if (error != 0)
    {
        DebugFormat("PNG Load Error: ", error);
        lodepng_state_cleanup(&state);
        return error;
    }

    img->channels = 4;
    img->buffer = (Buffer)
    {
        .data = data,
        .size = img->width * img->height * img->channels * state.info_raw.bitdepth / 8,
    };

    //switch (state.info_raw.colortype)
    //{
    //case LCT_RGB:
    //    img->channels = 3;
    //    break;
    //case LCT_RGBA:
    //    img->channels = 4;
    //    break;
    //}

    DebugFormat("channels: %i", img->channels);

    lodepng_state_cleanup(&state);

    return 0;
}