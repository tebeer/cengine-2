#ifndef _IMAGE_H_
#define _IMAGE_H_

#include "ustring.h"

typedef struct Image_t
{
    Buffer buffer;
    unsigned int width;
    unsigned int height;
    unsigned int channels;
} Image;

int Image_LoadZPNG(Buffer buffer, Image* img);
int Image_LoadPNG(Buffer buffer, Image* img);

#endif