#ifndef _INPUT_H_
#define _INPUT_H_

enum KeyCode
{
    KeyCode_MouseLeft = 0x01,
    KeyCode_MouseRight = 0x02,
};

typedef struct InputState_t
{
    float mouse_x;
    float mouse_y;
    int buttons[256];
} InputState;

#endif