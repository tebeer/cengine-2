#include "math2.h"

#include <math.h>       // Required for: sinf(), cosf(), tan(), fabs()

//----------------------------------------------------------------------------------
// Module Functions Definition - Utils math
//----------------------------------------------------------------------------------

// Clamp float value
float Clamp(float value, float min, float max)
{
    const float res = value < min ? min : value;
    return res > max ? max : res;
}

//----------------------------------------------------------------------------------
// Module Functions Definition - Vector2 math
//----------------------------------------------------------------------------------

// Vector with components value 0.0f
Vector2 Vector2Zero(void) { return (Vector2) { 0.0f, 0.0f }; }

// Vector with components value 1.0f
Vector2 Vector2One(void) { return (Vector2) { 1.0f, 1.0f }; }

// Add two vectors (v1 + v2)
Vector2 Vector2Add(Vector2 v1, Vector2 v2)
{
    return (Vector2) { v1.x + v2.x, v1.y + v2.y };
}

// Subtract two vectors (v1 - v2)
Vector2 Vector2Subtract(Vector2 v1, Vector2 v2)
{
    return (Vector2) { v1.x - v2.x, v1.y - v2.y };
}

// Calculate vector length
float Vector2Length(Vector2 v)
{
    return sqrtf((v.x*v.x) + (v.y*v.y));
}

// Calculate two vectors dot product
float Vector2DotProduct(Vector2 v1, Vector2 v2)
{
    return (v1.x*v2.x + v1.y*v2.y);
}

// Calculate distance between two vectors
float Vector2Distance(Vector2 v1, Vector2 v2)
{
    return sqrtf((v1.x - v2.x)*(v1.x - v2.x) + (v1.y - v2.y)*(v1.y - v2.y));
}

// Calculate angle from two vectors in X-axis
float Vector2Angle(Vector2 v1, Vector2 v2)
{
    float angle = atan2f(v2.y - v1.y, v2.x - v1.x)*(180.0f / PI);

    if (angle < 0) angle += 360.0f;

    return angle;
}

// Scale vector (multiply by value)
void Vector2Scale(Vector2 *v, float scale)
{
    v->x *= scale;
    v->y *= scale;
}

// Negate vector
void Vector2Negate(Vector2 *v)
{
    v->x = -v->x;
    v->y = -v->y;
}

// Divide vector by a float value
void Vector2Divide(Vector2 *v, float div)
{
    *v = (Vector2) { v->x / div, v->y / div };
}

// Normalize provided vector
void Vector2Normalize(Vector2 *v)
{
    Vector2Divide(v, Vector2Length(*v));
}

//----------------------------------------------------------------------------------
// Module Functions Definition - Vector3 math
//----------------------------------------------------------------------------------

// Vector with components value 0.0f
Vector3 Vector3Zero(void) { return (Vector3) { 0.0f, 0.0f, 0.0f }; }

// Vector with components value 1.0f
Vector3 Vector3One(void) { return (Vector3) { 1.0f, 1.0f, 1.0f }; }

// Add two vectors
Vector3 Vector3Add(Vector3 v1, Vector3 v2)
{
    return (Vector3) { v1.x + v2.x, v1.y + v2.y, v1.z + v2.z };
}

// Substract two vectors
Vector3 Vector3Subtract(Vector3 v1, Vector3 v2)
{
    return (Vector3) { v1.x - v2.x, v1.y - v2.y, v1.z - v2.z };
}

// Multiply vector by scalar
Vector3 Vector3Multiply(Vector3 v, float scalar)
{
    v.x *= scalar;
    v.y *= scalar;
    v.z *= scalar;

    return v;
}

// Multiply vector by vector
Vector3 Vector3MultiplyV(Vector3 v1, Vector3 v2)
{
    Vector3 result;

    result.x = v1.x * v2.x;
    result.y = v1.y * v2.y;
    result.z = v1.z * v2.z;

    return result;
}

// Calculate two vectors cross product
Vector3 Vector3CrossProduct(Vector3 v1, Vector3 v2)
{
    Vector3 result;

    result.x = v1.y*v2.z - v1.z*v2.y;
    result.y = v1.z*v2.x - v1.x*v2.z;
    result.z = v1.x*v2.y - v1.y*v2.x;

    return result;
}

// Calculate one vector perpendicular vector
Vector3 Vector3Perpendicular(Vector3 v)
{
    Vector3 result;

    float min = fabsf(v.x);
    Vector3 cardinalAxis = { 1.0f, 0.0f, 0.0f };

    if (fabsf(v.y) < min)
    {
        min = fabsf(v.y);
        cardinalAxis = (Vector3) { 0.0f, 1.0f, 0.0f };
    }

    if (fabsf(v.z) < min)
    {
        cardinalAxis = (Vector3) { 0.0f, 0.0f, 1.0f };
    }

    result = Vector3CrossProduct(v, cardinalAxis);

    return result;
}

// Calculate vector length
float Vector3Length(const Vector3 v)
{
    return sqrtf(v.x*v.x + v.y*v.y + v.z*v.z);
}

// Calculate two vectors dot product
float Vector3DotProduct(Vector3 v1, Vector3 v2)
{
    return (v1.x*v2.x + v1.y*v2.y + v1.z*v2.z);
}

// Calculate distance between two vectors
float Vector3Distance(Vector3 v1, Vector3 v2)
{
    float dx = v2.x - v1.x;
    float dy = v2.y - v1.y;
    float dz = v2.z - v1.z;

    return sqrtf(dx*dx + dy*dy + dz*dz);
}

// Scale provided vector
void Vector3Scale(Vector3 *v, float scale)
{
    v->x *= scale;
    v->y *= scale;
    v->z *= scale;
}

// Negate provided vector (invert direction)
void Vector3Negate(Vector3 *v)
{
    v->x = -v->x;
    v->y = -v->y;
    v->z = -v->z;
}

// Normalize provided vector
void Vector3Normalize(Vector3 *v)
{
    float length, ilength;

    length = Vector3Length(*v);

    if (length == 0.0f) length = 1.0f;

    ilength = 1.0f / length;

    v->x *= ilength;
    v->y *= ilength;
    v->z *= ilength;
}

// Transforms a Vector3 by a given Matrix
void Vector3Transform(Vector3 *v, Matrix mat)
{
    float x = v->x;
    float y = v->y;
    float z = v->z;

    v->x = mat.m00*x + mat.m01*y + mat.m02*z + mat.m03;
    v->y = mat.m10*x + mat.m11*y + mat.m12*z + mat.m13;
    v->z = mat.m20*x + mat.m21*y + mat.m22*z + mat.m23;
};

// Calculate linear interpolation between two vectors
Vector3 Vector3Lerp(Vector3 v1, Vector3 v2, float amount)
{
    Vector3 result;

    result.x = v1.x + amount*(v2.x - v1.x);
    result.y = v1.y + amount*(v2.y - v1.y);
    result.z = v1.z + amount*(v2.z - v1.z);

    return result;
}

// Calculate reflected vector to normal
Vector3 Vector3Reflect(Vector3 vector, Vector3 normal)
{
    // I is the original vector
    // N is the normal of the incident plane
    // R = I - (2*N*( DotProduct[ I,N] ))

    Vector3 result;

    float dotProduct = Vector3DotProduct(vector, normal);

    result.x = vector.x - (2.0f*normal.x)*dotProduct;
    result.y = vector.y - (2.0f*normal.y)*dotProduct;
    result.z = vector.z - (2.0f*normal.z)*dotProduct;

    return result;
}

// Return min value for each pair of components
Vector3 Vector3Min(Vector3 vec1, Vector3 vec2)
{
    Vector3 result;

    result.x = fminf(vec1.x, vec2.x);
    result.y = fminf(vec1.y, vec2.y);
    result.z = fminf(vec1.z, vec2.z);

    return result;
}

// Return max value for each pair of components
Vector3 Vector3Max(Vector3 vec1, Vector3 vec2)
{
    Vector3 result;

    result.x = fmaxf(vec1.x, vec2.x);
    result.y = fmaxf(vec1.y, vec2.y);
    result.z = fmaxf(vec1.z, vec2.z);

    return result;
}

// Compute barycenter coordinates (u, v, w) for point p with respect to triangle (a, b, c)
// NOTE: Assumes P is on the plane of the triangle
Vector3 Vector3Barycenter(Vector3 p, Vector3 a, Vector3 b, Vector3 c)
{
    //Vector v0 = b - a, v1 = c - a, v2 = p - a;

    Vector3 v0 = Vector3Subtract(b, a);
    Vector3 v1 = Vector3Subtract(c, a);
    Vector3 v2 = Vector3Subtract(p, a);
    float d00 = Vector3DotProduct(v0, v0);
    float d01 = Vector3DotProduct(v0, v1);
    float d11 = Vector3DotProduct(v1, v1);
    float d20 = Vector3DotProduct(v2, v0);
    float d21 = Vector3DotProduct(v2, v1);

    float denom = d00*d11 - d01*d01;

    Vector3 result;

    result.y = (d11*d20 - d01*d21) / denom;
    result.z = (d00*d21 - d01*d20) / denom;
    result.x = 1.0f - (result.z + result.y);

    return result;
}

// Returns Vector3 as float array
float *Vector3ToFloat(Vector3 vec)
{
    static float buffer[3];

    buffer[0] = vec.x;
    buffer[1] = vec.y;
    buffer[2] = vec.z;

    return buffer;
}

Vector3 Vector3RotateByQuaternion(Vector3 v, Quaternion q)
{
    Vector3 t = {
        2 * (q.y*v.z - q.z*v.y),
        2 * (q.z*v.x - q.x*v.z),
        2 * (q.x*v.y - q.y*v.x)
    };

    return (Vector3) {
        v.x + q.w * t.x + q.y*t.z - q.z*t.y,
        v.y + q.w * t.y + q.z*t.x - q.x*t.z,
        v.z + q.w * t.z + q.x*t.y - q.y*t.x,
    };
}

//----------------------------------------------------------------------------------
// Module Functions Definition - Matrix math
//----------------------------------------------------------------------------------

// Compute matrix determinant
float MatrixDeterminant(Matrix mat)
{
    float result;

    // Cache the matrix values (speed optimization)
    float a00 = mat.m00, a01 = mat.m10, a02 = mat.m20, a03 = mat.m30;
    float a10 = mat.m01, a11 = mat.m11, a12 = mat.m21, a13 = mat.m31;
    float a20 = mat.m02, a21 = mat.m12, a22 = mat.m22, a23 = mat.m32;
    float a30 = mat.m03, a31 = mat.m13, a32 = mat.m23, a33 = mat.m33;

    result = a30*a21*a12*a03 - a20*a31*a12*a03 - a30*a11*a22*a03 + a10*a31*a22*a03 +
        a20*a11*a32*a03 - a10*a21*a32*a03 - a30*a21*a02*a13 + a20*a31*a02*a13 +
        a30*a01*a22*a13 - a00*a31*a22*a13 - a20*a01*a32*a13 + a00*a21*a32*a13 +
        a30*a11*a02*a23 - a10*a31*a02*a23 - a30*a01*a12*a23 + a00*a31*a12*a23 +
        a10*a01*a32*a23 - a00*a11*a32*a23 - a20*a11*a02*a33 + a10*a21*a02*a33 +
        a20*a01*a12*a33 - a00*a21*a12*a33 - a10*a01*a22*a33 + a00*a11*a22*a33;

    return result;
}

// Returns the trace of the matrix (sum of the values along the diagonal)
float MatrixTrace(Matrix mat)
{
    return (mat.m00 + mat.m11 + mat.m22 + mat.m33);
}

// Transposes provided matrix
void MatrixTranspose(Matrix *mat)
{
    Matrix temp;

    temp.m00 = mat->m00;
    temp.m10 = mat->m01;
    temp.m20 = mat->m02;
    temp.m30 = mat->m03;
    temp.m01 = mat->m10;
    temp.m11 = mat->m11;
    temp.m21 = mat->m12;
    temp.m31 = mat->m13;
    temp.m02 = mat->m20;
    temp.m12 = mat->m21;
    temp.m22 = mat->m22;
    temp.m32 = mat->m23;
    temp.m03 = mat->m30;
    temp.m13 = mat->m31;
    temp.m23 = mat->m32;
    temp.m33 = mat->m33;

    *mat = temp;
}

// Invert provided matrix
Matrix MatrixInvert(const Matrix mat)
{
    Matrix temp;

    // Cache the matrix values (speed optimization)
    float a00 = mat.m00, a01 = mat.m10, a02 = mat.m20, a03 = mat.m30;
    float a10 = mat.m01, a11 = mat.m11, a12 = mat.m21, a13 = mat.m31;
    float a20 = mat.m02, a21 = mat.m12, a22 = mat.m22, a23 = mat.m32;
    float a30 = mat.m03, a31 = mat.m13, a32 = mat.m23, a33 = mat.m33;

    float b00 = a00*a11 - a01*a10;
    float b01 = a00*a12 - a02*a10;
    float b02 = a00*a13 - a03*a10;
    float b03 = a01*a12 - a02*a11;
    float b04 = a01*a13 - a03*a11;
    float b05 = a02*a13 - a03*a12;
    float b06 = a20*a31 - a21*a30;
    float b07 = a20*a32 - a22*a30;
    float b08 = a20*a33 - a23*a30;
    float b09 = a21*a32 - a22*a31;
    float b10 = a21*a33 - a23*a31;
    float b11 = a22*a33 - a23*a32;

    // Calculate the invert determinant (inlined to avoid double-caching)
    float invDet = 1.0f / (b00*b11 - b01*b10 + b02*b09 + b03*b08 - b04*b07 + b05*b06);

    temp.m00 = (a11*b11 - a12*b10 + a13*b09)*invDet;
    temp.m10 = (-a01*b11 + a02*b10 - a03*b09)*invDet;
    temp.m20 = (a31*b05 - a32*b04 + a33*b03)*invDet;
    temp.m30 = (-a21*b05 + a22*b04 - a23*b03)*invDet;
    temp.m01 = (-a10*b11 + a12*b08 - a13*b07)*invDet;
    temp.m11 = (a00*b11 - a02*b08 + a03*b07)*invDet;
    temp.m21 = (-a30*b05 + a32*b02 - a33*b01)*invDet;
    temp.m31 = (a20*b05 - a22*b02 + a23*b01)*invDet;
    temp.m02 = (a10*b10 - a11*b08 + a13*b06)*invDet;
    temp.m12 = (-a00*b10 + a01*b08 - a03*b06)*invDet;
    temp.m22 = (a30*b04 - a31*b02 + a33*b00)*invDet;
    temp.m32 = (-a20*b04 + a21*b02 - a23*b00)*invDet;
    temp.m03 = (-a10*b09 + a11*b07 - a12*b06)*invDet;
    temp.m13 = (a00*b09 - a01*b07 + a02*b06)*invDet;
    temp.m23 = (-a30*b03 + a31*b01 - a32*b00)*invDet;
    temp.m33 = (a20*b03 - a21*b01 + a22*b00)*invDet;

    return temp;
}

// Normalize provided matrix
void MatrixNormalize(Matrix *mat)
{
    float det = MatrixDeterminant(*mat);

    mat->m00 /= det;
    mat->m10 /= det;
    mat->m20 /= det;
    mat->m30 /= det;
    mat->m01 /= det;
    mat->m11 /= det;
    mat->m21 /= det;
    mat->m31 /= det;
    mat->m02 /= det;
    mat->m12 /= det;
    mat->m22 /= det;
    mat->m32 /= det;
    mat->m03 /= det;
    mat->m13 /= det;
    mat->m23 /= det;
    mat->m33 /= det;
}

// Returns identity matrix
Matrix MatrixIdentity(void)
{
    Matrix result = { 1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f };

    return result;
}

// Add two matrices
Matrix MatrixAdd(Matrix left, Matrix right)
{
    Matrix result = MatrixIdentity();

    result.m00 = left.m00 + right.m00;
    result.m10 = left.m10 + right.m10;
    result.m20 = left.m20 + right.m20;
    result.m30 = left.m30 + right.m30;
    result.m01 = left.m01 + right.m01;
    result.m11 = left.m11 + right.m11;
    result.m21 = left.m21 + right.m21;
    result.m31 = left.m31 + right.m31;
    result.m02 = left.m02 + right.m02;
    result.m12 = left.m12 + right.m12;
    result.m22 = left.m22 + right.m22;
    result.m32 = left.m32 + right.m32;
    result.m03 = left.m03 + right.m03;
    result.m13 = left.m13 + right.m13;
    result.m23 = left.m23 + right.m23;
    result.m33 = left.m33 + right.m33;

    return result;
}

// Substract two matrices (left - right)
Matrix MatrixSubstract(Matrix left, Matrix right)
{
    Matrix result = MatrixIdentity();

    result.m00 = left.m00 - right.m00;
    result.m10 = left.m10 - right.m10;
    result.m20 = left.m20 - right.m20;
    result.m30 = left.m30 - right.m30;
    result.m01 = left.m01 - right.m01;
    result.m11 = left.m11 - right.m11;
    result.m21 = left.m21 - right.m21;
    result.m31 = left.m31 - right.m31;
    result.m02 = left.m02 - right.m02;
    result.m12 = left.m12 - right.m12;
    result.m22 = left.m22 - right.m22;
    result.m32 = left.m32 - right.m32;
    result.m03 = left.m03 - right.m03;
    result.m13 = left.m13 - right.m13;
    result.m23 = left.m23 - right.m23;
    result.m33 = left.m33 - right.m33;

    return result;
}

// Returns translation matrix
Matrix MatrixTranslate(float x, float y, float z)
{
    Matrix result = { 1.0f, 0.0f, 0.0f, x,
        0.0f, 1.0f, 0.0f, y,
        0.0f, 0.0f, 1.0f, z,
        0.0f, 0.0f, 0.0f, 1.0f };

    return result;
}

// Create rotation matrix from axis and angle
// NOTE: Angle should be provided in radians
Matrix MatrixRotate(Vector3 axis, float angle)
{
    Matrix result;

    Matrix mat = MatrixIdentity();

    float x = axis.x, y = axis.y, z = axis.z;

    float length = sqrtf(x*x + y*y + z*z);

    if ((length != 1.0f) && (length != 0.0f))
    {
        length = 1.0f / length;
        x *= length;
        y *= length;
        z *= length;
    }

    float sinres = sinf(angle);
    float cosres = cosf(angle);
    float t = 1.0f - cosres;

    // Cache some matrix values (speed optimization)
    float a00 = mat.m00, a01 = mat.m10, a02 = mat.m20, a03 = mat.m30;
    float a10 = mat.m01, a11 = mat.m11, a12 = mat.m21, a13 = mat.m31;
    float a20 = mat.m02, a21 = mat.m12, a22 = mat.m22, a23 = mat.m32;

    // Construct the elements of the rotation matrix
    float b00 = x*x*t + cosres, b01 = y*x*t + z*sinres, b02 = z*x*t - y*sinres;
    float b10 = x*y*t - z*sinres, b11 = y*y*t + cosres, b12 = z*y*t + x*sinres;
    float b20 = x*z*t + y*sinres, b21 = y*z*t - x*sinres, b22 = z*z*t + cosres;

    // Perform rotation-specific matrix multiplication
    result.m00 = a00*b00 + a10*b01 + a20*b02;
    result.m10 = a01*b00 + a11*b01 + a21*b02;
    result.m20 = a02*b00 + a12*b01 + a22*b02;
    result.m30 = a03*b00 + a13*b01 + a23*b02;
    result.m01 = a00*b10 + a10*b11 + a20*b12;
    result.m11 = a01*b10 + a11*b11 + a21*b12;
    result.m21 = a02*b10 + a12*b11 + a22*b12;
    result.m31 = a03*b10 + a13*b11 + a23*b12;
    result.m02 = a00*b20 + a10*b21 + a20*b22;
    result.m12 = a01*b20 + a11*b21 + a21*b22;
    result.m22 = a02*b20 + a12*b21 + a22*b22;
    result.m32 = a03*b20 + a13*b21 + a23*b22;
    result.m03 = mat.m03;
    result.m13 = mat.m13;
    result.m23 = mat.m23;
    result.m33 = mat.m33;

    return result;
}

// Returns x-rotation matrix (angle in radians)
Matrix MatrixRotateX(float angle)
{
    Matrix result = MatrixIdentity();

    float cosres = cosf(angle);
    float sinres = sinf(angle);

    result.m11 = cosres;
    result.m21 = -sinres;
    result.m12 = sinres;
    result.m22 = cosres;

    return result;
}

// Returns y-rotation matrix (angle in radians)
Matrix MatrixRotateY(float angle)
{
    Matrix result = MatrixIdentity();

    float cosres = cosf(angle);
    float sinres = sinf(angle);

    result.m00 = cosres;
    result.m20 = sinres;
    result.m02 = -sinres;
    result.m22 = cosres;

    return result;
}

// Returns z-rotation matrix (angle in radians)
Matrix MatrixRotateZ(float angle)
{
    Matrix result = MatrixIdentity();

    float cosres = cosf(angle);
    float sinres = sinf(angle);

    result.m00 = cosres;
    result.m10 = -sinres;
    result.m01 = sinres;
    result.m11 = cosres;

    return result;
}

// Returns scaling matrix
Matrix MatrixScale(float x, float y, float z)
{
    Matrix result = { 
        x, 0.0f, 0.0f, 0.0f,
        0.0f, y, 0.0f, 0.0f,
        0.0f, 0.0f, z, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f };

    return result;
}

// Returns two matrix multiplication
// NOTE: When multiplying matrices... the order matters!
Matrix MatrixMultiply(Matrix lhs, Matrix rhs)
{
    Matrix res;
    res.m00 = lhs.m00 * rhs.m00 + lhs.m01 * rhs.m10 + lhs.m02 * rhs.m20 + lhs.m03 * rhs.m30;
    res.m01 = lhs.m00 * rhs.m01 + lhs.m01 * rhs.m11 + lhs.m02 * rhs.m21 + lhs.m03 * rhs.m31;
    res.m02 = lhs.m00 * rhs.m02 + lhs.m01 * rhs.m12 + lhs.m02 * rhs.m22 + lhs.m03 * rhs.m32;
    res.m03 = lhs.m00 * rhs.m03 + lhs.m01 * rhs.m13 + lhs.m02 * rhs.m23 + lhs.m03 * rhs.m33;
    res.m10 = lhs.m10 * rhs.m00 + lhs.m11 * rhs.m10 + lhs.m12 * rhs.m20 + lhs.m13 * rhs.m30;
    res.m11 = lhs.m10 * rhs.m01 + lhs.m11 * rhs.m11 + lhs.m12 * rhs.m21 + lhs.m13 * rhs.m31;
    res.m12 = lhs.m10 * rhs.m02 + lhs.m11 * rhs.m12 + lhs.m12 * rhs.m22 + lhs.m13 * rhs.m32;
    res.m13 = lhs.m10 * rhs.m03 + lhs.m11 * rhs.m13 + lhs.m12 * rhs.m23 + lhs.m13 * rhs.m33;
    res.m20 = lhs.m20 * rhs.m00 + lhs.m21 * rhs.m10 + lhs.m22 * rhs.m20 + lhs.m23 * rhs.m30;
    res.m21 = lhs.m20 * rhs.m01 + lhs.m21 * rhs.m11 + lhs.m22 * rhs.m21 + lhs.m23 * rhs.m31;
    res.m22 = lhs.m20 * rhs.m02 + lhs.m21 * rhs.m12 + lhs.m22 * rhs.m22 + lhs.m23 * rhs.m32;
    res.m23 = lhs.m20 * rhs.m03 + lhs.m21 * rhs.m13 + lhs.m22 * rhs.m23 + lhs.m23 * rhs.m33;
    res.m30 = lhs.m30 * rhs.m00 + lhs.m31 * rhs.m10 + lhs.m32 * rhs.m20 + lhs.m33 * rhs.m30;
    res.m31 = lhs.m30 * rhs.m01 + lhs.m31 * rhs.m11 + lhs.m32 * rhs.m21 + lhs.m33 * rhs.m31;
    res.m32 = lhs.m30 * rhs.m02 + lhs.m31 * rhs.m12 + lhs.m32 * rhs.m22 + lhs.m33 * rhs.m32;
    res.m33 = lhs.m30 * rhs.m03 + lhs.m31 * rhs.m13 + lhs.m32 * rhs.m23 + lhs.m33 * rhs.m33;
    return res;
}

// Returns perspective projection matrix
Matrix MatrixFrustum(double left, double right, double bottom, double top, double near, double far)
{
    Matrix result;

    float rl = (float)(right - left);
    float tb = (float)(top - bottom);
    float fn = (float)(far - near);

    result.m00 = (float)(near*2.0f) / rl;
    result.m10 = 0.0f;
    result.m20 = 0.0f;
    result.m30 = 0.0f;

    result.m01 = 0.0f;
    result.m11 = (float)(near*2.0f) / tb;
    result.m21 = 0.0f;
    result.m31 = 0.0f;

    result.m02 = (float)(right + left) / rl;
    result.m12 = -(float)(top + bottom) / tb; // negate to reverse Z
    result.m22 = (float)(far + near) / fn;    // negate to reverse Z
    result.m32 = 1.0f;                        // negate to reverse Z

    result.m03 = 0.0f;
    result.m13 = 0.0f;
    result.m23 = -(float)(far*near*2.0f) / fn;
    result.m33 = 0.0f;

    return result;
}

// Returns perspective projection matrix
// NOTE: Angle should be provided in radians
Matrix MatrixPerspective(double fovy, double aspect, double near, double far)
{
    double top = near*tan(fovy*0.5);
    double right = top*aspect;

    return MatrixFrustum(-right, right, -top, top, near, far);
}

// Returns orthographic projection matrix
Matrix MatrixOrtho(double left, double right, double bottom, double top, double near, double far)
{
    Matrix result;

    float rl = (float)(right - left);
    float tb = (float)(top - bottom);
    float fn = (float)(far - near);

    result.m00 = 2.0f / rl;
    result.m10 = 0.0f;
    result.m20 = 0.0f;
    result.m30 = 0.0f;
    result.m01 = 0.0f;
    result.m11 = 2.0f / tb;
    result.m21 = 0.0f;
    result.m31 = 0.0f;
    result.m02 = 0.0f;
    result.m12 = 0.0f;
    result.m22 = -2.0f / fn;
    result.m32 = 0.0f;
    result.m03 = -(float)(left + right) / rl;
    result.m13 = -(float)(top + bottom) / tb;
    result.m23 = -(float)(far + near) / fn;
    result.m33 = 1.0f;

    return result;
}

// Returns camera look-at matrix (view matrix)
Matrix MatrixLookAt(Vector3 eye, Vector3 target, Vector3 up)
{
    Matrix result;

    Vector3 z = Vector3Subtract(eye, target);
    Vector3Normalize(&z);
    Vector3 x = Vector3CrossProduct(up, z);
    Vector3Normalize(&x);
    Vector3 y = Vector3CrossProduct(z, x);
    Vector3Normalize(&y);

    result.m00 = x.x;
    result.m10 = x.y;
    result.m20 = x.z;
    result.m30 = 0.0f;
    result.m01 = y.x;
    result.m11 = y.y;
    result.m21 = y.z;
    result.m31 = 0.0f;
    result.m02 = z.x;
    result.m12 = z.y;
    result.m22 = z.z;
    result.m32 = 0.0f;
    result.m03 = eye.x;
    result.m13 = eye.y;
    result.m23 = eye.z;
    result.m33 = 1.0f;

    result = MatrixInvert(result);

    return result;
}

Matrix MatrixTransform(Transform t)
{
    Quaternion q = t.rotation;

    float x = q.x * 2.0f;
    float y = q.y * 2.0f;
    float z = q.z * 2.0f;
    float xx = q.x * x;
    float yy = q.y * y;
    float zz = q.z * z;
    float xy = q.x * y;
    float xz = q.x * z;
    float yz = q.y * z;
    float wx = q.w * x;
    float wy = q.w * y;
    float wz = q.w * z;

    Vector3 scale = t.scale;
    Matrix m;
    m.m00 = scale.x * (1.0f - (yy + zz));
    m.m10 = scale.x * (xy + wz);
    m.m20 = scale.x * (xz - wy);
    m.m30 = 0.0F;
    m.m01 = scale.y * (xy - wz);
    m.m11 = scale.y * (1.0f - (xx + zz));
    m.m21 = scale.y * (yz + wx);
    m.m31 = 0.0F;
    m.m02 = scale.z * (xz + wy);
    m.m12 = scale.z * (yz - wx);
    m.m22 = scale.z * (1.0f - (xx + yy));
    m.m32 = 0.0F;
    m.m03 = t.position.x;
    m.m13 = t.position.y;
    m.m23 = t.position.z;
    m.m33 = 1.0F;

    return m;
}

// Returns float array of matrix data
float *MatrixToFloat(Matrix mat)
{
    static float buffer[16];

    buffer[0] = mat.m00;
    buffer[1] = mat.m10;
    buffer[2] = mat.m20;
    buffer[3] = mat.m30;
    buffer[4] = mat.m01;
    buffer[5] = mat.m11;
    buffer[6] = mat.m21;
    buffer[7] = mat.m31;
    buffer[8] = mat.m02;
    buffer[9] = mat.m12;
    buffer[10] = mat.m22;
    buffer[11] = mat.m32;
    buffer[12] = mat.m03;
    buffer[13] = mat.m13;
    buffer[14] = mat.m23;
    buffer[15] = mat.m33;

    return buffer;
}

//----------------------------------------------------------------------------------
// Module Functions Definition - Quaternion math
//----------------------------------------------------------------------------------

// Returns identity quaternion
Quaternion QuaternionIdentity(void)
{
    return (Quaternion) { 0.0f, 0.0f, 0.0f, 1.0f };
}

// Computes the length of a quaternion
float QuaternionLength(Quaternion quat)
{
    return sqrtf(quat.x*quat.x + quat.y*quat.y + quat.z*quat.z + quat.w*quat.w);
}

// Normalize provided quaternion
void QuaternionNormalize(Quaternion *q)
{
    float length, ilength;

    length = QuaternionLength(*q);

    if (length == 0.0f) length = 1.0f;

    ilength = 1.0f / length;

    q->x *= ilength;
    q->y *= ilength;
    q->z *= ilength;
    q->w *= ilength;
}

// Invert provided quaternion
void QuaternionInvert(Quaternion *quat)
{
    float lengthSq = quat->x*quat->x + quat->y*quat->y + quat->z*quat->z + quat->w*quat->w;

    if (lengthSq != 0.0)
    {
        float length = sqrtf(lengthSq);
        float i = 1.0f / length;

        quat->x *= -i;
        quat->y *= -i;
        quat->z *= -i;
        quat->w *= i;
    }
}

// Calculate two quaternion multiplication
Quaternion QuaternionMultiply(Quaternion q1, Quaternion q2)
{
    Quaternion result;

    float lhx = q1.x, lhy = q1.y, lhz = q1.z, lhw = q1.w;
    float rhx = q2.x, rhy = q2.y, rhz = q2.z, rhw = q2.w;

    result.x = lhw*rhx + lhx*rhw + lhy*rhz - lhz*rhy;
    result.y = lhw*rhy + lhy*rhw + lhz*rhx - lhx*rhz;
    result.z = lhw*rhz + lhz*rhw + lhx*rhy - lhy*rhx;
    result.w = lhw*rhw - lhx*rhx - lhy*rhy - lhz*rhz;

    return result;
}

// Calculate linear interpolation between two quaternions
Quaternion QuaternionLerp(Quaternion q1, Quaternion q2, float amount)
{
    Quaternion result;

    result.x = q1.x + amount*(q2.x - q1.x);
    result.y = q1.y + amount*(q2.y - q1.y);
    result.z = q1.z + amount*(q2.z - q1.z);
    result.w = q1.w + amount*(q2.w - q1.w);

    return result;
}

// Calculates spherical linear interpolation between two quaternions
Quaternion QuaternionSlerp(Quaternion q1, Quaternion q2, float amount)
{
    Quaternion result;

    float cosHalfTheta = q1.x*q2.x + q1.y*q2.y + q1.z*q2.z + q1.w*q2.w;

    if (fabs(cosHalfTheta) >= 1.0f) result = q1;
    else if (cosHalfTheta > 0.95f) result = QuaternionNlerp(q1, q2, amount);
    else
    {
        float halfTheta = acosf(cosHalfTheta);
        float sinHalfTheta = sqrtf(1.0f - cosHalfTheta*cosHalfTheta);

        if (fabs(sinHalfTheta) < 0.001f)
        {
            result.x = (q1.x*0.5f + q2.x*0.5f);
            result.y = (q1.y*0.5f + q2.y*0.5f);
            result.z = (q1.z*0.5f + q2.z*0.5f);
            result.w = (q1.w*0.5f + q2.w*0.5f);
        }
        else
        {
            float ratioA = sinf((1 - amount)*halfTheta) / sinHalfTheta;
            float ratioB = sinf(amount*halfTheta) / sinHalfTheta;

            result.x = (q1.x*ratioA + q2.x*ratioB);
            result.y = (q1.y*ratioA + q2.y*ratioB);
            result.z = (q1.z*ratioA + q2.z*ratioB);
            result.w = (q1.w*ratioA + q2.w*ratioB);
        }
    }

    return result;
}

// Calculate slerp-optimized interpolation between two quaternions
Quaternion QuaternionNlerp(Quaternion q1, Quaternion q2, float amount)
{
    Quaternion result = QuaternionLerp(q1, q2, amount);
    QuaternionNormalize(&result);

    return result;
}

// Calculate quaternion based on the rotation from one vector to another
Quaternion QuaternionFromVector3ToVector3(Vector3 from, Vector3 to)
{
    Quaternion q = { 0 };

    float cos2Theta = Vector3DotProduct(from, to);
    Vector3 cross = Vector3CrossProduct(from, to);

    q.x = cross.x;
    q.y = cross.y;
    q.z = cross.y;
    q.w = 1.0f + cos2Theta;     // NOTE: Added QuaternioIdentity()

                                // Normalize to essentially nlerp the original and identity to 0.5
    QuaternionNormalize(&q);

    // Above lines are equivalent to:
    //Quaternion result = QuaternionNlerp(q, QuaternionIdentity(), 0.5f);

    return q;
}

// Returns a quaternion for a given rotation matrix
Quaternion QuaternionFromMatrix(Matrix matrix)
{
    Quaternion result;

    float trace = MatrixTrace(matrix);

    if (trace > 0.0f)
    {
        float s = sqrtf(trace + 1)*2.0f;
        float invS = 1.0f / s;

        result.w = s*0.25f;
        result.x = (matrix.m21 - matrix.m12)*invS;
        result.y = (matrix.m02 - matrix.m20)*invS;
        result.z = (matrix.m10 - matrix.m01)*invS;
    }
    else
    {
        float m00 = matrix.m00, m11 = matrix.m11, m22 = matrix.m22;

        if (m00 > m11 && m00 > m22)
        {
            float s = sqrtf(1.0f + m00 - m11 - m22)*2.0f;
            float invS = 1.0f / s;

            result.w = (matrix.m21 - matrix.m12)*invS;
            result.x = s*0.25f;
            result.y = (matrix.m01 + matrix.m10)*invS;
            result.z = (matrix.m02 + matrix.m20)*invS;
        }
        else if (m11 > m22)
        {
            float s = sqrtf(1.0f + m11 - m00 - m22)*2.0f;
            float invS = 1.0f / s;

            result.w = (matrix.m02 - matrix.m20)*invS;
            result.x = (matrix.m01 + matrix.m10)*invS;
            result.y = s*0.25f;
            result.z = (matrix.m12 + matrix.m21)*invS;
        }
        else
        {
            float s = sqrtf(1.0f + m22 - m00 - m11)*2.0f;
            float invS = 1.0f / s;

            result.w = (matrix.m10 - matrix.m01)*invS;
            result.x = (matrix.m02 + matrix.m20)*invS;
            result.y = (matrix.m12 + matrix.m21)*invS;
            result.z = s*0.25f;
        }
    }

    return result;
}

// Returns a matrix for a given quaternion
Matrix QuaternionToMatrix(Quaternion q)
{
    // Precalculate coordinate products
    float x = q.x * 2.0f;
    float y = q.y * 2.0f;
    float z = q.z * 2.0f;
    float xx = q.x * x;
    float yy = q.y * y;
    float zz = q.z * z;
    float xy = q.x * y;
    float xz = q.x * z;
    float yz = q.y * z;
    float wx = q.w * x;
    float wy = q.w * y;
    float wz = q.w * z;

    // Calculate 3x3 matrix from orthonormal basis
    Matrix m;
    m.m00 = 1.0f - (yy + zz); m.m10 = xy + wz; m.m20 = xz - wy; m.m30 = 0.0F;
    m.m01 = xy - wz; m.m11 = 1.0f - (xx + zz); m.m21 = yz + wx; m.m31 = 0.0F;
    m.m02 = xz + wy; m.m12 = yz - wx; m.m22 = 1.0f - (xx + yy); m.m32 = 0.0F;
    m.m03 = 0.0F; m.m13 = 0.0F; m.m23 = 0.0F; m.m33 = 1.0F;
    return m;
}

// Returns rotation quaternion for an angle and axis
// NOTE: angle must be provided in radians
Quaternion QuaternionFromAxisAngle(Vector3 axis, float angle)
{
    Quaternion result = { 0.0f, 0.0f, 0.0f, 1.0f };

    if (Vector3Length(axis) != 0.0f)

        angle *= 0.5f;

    Vector3Normalize(&axis);

    float sinres = sinf(angle);
    float cosres = cosf(angle);

    result.x = axis.x*sinres;
    result.y = axis.y*sinres;
    result.z = axis.z*sinres;
    result.w = cosres;

    QuaternionNormalize(&result);

    return result;
}

// Returns the rotation angle and axis for a given quaternion
void QuaternionToAxisAngle(Quaternion q, Vector3 *outAxis, float *outAngle)
{
    if (fabs(q.w) > 1.0f) QuaternionNormalize(&q);

    Vector3 resAxis = { 0.0f, 0.0f, 0.0f };
    float resAngle = 0.0f;

    resAngle = 2.0f*acosf(q.w);
    float den = sqrtf(1.0f - q.w*q.w);

    if (den > 0.0001f)
    {
        resAxis.x = q.x / den;
        resAxis.y = q.y / den;
        resAxis.z = q.z / den;
    }
    else
    {
        // This occurs when the angle is zero.
        // Not a problem: just set an arbitrary normalized axis.
        resAxis.x = 1.0f;
    }

    *outAxis = resAxis;
    *outAngle = resAngle;
}

// Returns he quaternion equivalent to Euler angles
Quaternion QuaternionFromEuler(float roll, float pitch, float yaw)
{
    Quaternion q = { 0 };

    roll *= DEG2RAD;
    pitch *= DEG2RAD;
    yaw *= DEG2RAD;

    float x0 = cosf(roll*0.5f);
    float x1 = sinf(roll*0.5f);
    float y0 = cosf(pitch*0.5f);
    float y1 = sinf(pitch*0.5f);
    float z0 = cosf(yaw*0.5f);
    float z1 = sinf(yaw*0.5f);

    q.x = x1*y0*z0 - x0*y1*z1;
    q.y = x0*y1*z0 + x1*y0*z1;
    q.z = x0*y0*z1 - x1*y1*z0;
    q.w = x0*y0*z0 + x1*y1*z1;

    // TODO: This works differently than in Unity. Sometimes rotation flips around.
    //QuaternionNormalize(&q);

    return q;
}

//Quaternion QuaternionFromEuler(float bank, float heading, float attitude)
//{
//    bank *= DEG2RAD;
//    heading *= DEG2RAD;
//    attitude *= DEG2RAD;
//
//    // Assuming the angles are in radians.
//    float c1 = cosf(heading);
//    float s1 = sinf(heading);
//    float c2 = cosf(attitude);
//    float s2 = sinf(attitude);
//    float c3 = cosf(bank);
//    float s3 = sinf(bank);
//
//    Quaternion q;
//    q.w = sqrtf(1.0f + c1 * c2 + c1*c3 - s1 * s2 * s3 + c2*c3) / 2.0f;
//    float w4 = (4.0f * q.w);
//    q.x = (c2 * s3 + c1 * s3 + s1 * s2 * c3) / w4;
//    q.y = (s1 * c2 + s1 * c3 + c1 * s2 * s3) / w4;
//    q.z = (-s1 * s3 + c1 * s2 * c3 + s2) / w4;
//
//    return q;
//}

// Return the Euler angles equivalent to quaternion (roll, pitch, yaw)
// NOTE: Angles are returned in a Vector3 struct in degrees
Vector3 QuaternionToEuler(Quaternion q)
{
    Vector3 v = { 0 };

    // roll (x-axis rotation)
    float x0 = 2.0f*(q.w*q.x + q.y*q.z);
    float x1 = 1.0f - 2.0f*(q.x*q.x + q.y*q.y);
    v.x = atan2f(x0, x1)*RAD2DEG;

    // pitch (y-axis rotation)
    float y0 = 2.0f*(q.w*q.y - q.z*q.x);
    y0 = y0 > 1.0f ? 1.0f : y0;
    y0 = y0 < -1.0f ? -1.0f : y0;
    v.y = asinf(y0)*RAD2DEG;

    // yaw (z-axis rotation)
    float z0 = 2.0f*(q.w*q.z + q.x*q.y);
    float z1 = 1.0f - 2.0f*(q.y*q.y + q.z*q.z);
    v.z = atan2f(z0, z1)*RAD2DEG;

    return v;
}

// Transform a quaternion given a transformation matrix
void QuaternionTransform(Quaternion *q, Matrix mat)
{
    float x = q->x;
    float y = q->y;
    float z = q->z;
    float w = q->w;

    q->x = mat.m00*x + mat.m01*y + mat.m02*z + mat.m03*w;
    q->y = mat.m10*x + mat.m11*y + mat.m12*z + mat.m13*w;
    q->z = mat.m20*x + mat.m21*y + mat.m22*z + mat.m23*w;
    q->w = mat.m30*x + mat.m31*y + mat.m32*z + mat.m33*w;
}
