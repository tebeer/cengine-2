#ifndef _MATH_H_
#define _MATH_H_

#ifndef PI
#define PI 3.14159265358979323846f
#endif

#ifndef DEG2RAD
#define DEG2RAD (PI/180.0f)
#endif

#ifndef RAD2DEG
#define RAD2DEG (180.0f/PI)
#endif

typedef struct Vector2
{
    float x;
    float y;
} Vector2;

typedef struct Vector3
{
    float x;
    float y;
    float z;
} Vector3;

typedef struct Matrix
{
    float m00, m01, m02, m03;
    float m10, m11, m12, m13;
    float m20, m21, m22, m23;
    float m30, m31, m32, m33;
} Matrix;

typedef struct Quaternion
{
    float x;
    float y;
    float z;
    float w;
} Quaternion;

typedef struct Transform
{
    Vector3 position;
    Quaternion rotation;
    Vector3 scale;
} Transform;

#ifdef __cplusplus
extern "C"
{
#endif

//------------------------------------------------------------------------------------
// Functions Declaration - math utils
//------------------------------------------------------------------------------------
float Clamp(float value, float min, float max);           // Clamp float value

                                                          //------------------------------------------------------------------------------------
                                                          // Functions Declaration to work with Vector2
                                                          //------------------------------------------------------------------------------------
Vector2 Vector2Zero(void);                                // Vector with components value 0.0f
Vector2 Vector2One(void);                                 // Vector with components value 1.0f
Vector2 Vector2Add(Vector2 v1, Vector2 v2);               // Add two vectors (v1 + v2)
Vector2 Vector2Subtract(Vector2 v1, Vector2 v2);          // Subtract two vectors (v1 - v2)
float Vector2Length(Vector2 v);                           // Calculate vector length
float Vector2DotProduct(Vector2 v1, Vector2 v2);          // Calculate two vectors dot product
float Vector2Distance(Vector2 v1, Vector2 v2);            // Calculate distance between two vectors
float Vector2Angle(Vector2 v1, Vector2 v2);               // Calculate angle between two vectors in X-axis
void Vector2Scale(Vector2 *v, float scale);               // Scale vector (multiply by value)
void Vector2Negate(Vector2 *v);                           // Negate vector
void Vector2Divide(Vector2 *v, float div);                // Divide vector by a float value
void Vector2Normalize(Vector2 *v);                        // Normalize provided vector

                                                          //------------------------------------------------------------------------------------
                                                          // Functions Declaration to work with Vector3
                                                          //------------------------------------------------------------------------------------
Vector3 Vector3Zero(void);                                 // Vector with components value 0.0f
Vector3 Vector3One(void);                                  // Vector with components value 1.0f
Vector3 Vector3Add(Vector3 v1, Vector3 v2);                // Add two vectors
Vector3 Vector3Multiply(Vector3 v, float scalar);          // Multiply vector by scalar
Vector3 Vector3MultiplyV(Vector3 v1, Vector3 v2);          // Multiply vector by vector
Vector3 Vector3Subtract(Vector3 v1, Vector3 v2);           // Substract two vectors
Vector3 Vector3CrossProduct(Vector3 v1, Vector3 v2);       // Calculate two vectors cross product
Vector3 Vector3Perpendicular(Vector3 v);                   // Calculate one vector perpendicular vector
float Vector3Length(const Vector3 v);                      // Calculate vector length
float Vector3DotProduct(Vector3 v1, Vector3 v2);           // Calculate two vectors dot product
float Vector3Distance(Vector3 v1, Vector3 v2);             // Calculate distance between two points
void Vector3Scale(Vector3 *v, float scale);                // Scale provided vector
void Vector3Negate(Vector3 *v);                            // Negate provided vector (invert direction)
void Vector3Normalize(Vector3 *v);                         // Normalize provided vector
void Vector3Transform(Vector3 *v, Matrix mat);             // Transforms a Vector3 by a given Matrix
Vector3 Vector3Lerp(Vector3 v1, Vector3 v2, float amount); // Calculate linear interpolation between two vectors
Vector3 Vector3Reflect(Vector3 vector, Vector3 normal);    // Calculate reflected vector to normal
Vector3 Vector3Min(Vector3 vec1, Vector3 vec2);            // Return min value for each pair of components
Vector3 Vector3Max(Vector3 vec1, Vector3 vec2);            // Return max value for each pair of components
Vector3 Vector3Barycenter(Vector3 p, Vector3 a, Vector3 b, Vector3 c); // Barycenter coords for p in triangle abc
float *Vector3ToFloat(Vector3 vec);                        // Returns Vector3 as float array
Vector3 Vector3RotateByQuaternion(Vector3 v, Quaternion q);

                                                           //------------------------------------------------------------------------------------
                                                           // Functions Declaration to work with Matrix
                                                           //------------------------------------------------------------------------------------
float MatrixDeterminant(Matrix mat);                      // Compute matrix determinant
float MatrixTrace(Matrix mat);                            // Returns the trace of the matrix (sum of the values along the diagonal)
void MatrixTranspose(Matrix *mat);                        // Transposes provided matrix
Matrix MatrixInvert(const Matrix mat);                           // Invert provided matrix
void MatrixNormalize(Matrix *mat);                        // Normalize provided matrix
Matrix MatrixIdentity(void);                              // Returns identity matrix
Matrix MatrixAdd(Matrix left, Matrix right);              // Add two matrices
Matrix MatrixSubstract(Matrix left, Matrix right);        // Substract two matrices (left - right)
Matrix MatrixTranslate(float x, float y, float z);        // Returns translation matrix
Matrix MatrixRotate(Vector3 axis, float angle);           // Returns rotation matrix for an angle around an specified axis (angle in radians)
Matrix MatrixRotateX(float angle);                        // Returns x-rotation matrix (angle in radians)
Matrix MatrixRotateY(float angle);                        // Returns y-rotation matrix (angle in radians)
Matrix MatrixRotateZ(float angle);                        // Returns z-rotation matrix (angle in radians)
Matrix MatrixScale(float x, float y, float z);            // Returns scaling matrix
Matrix MatrixMultiply(Matrix left, Matrix right);         // Returns two matrix multiplication
Matrix MatrixFrustum(double left, double right, double bottom, double top, double near, double far);  // Returns perspective projection matrix
Matrix MatrixPerspective(double fovy, double aspect, double near, double far);                        // Returns perspective projection matrix
Matrix MatrixOrtho(double left, double right, double bottom, double top, double near, double far);    // Returns orthographic projection matrix
Matrix MatrixLookAt(Vector3 position, Vector3 target, Vector3 up);  // Returns camera look-at matrix (view matrix)
Matrix MatrixTransform(Transform t);
float *MatrixToFloat(Matrix mat);                         // Returns float array of Matrix data

                                                          //------------------------------------------------------------------------------------
                                                          // Functions Declaration to work with Quaternions
                                                          //------------------------------------------------------------------------------------
Quaternion QuaternionIdentity(void);                      // Returns identity quaternion
float QuaternionLength(Quaternion quat);                  // Compute the length of a quaternion
void QuaternionNormalize(Quaternion *q);                  // Normalize provided quaternion
void QuaternionInvert(Quaternion *quat);                  // Invert provided quaternion
Quaternion QuaternionMultiply(Quaternion q1, Quaternion q2);    // Calculate two quaternion multiplication
Quaternion QuaternionLerp(Quaternion q1, Quaternion q2, float amount);    // Calculate linear interpolation between two quaternions
Quaternion QuaternionSlerp(Quaternion q1, Quaternion q2, float amount);   // Calculates spherical linear interpolation between two quaternions
Quaternion QuaternionNlerp(Quaternion q1, Quaternion q2, float amount);   // Calculate slerp-optimized interpolation between two quaternions
Quaternion QuaternionFromVector3ToVector3(Vector3 from, Vector3 to);      // Calculate quaternion based on the rotation from one vector to another
Quaternion QuaternionFromMatrix(Matrix matrix);                 // Returns a quaternion for a given rotation matrix
Matrix QuaternionToMatrix(Quaternion q);                        // Returns a matrix for a given quaternion
Quaternion QuaternionFromAxisAngle(Vector3 axis, float angle);  // Returns rotation quaternion for an angle and axis
void QuaternionToAxisAngle(Quaternion q, Vector3 *outAxis, float *outAngle);  // Returns the rotation angle and axis for a given quaternion
Quaternion QuaternionFromEuler(float roll, float pitch, float yaw);           // Returns he quaternion equivalent to Euler angles
Vector3 QuaternionToEuler(Quaternion q);                  // Return the Euler angles equivalent to quaternion (roll, pitch, yaw)
void QuaternionTransform(Quaternion *q, Matrix mat);      // Transform a quaternion given a transformation matrix

#ifdef __cplusplus
}
#endif

#endif