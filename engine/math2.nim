type
  Vector2* {.importc: "Vector2", header: "math2.h", bycopy.} = object
    x* {.importc: "x".}: cfloat
    y* {.importc: "y".}: cfloat

  Vector3* {.importc: "Vector3", header: "math2.h", bycopy.} = object
    x* {.importc: "x".}: cfloat
    y* {.importc: "y".}: cfloat
    z* {.importc: "z".}: cfloat

  Matrix* {.importc: "Matrix", header: "math2.h", bycopy.} = object
    m00* {.importc: "m00".}: cfloat
    m01* {.importc: "m01".}: cfloat
    m02* {.importc: "m02".}: cfloat
    m03* {.importc: "m03".}: cfloat
    m10* {.importc: "m10".}: cfloat
    m11* {.importc: "m11".}: cfloat
    m12* {.importc: "m12".}: cfloat
    m13* {.importc: "m13".}: cfloat
    m20* {.importc: "m20".}: cfloat
    m21* {.importc: "m21".}: cfloat
    m22* {.importc: "m22".}: cfloat
    m23* {.importc: "m23".}: cfloat
    m30* {.importc: "m30".}: cfloat
    m31* {.importc: "m31".}: cfloat
    m32* {.importc: "m32".}: cfloat
    m33* {.importc: "m33".}: cfloat

  Quaternion* {.importc: "Quaternion", header: "math2.h", bycopy.} = object
    x* {.importc: "x".}: cfloat
    y* {.importc: "y".}: cfloat
    z* {.importc: "z".}: cfloat
    w* {.importc: "w".}: cfloat

  Transform* {.importc: "Transform", header: "math2.h", bycopy.} = object
    position* {.importc: "position".}: Vector3
    rotation* {.importc: "rotation".}: Quaternion
    scale* {.importc: "scale".}: Vector3


## ------------------------------------------------------------------------------------
##  Functions Declaration - math utils
## ------------------------------------------------------------------------------------

proc Clamp*(value: cfloat; min: cfloat; max: cfloat): cfloat {.cdecl, importc: "Clamp",
    header: "math2.h".}
##  Clamp float value
## ------------------------------------------------------------------------------------
##  Functions Declaration to work with Vector2
## ------------------------------------------------------------------------------------

proc Vector2Zero*(): Vector2 {.cdecl, importc: "Vector2Zero", header: "math2.h".}
##  Vector with components value 0.0f

proc Vector2One*(): Vector2 {.cdecl, importc: "Vector2One", header: "math2.h".}
##  Vector with components value 1.0f

proc Vector2Add*(v1: Vector2; v2: Vector2): Vector2 {.cdecl, importc: "Vector2Add",
    header: "math2.h".}
##  Add two vectors (v1 + v2)

proc Vector2Subtract*(v1: Vector2; v2: Vector2): Vector2 {.cdecl,
    importc: "Vector2Subtract", header: "math2.h".}
##  Subtract two vectors (v1 - v2)

proc Vector2Length*(v: Vector2): cfloat {.cdecl, importc: "Vector2Length",
                                      header: "math2.h".}
##  Calculate vector length

proc Vector2DotProduct*(v1: Vector2; v2: Vector2): cfloat {.cdecl,
    importc: "Vector2DotProduct", header: "math2.h".}
##  Calculate two vectors dot product

proc Vector2Distance*(v1: Vector2; v2: Vector2): cfloat {.cdecl,
    importc: "Vector2Distance", header: "math2.h".}
##  Calculate distance between two vectors

proc Vector2Angle*(v1: Vector2; v2: Vector2): cfloat {.cdecl, importc: "Vector2Angle",
    header: "math2.h".}
##  Calculate angle between two vectors in X-axis

proc Vector2Scale*(v: ptr Vector2; scale: cfloat) {.cdecl, importc: "Vector2Scale",
    header: "math2.h".}
##  Scale vector (multiply by value)

proc Vector2Negate*(v: ptr Vector2) {.cdecl, importc: "Vector2Negate",
                                  header: "math2.h".}
##  Negate vector

proc Vector2Divide*(v: ptr Vector2; `div`: cfloat) {.cdecl, importc: "Vector2Divide",
    header: "math2.h".}
##  Divide vector by a float value

proc Vector2Normalize*(v: ptr Vector2) {.cdecl, importc: "Vector2Normalize",
                                     header: "math2.h".}
##  Normalize provided vector
## ------------------------------------------------------------------------------------
##  Functions Declaration to work with Vector3
## ------------------------------------------------------------------------------------

proc Vector3Zero*(): Vector3 {.cdecl, importc: "Vector3Zero", header: "math2.h".}
##  Vector with components value 0.0f

proc Vector3One*(): Vector3 {.cdecl, importc: "Vector3One", header: "math2.h".}
##  Vector with components value 1.0f

proc Vector3Add*(v1: Vector3; v2: Vector3): Vector3 {.cdecl, importc: "Vector3Add",
    header: "math2.h".}
##  Add two vectors

proc Vector3Multiply*(v: Vector3; scalar: cfloat): Vector3 {.cdecl,
    importc: "Vector3Multiply", header: "math2.h".}
##  Multiply vector by scalar

proc Vector3MultiplyV*(v1: Vector3; v2: Vector3): Vector3 {.cdecl,
    importc: "Vector3MultiplyV", header: "math2.h".}
##  Multiply vector by vector

proc Vector3Subtract*(v1: Vector3; v2: Vector3): Vector3 {.cdecl,
    importc: "Vector3Subtract", header: "math2.h".}
##  Substract two vectors

proc Vector3CrossProduct*(v1: Vector3; v2: Vector3): Vector3 {.cdecl,
    importc: "Vector3CrossProduct", header: "math2.h".}
##  Calculate two vectors cross product

proc Vector3Perpendicular*(v: Vector3): Vector3 {.cdecl,
    importc: "Vector3Perpendicular", header: "math2.h".}
##  Calculate one vector perpendicular vector

proc Vector3Length*(v: Vector3): cfloat {.cdecl, importc: "Vector3Length",
                                      header: "math2.h".}
##  Calculate vector length

proc Vector3DotProduct*(v1: Vector3; v2: Vector3): cfloat {.cdecl,
    importc: "Vector3DotProduct", header: "math2.h".}
##  Calculate two vectors dot product

proc Vector3Distance*(v1: Vector3; v2: Vector3): cfloat {.cdecl,
    importc: "Vector3Distance", header: "math2.h".}
##  Calculate distance between two points

proc Vector3Scale*(v: ptr Vector3; scale: cfloat) {.cdecl, importc: "Vector3Scale",
    header: "math2.h".}
##  Scale provided vector

proc Vector3Negate*(v: ptr Vector3) {.cdecl, importc: "Vector3Negate",
                                  header: "math2.h".}
##  Negate provided vector (invert direction)

proc Vector3Normalize*(v: ptr Vector3) {.cdecl, importc: "Vector3Normalize",
                                     header: "math2.h".}
##  Normalize provided vector

proc Vector3Transform*(v: ptr Vector3; mat: Matrix) {.cdecl,
    importc: "Vector3Transform", header: "math2.h".}
##  Transforms a Vector3 by a given Matrix

proc Vector3Lerp*(v1: Vector3; v2: Vector3; amount: cfloat): Vector3 {.cdecl,
    importc: "Vector3Lerp", header: "math2.h".}
##  Calculate linear interpolation between two vectors

proc Vector3Reflect*(vector: Vector3; normal: Vector3): Vector3 {.cdecl,
    importc: "Vector3Reflect", header: "math2.h".}
##  Calculate reflected vector to normal

proc Vector3Min*(vec1: Vector3; vec2: Vector3): Vector3 {.cdecl, importc: "Vector3Min",
    header: "math2.h".}
##  Return min value for each pair of components

proc Vector3Max*(vec1: Vector3; vec2: Vector3): Vector3 {.cdecl, importc: "Vector3Max",
    header: "math2.h".}
##  Return max value for each pair of components

proc Vector3Barycenter*(p: Vector3; a: Vector3; b: Vector3; c: Vector3): Vector3 {.cdecl,
    importc: "Vector3Barycenter", header: "math2.h".}
##  Barycenter coords for p in triangle abc

proc Vector3ToFloat*(vec: Vector3): ptr cfloat {.cdecl, importc: "Vector3ToFloat",
    header: "math2.h".}
##  Returns Vector3 as float array

proc Vector3RotateByQuaternion*(v: Vector3; q: Quaternion): Vector3 {.cdecl,
    importc: "Vector3RotateByQuaternion", header: "math2.h".}
## ------------------------------------------------------------------------------------
##  Functions Declaration to work with Matrix
## ------------------------------------------------------------------------------------

proc MatrixDeterminant*(mat: Matrix): cfloat {.cdecl, importc: "MatrixDeterminant",
    header: "math2.h".}
##  Compute matrix determinant

proc MatrixTrace*(mat: Matrix): cfloat {.cdecl, importc: "MatrixTrace",
                                     header: "math2.h".}
##  Returns the trace of the matrix (sum of the values along the diagonal)

proc MatrixTranspose*(mat: ptr Matrix) {.cdecl, importc: "MatrixTranspose",
                                     header: "math2.h".}
##  Transposes provided matrix

proc MatrixInvert*(mat: Matrix): Matrix {.cdecl, importc: "MatrixInvert",
                                      header: "math2.h".}
##  Invert provided matrix

proc MatrixNormalize*(mat: ptr Matrix) {.cdecl, importc: "MatrixNormalize",
                                     header: "math2.h".}
##  Normalize provided matrix

proc MatrixIdentity*(): Matrix {.cdecl, importc: "MatrixIdentity", header: "math2.h".}
##  Returns identity matrix

proc MatrixAdd*(left: Matrix; right: Matrix): Matrix {.cdecl, importc: "MatrixAdd",
    header: "math2.h".}
##  Add two matrices

proc MatrixSubstract*(left: Matrix; right: Matrix): Matrix {.cdecl,
    importc: "MatrixSubstract", header: "math2.h".}
##  Substract two matrices (left - right)

proc MatrixTranslate*(x: cfloat; y: cfloat; z: cfloat): Matrix {.cdecl,
    importc: "MatrixTranslate", header: "math2.h".}
##  Returns translation matrix

proc MatrixRotate*(axis: Vector3; angle: cfloat): Matrix {.cdecl,
    importc: "MatrixRotate", header: "math2.h".}
##  Returns rotation matrix for an angle around an specified axis (angle in radians)

proc MatrixRotateX*(angle: cfloat): Matrix {.cdecl, importc: "MatrixRotateX",
    header: "math2.h".}
##  Returns x-rotation matrix (angle in radians)

proc MatrixRotateY*(angle: cfloat): Matrix {.cdecl, importc: "MatrixRotateY",
    header: "math2.h".}
##  Returns y-rotation matrix (angle in radians)

proc MatrixRotateZ*(angle: cfloat): Matrix {.cdecl, importc: "MatrixRotateZ",
    header: "math2.h".}
##  Returns z-rotation matrix (angle in radians)

proc MatrixScale*(x: cfloat; y: cfloat; z: cfloat): Matrix {.cdecl,
    importc: "MatrixScale", header: "math2.h".}
##  Returns scaling matrix

proc MatrixMultiply*(left: Matrix; right: Matrix): Matrix {.cdecl,
    importc: "MatrixMultiply", header: "math2.h".}
##  Returns two matrix multiplication

proc MatrixFrustum*(left: cdouble; right: cdouble; bottom: cdouble; top: cdouble;
                   near: cdouble; far: cdouble): Matrix {.cdecl,
    importc: "MatrixFrustum", header: "math2.h".}
##  Returns perspective projection matrix

proc MatrixPerspective*(fovy: cdouble; aspect: cdouble; near: cdouble; far: cdouble): Matrix {.
    cdecl, importc: "MatrixPerspective", header: "math2.h".}
##  Returns perspective projection matrix

proc MatrixOrtho*(left: cdouble; right: cdouble; bottom: cdouble; top: cdouble;
                 near: cdouble; far: cdouble): Matrix {.cdecl, importc: "MatrixOrtho",
    header: "math2.h".}
##  Returns orthographic projection matrix

proc MatrixLookAt*(position: Vector3; target: Vector3; up: Vector3): Matrix {.cdecl,
    importc: "MatrixLookAt", header: "math2.h".}
##  Returns camera look-at matrix (view matrix)

proc MatrixTransform*(t: Transform): Matrix {.cdecl, importc: "MatrixTransform",
    header: "math2.h".}
proc MatrixToFloat*(mat: Matrix): ptr cfloat {.cdecl, importc: "MatrixToFloat",
    header: "math2.h".}
##  Returns float array of Matrix data
## ------------------------------------------------------------------------------------
##  Functions Declaration to work with Quaternions
## ------------------------------------------------------------------------------------

proc QuaternionIdentity*(): Quaternion {.cdecl, importc: "QuaternionIdentity",
                                      header: "math2.h".}
##  Returns identity quaternion

proc QuaternionLength*(quat: Quaternion): cfloat {.cdecl,
    importc: "QuaternionLength", header: "math2.h".}
##  Compute the length of a quaternion

proc QuaternionNormalize*(q: ptr Quaternion) {.cdecl, importc: "QuaternionNormalize",
    header: "math2.h".}
##  Normalize provided quaternion

proc QuaternionInvert*(quat: ptr Quaternion) {.cdecl, importc: "QuaternionInvert",
    header: "math2.h".}
##  Invert provided quaternion

proc QuaternionMultiply*(q1: Quaternion; q2: Quaternion): Quaternion {.cdecl,
    importc: "QuaternionMultiply", header: "math2.h".}
##  Calculate two quaternion multiplication

proc QuaternionLerp*(q1: Quaternion; q2: Quaternion; amount: cfloat): Quaternion {.
    cdecl, importc: "QuaternionLerp", header: "math2.h".}
##  Calculate linear interpolation between two quaternions

proc QuaternionSlerp*(q1: Quaternion; q2: Quaternion; amount: cfloat): Quaternion {.
    cdecl, importc: "QuaternionSlerp", header: "math2.h".}
##  Calculates spherical linear interpolation between two quaternions

proc QuaternionNlerp*(q1: Quaternion; q2: Quaternion; amount: cfloat): Quaternion {.
    cdecl, importc: "QuaternionNlerp", header: "math2.h".}
##  Calculate slerp-optimized interpolation between two quaternions

proc QuaternionFromVector3ToVector3*(`from`: Vector3; to: Vector3): Quaternion {.
    cdecl, importc: "QuaternionFromVector3ToVector3", header: "math2.h".}
##  Calculate quaternion based on the rotation from one vector to another

proc QuaternionFromMatrix*(matrix: Matrix): Quaternion {.cdecl,
    importc: "QuaternionFromMatrix", header: "math2.h".}
##  Returns a quaternion for a given rotation matrix

proc QuaternionToMatrix*(q: Quaternion): Matrix {.cdecl,
    importc: "QuaternionToMatrix", header: "math2.h".}
##  Returns a matrix for a given quaternion

proc QuaternionFromAxisAngle*(axis: Vector3; angle: cfloat): Quaternion {.cdecl,
    importc: "QuaternionFromAxisAngle", header: "math2.h".}
##  Returns rotation quaternion for an angle and axis

proc QuaternionToAxisAngle*(q: Quaternion; outAxis: ptr Vector3; outAngle: ptr cfloat) {.
    cdecl, importc: "QuaternionToAxisAngle", header: "math2.h".}
##  Returns the rotation angle and axis for a given quaternion

proc QuaternionFromEuler*(roll: cfloat; pitch: cfloat; yaw: cfloat): Quaternion {.cdecl,
    importc: "QuaternionFromEuler", header: "math2.h".}
##  Returns he quaternion equivalent to Euler angles

proc QuaternionToEuler*(q: Quaternion): Vector3 {.cdecl,
    importc: "QuaternionToEuler", header: "math2.h".}
##  Return the Euler angles equivalent to quaternion (roll, pitch, yaw)

proc QuaternionTransform*(q: ptr Quaternion; mat: Matrix) {.cdecl,
    importc: "QuaternionTransform", header: "math2.h".}
##  Transform a quaternion given a transformation matrix


proc `*`*(m1: Matrix, m2: Matrix): Matrix {.inline.} = 
    result = MatrixMultiply(m1, m2)

proc `*`*(m: Matrix, v: Vector3): Vector3 {.inline.} = 
    Vector3Transform(unsafeAddr v, m)
    result = v

proc `*`*(q: Quaternion, v: Vector3): Vector3 {.inline.} = 
    result = Vector3RotateByQuaternion(v, q)

proc `*`*(v: Vector3, f:float): Vector3 {.inline.} = 
    result = Vector3Multiply(v, f)

proc `+`*(v1: Vector3, v2: Vector3): Vector3 {.inline.} = 
    result = Vector3Add(v1, v2)

