#include "memory.h"
#include "util.h"
#include <stdlib.h>
#include <string.h>
#include "print.h"
#include "platform.h"


#define ARENA_SIZE_LIMIT 0

static Arena _arena = { 0 };

void* Memory_Alloc(unsigned int size)
{
    void* mem = calloc(size, 1);
    ASSERT(mem != NULL, "Memory allocation failed for %i bytes.", size);
    return mem;
}

void* Memory_Realloc(void* block, unsigned int size)
{
    return realloc(block, size);
}

void Arena_Create(unsigned int size, Arena* arena)
{
    DebugFormat("Arena_Create %i", size);
    arena->data = Memory_Alloc(size);
    arena->size = size;
    arena->used = 0;
}

void* Arena_Alloc(unsigned int size)
{
    if (_arena.size == 0)
        Arena_Create(1024 * 1024, &_arena);

    assert((_arena.used + size <= _arena.size) && "Arena is full");

    void* mem = _arena.data + _arena.used;
    _arena.used += size;

    return mem;
}

//Buffer Arena_Alloc(Arena* arena, unsigned int size)
//{
//    assert((arena->used + size <= arena->size) && "Arena is full");
//
//    Buffer buffer;
//    buffer.data = arena->data + arena->used;
//    buffer.size = size;
//    arena->used += size;
//    return buffer;
//}

void Arena_Free(Arena* arena)
{
    Memory_Free(arena->data);
    (*arena) = (Arena) { 0 };
}

void Memory_Free(void* block)
{
    free(block);
}

void Memory_Copy(void* source, void* destination, unsigned int size)
{
    memcpy(destination, source, size);
}


Buffer Buffer_Create(AllocInfo info)
{
    return Buffer_Create2(info.bytes, info.file, info.line);
}

Buffer Buffer_Create2(const unsigned int size, const char* file, const unsigned int line)
{
    Buffer buffer;

    double time = Platform_GetTimeMs();

    if (size < ARENA_SIZE_LIMIT)
    {
        buffer.data = Arena_Alloc(size);
        time = (float)(Platform_GetTimeMs() - time);
        //DebugFormat("Arena alloc %i bytes. (%i) %i / %i %s:%i %f", info.bytes, buffer.data, _arena.used, _arena.size, info.file, info.line, time);
    }
    else
    {
        buffer.data = Memory_Alloc(size);
        time = (float)(Platform_GetTimeMs() - time);

        //DebugFormat("OS Alloc %i bytes. (%i) %s:%i %f", info.bytes, buffer.data, info.file, info.line, time);
    }

    buffer.size = size;

    return buffer;
}

void Buffer_Append(Buffer* buffer, Buffer append)
{
    unsigned int oldSize = buffer->size;
    *buffer = Buffer_Resize(*buffer, GetAllocInfo(buffer->size + append.size));
    Memory_Copy(append.data, buffer->data + oldSize, append.size);
}

void Buffer_AddGrow(Buffer* buffer, unsigned int count, Buffer data)
{
    if (buffer->data == 0)
        *buffer = Buffer_Create(GetAllocInfo(data.size));

    if (count * data.size >= buffer->size)
        *buffer = Buffer_Resize(*buffer, GetAllocInfo(buffer->size * 2));

    Memory_Copy(data.data, buffer->data + count * data.size, data.size);
}

Buffer Buffer_Resize(Buffer buffer, AllocInfo info)
{
    int minSize;
    unsigned int newSize = info.bytes;

    if (newSize < buffer.size)
        minSize = newSize;
    else
        minSize = buffer.size;

    Buffer newBuffer = Buffer_Create(info);

    if (buffer.data != 0)
    {
        Memory_Copy(buffer.data, newBuffer.data, minSize);
        Buffer_Free(&buffer);
    }

    return newBuffer;
}

void Buffer_Free(Buffer* buffer)
{
    ASSERT(buffer->data != 0, "Attempted to free an uninitialized buffer");

    if(buffer->size <= ARENA_SIZE_LIMIT)
    {
        //DebugFormat("Freed %i bytes %i from arena", buffer->size, buffer->data);
    }
    else
    {
        //DebugFormat("Free %i bytes %i", buffer->size, buffer->data);
        Memory_Free(buffer->data);
    }
    (*buffer) = (Buffer) { 0 };
}

Buffer Buffer_Combine(Buffer buffers[], int count)
{
    uint32 totalSize = 0;
    for (int i = 0; i < count; ++i)
        totalSize += buffers[i].size;
    Buffer combined = Buffer_Create(GetAllocInfo(totalSize));
    totalSize = 0;
    for (int i = 0; i < count; ++i)
    {
        Memory_Copy(buffers[i].data, combined.data + totalSize, buffers[i].size);
        totalSize += buffers[i].size;
    }
    return combined;
}

Stream Stream_Create(AllocInfo info)
{
    return (Stream) { .buffer = Buffer_Create(info), .pos = 0 };
}

void Stream_Write(Stream* stream, Buffer buffer)
{
    uint32 prevpos = stream->pos;
    stream->pos += buffer.size;
    ASSERT(stream->pos <= stream->buffer.size, "Buffer overflow");

    Memory_Copy(buffer.data, stream->buffer.data + prevpos, buffer.size);
}