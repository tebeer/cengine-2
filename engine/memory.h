#ifndef _MEMORY_H_
#define _MEMORY_H_

#include "util.h"

#define STACK_ALLOC_LIMIT 1024
#define STACK_ALLOC(size) _alloca(ASSERT(((size) <= STACK_ALLOC_LIMIT) ? (size) : 0, "STACK_ALLOC (%i) size over STACK_ALLOC_LIMIT (%i)", (size), STACK_ALLOC_LIMIT))
#define STACK_ARRAY(type, count) STACK_ALLOC(sizeof(type) * count)

#define GetAllocInfo(size) (AllocInfo) { size, __FILE__, __LINE__ } 

#ifdef __cplusplus
#define ToBuffer(t) { (unsigned char*)&t, sizeof(t) } 
#else
#define ToBuffer(t) (Buffer) { (unsigned char*)&t, sizeof(t) } 
#endif

#define BUFFER_CREATE(size) Buffer_Create2(size, __FILE__, __LINE__);
#define BUFFER_COMBINE(...) Buffer_Combine((Buffer[]){__VA_ARGS__}, sizeof((Buffer[]){__VA_ARGS__}) / sizeof(Buffer));
#define STACK_BUFFER(bufsize) (Buffer) { .data = STACK_ALLOC(bufsize) { 0 }, .size = bufsize};

typedef struct AllocInfo_t
{
    unsigned int bytes;
    const char* file;
    unsigned int line;
} AllocInfo;

typedef struct Buffer_t
{
    unsigned char* data;
    unsigned int size;
} Buffer;

typedef struct Arena_t
{
    unsigned char* data;
    unsigned int size;
    unsigned int used;
} Arena;

typedef struct Stream_t
{
    Buffer buffer;
    unsigned int pos;
} Stream;

#ifdef __cplusplus
extern "C" {
#endif

    //void Arena_Create(unsigned int size);
    void* Arena_Alloc(unsigned int size);
    //void Arena_Free(Arena* arena);

    void* Memory_Alloc(unsigned int size);
    void* Memory_Realloc(void* block, unsigned int size);
    void Memory_Free(void* block);
    void Memory_Copy(void* source, void* destination, unsigned int size);

    Buffer Buffer_Create(AllocInfo info);
    Buffer Buffer_Create2(const unsigned int size, const char* file, const unsigned int line);
    void Buffer_Append(Buffer* buffer, Buffer append);
    void Buffer_AddGrow(Buffer* buffer, unsigned int count, Buffer data);
    Buffer Buffer_Resize(Buffer buffer, AllocInfo info);
    void Buffer_Free(Buffer* buffer);
    Buffer Buffer_Combine(Buffer buffers[], int count);

    Stream Stream_Create(AllocInfo info);
    void Stream_Write(Stream* stream, Buffer buffer);

#ifdef __cplusplus
}
#endif
    
#endif