#include "mesh.h"
#include "util.h"
#include "print.h"

void Mesh_Destroy(Mesh* mesh)
{
    Buffer_Free(&mesh->indexBuffer);
    Buffer_Free(&mesh->vertexBuffer);
    Buffer_Free(&mesh->uv1Buffer);

    *mesh = (Mesh) { 0 };
}

//void Mesh_UploadToGPU(Mesh* mesh)
//{
//    ASSERT(mesh->gpuIndexBuffer == 0, "gpuIndexBuffer not null");
//    ASSERT(mesh->gpuVertexBuffer == 0, "gpuVertexBuffer not null");
//
//    ASSERT(mesh->indexBuffer.data != 0, "IndexBuffer null");
//    ASSERT(mesh->indexBuffer.size != 0, "IndexBuffer size 0");
//    ASSERT(mesh->vertexBuffer.data != 0, "VertexBuffer null");
//    ASSERT(mesh->vertexBuffer.size != 0, "VertexBuffer size 0");
//
//    mesh->gpuIndexBuffer = GFX_CreateBuffer(mesh->indexBuffer.data, mesh->indexBuffer.size, BufferType_Index);
//    mesh->gpuVertexBuffer = GFX_CreateBuffer(mesh->vertexBuffer.data, mesh->vertexBuffer.size, BufferType_Vertex);
//}

//int Mesh_LoadFromFile(String filepath, String filter)
//{
//    // TODO: check if mesh is already loaded
//
//    int asset = Asset_Open(filepath.buffer.data);
//
//    if (asset == -1)
//    {
//        return -1;
//    }
//
//    String tempstr = UString_Fixed(256);
//
//    int meshId = -1;
//    int returnMesh = -1;
//
//    while (1)
//    {
//        Asset_ReadUString(asset, &tempstr);
//
//        if (UString_Equals(tempstr, UString("Mesh")))
//        {
//            if (returnMesh != -1)
//                break;
//
//            meshId = Mesh_Create();
//
//            // Mesh name
//            Asset_ReadUString(asset, &tempstr);
//
//            if (filter.length == 0 || UString_Equals(tempstr, filter))
//                returnMesh = meshId;
//        }
//        else if (UString_Equals(tempstr, UString("Vertices")))
//        {
//            // Vertex count
//            Asset_ReadUString(asset, &tempstr);
//
//            int vertexCount = ParseInt(tempstr.buffer.data);
//            int loc = VertexDataLocation_Position;
//            Mesh_SetDataCount(meshId, loc, (VertexPtrType)vertexCount);
//
//            // Read vertex data
//            Asset_Read(asset, meshes.data[meshId].vertexBuffer[loc].data, Mesh_VertexDataSize[loc], vertexCount);
//        }
//        else if (UString_Equals(tempstr, UString("UVs")))
//        {
//            int vertexCount = meshes.data[meshId].vertexDataCount[VertexDataLocation_Position];
//            int loc = VertexDataLocation_UV;
//
//            Mesh_SetDataCount(meshId, loc, vertexCount);
//            
//            // Read UV data
//            UV* uvArray = (UV*)meshes.data[meshId].vertexBuffer[loc].data;
//            
//            Asset_Read(asset, uvArray, Mesh_VertexDataSize[loc], vertexCount);
//            
//            for (int i = 0; i < vertexCount; ++i)
//            {
//                uvArray[i].y = 1.0f - uvArray[i].y;
//            }
//        }
//        else if (UString_Equals(tempstr, UString("Normals")))
//        {
//            int vertexCount = meshes.data[meshId].vertexDataCount[VertexDataLocation_Position];
//
//            int loc = VertexDataLocation_Normal;
//            Mesh_SetDataCount(meshId, loc, vertexCount);
//            
//            // Read Normal data
//            Asset_Read(asset, meshes.data[meshId].vertexBuffer[loc].data, Mesh_VertexDataSize[loc], vertexCount);
//        }
//        else if (UString_Equals(tempstr, UString("Indices")))
//        {
//            // Index count
//            Asset_ReadUString(asset, &tempstr);
//            
//            int indexCount = ParseInt(tempstr.buffer.data);
//            Mesh_SetIndexCount(meshId, (IndexCountType)indexCount);
//               
//            // Read index data
//            Asset_Read(asset, meshes.data[meshId].indexBuffer.data, sizeof(VertexPtrType), indexCount);
//        }
//        else
//            break;
//    }
//
//    Asset_Close(asset);
//
//    return meshId;
//}

void Mesh_AddIndex(Mesh* mesh, uint16 index)
{
    if (mesh->indexBuffer.data == 0)
        mesh->indexBuffer = Buffer_Create(GetAllocInfo(sizeof(uint16)));

    if (mesh->indexCount * sizeof(uint16) >= mesh->indexBuffer.size)
        mesh->indexBuffer = Buffer_Resize(mesh->indexBuffer, GetAllocInfo(mesh->indexBuffer.size * 2));

    *((uint16*)mesh->indexBuffer.data + mesh->indexCount) = index;
    mesh->indexCount++;
}


void Mesh_Trim(Mesh* mesh)
{
    uint32 indicesSize = mesh->indexCount * sizeof(uint16);
    if (mesh->indexBuffer.size > indicesSize)
        mesh->indexBuffer = Buffer_Resize(mesh->indexBuffer, GetAllocInfo(indicesSize));

    uint32 verticesSize = mesh->vertexCount * sizeof(Vector3);
    if (mesh->vertexBuffer.size > verticesSize)
        mesh->vertexBuffer = Buffer_Resize(mesh->vertexBuffer, GetAllocInfo(verticesSize));

    uint32 uv1Size = mesh->vertexCount * sizeof(Vector2);
    if (mesh->uv1Buffer.size > uv1Size)
        mesh->uv1Buffer = Buffer_Resize(mesh->uv1Buffer, GetAllocInfo(uv1Size));

    uint32 normalsSize = mesh->vertexCount * sizeof(Vector3);
    if (mesh->normalBuffer.size > normalsSize)
        mesh->normalBuffer = Buffer_Resize(mesh->normalBuffer, GetAllocInfo(normalsSize));
}