#ifndef _MESH_H_
#define _MESH_H_

#include "math2.h"
#include "util.h"

typedef enum MeshChannel_t
{
    MeshChannel_Position = 0x01,
    MeshChannel_UV1 = 0x02,
    MeshChannel_Normal = 0x04,
} MeshChannel;

typedef struct Mesh_t
{
    union vertices_u
    {
        Buffer vertexBuffer;
        Vector3* vertices; // Never assign to this, as it might overwrite buffer size as well.
    };
    union uv1_u
    {
        Buffer uv1Buffer;
        Vector2* uv1;
    };
    union normals_u
    {
        Buffer normalBuffer;
        Vector3* normals;
    };
    uint16 vertexCount;
    uint8 channels;
    union indices_u
    {
        Buffer indexBuffer;
        uint16* indices;
    };
    uint32 indexCount;
} Mesh;

CAPI_START

void Mesh_Destroy(Mesh* mesh);
void Mesh_UploadToGPU(Mesh* mesh);

void Mesh_AddIndex(Mesh* mesh, uint16 index);
void Mesh_AddVertex(Mesh* mesh, Vector3 vertex, Vector2 uv1);
void Mesh_Trim(Mesh* mesh);

CAPI_END

#endif
