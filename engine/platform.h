#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include "util.h"

void Platform_GetResolution(int* width, int* height);
void Platform_GetWindowSize(int* width, int* height);
double Platform_GetTimeMs();
uint64 Platform_GetCPUCycles();

#endif