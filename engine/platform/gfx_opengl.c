#include "gfx.h"
#include "mesh.h"
#include "print.h"
#include "math2.h"
#include "platform.h"
#include "ustring.h"

// http://galogen.gpfault.net/galogen-web.html
#include "gl.h"
#include <math.h>

#include <stdio.h>
#include <assert.h>

#include "../stb_easy_font.h"

typedef struct GLShader_t
{
    GLuint program;
    GLuint vertexShader;
    GLuint fragmentShader;
    uint8 constantLocationInBuffer[16];
    uint8 constantSizeInBuffer[16];
    GLint constantUniformLocation[16];
    uint32 constantCount;
    GLint textureLocation[16];
    GLuint textureCount;
} GLShader;

typedef struct GLVertexLayout_t
{
    GLint attribLocation[16];
    GLenum attribType[16];
    GLuint attribOffset[16];
    GLuint attribDimension[16];
    VertexLayout vertexLayout;
} GLVertexLayout;

typedef struct GLConstantBuffer_t
{
    Buffer buffer;
} GLConstantBuffer;

typedef struct GLTexture_t
{
    GLuint id;
} GLTexture;

#define CACHE_TYPE GLShader
#include "cache.h"

#define CACHE_TYPE GLVertexLayout
#include "cache.h"

#define CACHE_TYPE GLConstantBuffer
#include "cache.h"

#define CACHE_TYPE GLTexture
#include "cache.h"

typedef struct GLResourceCache_t
{
    DataCache_GLShader shaders;
    DataCache_GLVertexLayout vertexLayouts;
    DataCache_GLConstantBuffer constantBuffers;
    DataCache_GLTexture textures;

} GLResourceCache;

static GLResourceCache s_resourceCache = { 0 };

const char* GetErrorString(GLenum const err)
{
    switch (err)
    {
        // opengl 2 errors (8)
    case GL_NO_ERROR:
        return "GL_NO_ERROR";

    case GL_INVALID_ENUM:
        return "GL_INVALID_ENUM";

    case GL_INVALID_VALUE:
        return "GL_INVALID_VALUE";

    case GL_INVALID_OPERATION:
        return "GL_INVALID_OPERATION";

    case GL_STACK_OVERFLOW:
        return "GL_STACK_OVERFLOW";

    case GL_STACK_UNDERFLOW:
        return "GL_STACK_UNDERFLOW";

    case GL_OUT_OF_MEMORY:
        return "GL_OUT_OF_MEMORY";

    //case GL_TABLE_TOO_LARGE:
    //    return "GL_TABLE_TOO_LARGE";
    //
    //    // opengl 3 errors (1)
    //case GL_INVALID_FRAMEBUFFER_OPERATION:
    //    return "GL_INVALID_FRAMEBUFFER_OPERATION";
    //
    //    // gles 2, 3 and gl 4 error are handled by the switch above
    default:
        return 0;
    }
}

void GFX_AssertErrors(const char* info, const char* file, const uint32 line)
{
    GLenum err;
    int hasErrors = 0;
    while ((err = glGetError()) != GL_NO_ERROR)
    {
        hasErrors = 1;
        DebugFormat("GL Error: %s", GetErrorString(err));
    }

    ASSERT(!hasErrors, "GL Errors detected at %s : %i\nInfo: %s", file, line, info);
}

void GFX_PrintErrors()
{
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR)
    {
        DebugFormat("GL Error: %s", GetErrorString(err));
    }
}

String GFX_GetGPUVendorName()
{
    static String vendorName = { 0 };
    if (vendorName.str == 0)
    {
        unsigned char* str = (unsigned char*)glGetString(GL_VENDOR);
        vendorName = UString_Cstr(str);
    }

    return vendorName;
}

String GFX_GetGPUName()
{
    static String gpuName = { 0 };
    if (gpuName.str == 0)
    {
        unsigned char* str = (unsigned char*)glGetString(GL_RENDERER);
        gpuName = UString_Cstr(str);
    }

    return gpuName;
}

void GFX_SetViewport(int width, int height)
{
    if (height == 0)
    {
        height = 1;
    }

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
   //Shader_SetProjectionMatrixPerspective(65.0f, (GLfloat)width / (GLfloat)height, 0.1f, 10000.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

static int _vertexCount = 0;

int GFX_GetVertexCount()
{
    return _vertexCount;
}

void GFX_ResetVertexCount()
{
    _vertexCount = 0;
}

void* GFX_CreateBuffer(void* data, unsigned int size, BufferType type)
{
    ASSERT(size > 0, "GFX_CreateBuffer: Buffer size is 0");

    GLuint buffer;
    glGenBuffers(1, &buffer);

    GLenum target;
    if (type == BufferType_Vertex)
        target = GL_ARRAY_BUFFER;
    else if (type == BufferType_Index)
        target = GL_ELEMENT_ARRAY_BUFFER;
    else
        ASSERT(0, "Invalid buffer type %i", type);

    glBindBuffer(target, buffer);
    glBufferData(target, size, data, GL_STATIC_DRAW);
    glBindBuffer(target, 0);

    Debug_AssertErrors("GFX_CreateBuffer");

    return (void*)(uintptr_t)buffer;
}

void GFX_DeleteBuffer(void* buffer)
{
    glDeleteBuffers(1, (GLuint*)buffer);
    Debug_AssertErrors("GFX_DeleteBuffer");
}

void GFX_SetBufferData(void* buffer, void* data, unsigned int size, BufferType type)
{
    if (type == BufferType_Vertex)
    {
        glBindBuffer(GL_ARRAY_BUFFER, (GLuint)(uintptr_t)buffer);
        glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);
    }
    else if (type == BufferType_Index)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (GLuint)(uintptr_t)buffer);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, size, data);
    }

    Debug_AssertErrors("GFX_SetBufferData");
}

DataHandle GFX_CreateShaderConstantBuffer(uint32 size)
{
    GLConstantBuffer buffer = {0}; 
    buffer.buffer = Buffer_Create(GetAllocInfo(size));

    return DataCache_GLConstantBuffer_Add(&s_resourceCache.constantBuffers, buffer);
}

void GFX_SetShaderConstantBuffer(DataHandle bufferHandle, Buffer data)
{
    GLConstantBuffer* buffer = DataCache_GLConstantBuffer_GetRef(&s_resourceCache.constantBuffers, bufferHandle);

    ASSERT(buffer->buffer.size >= data.size, "Input data size too large %i %i", buffer->buffer.size, data.size);

    Memory_Copy(data.data, buffer->buffer.data, data.size);
}

void GetGLVertexElementType(VertexElementType elementType, GLenum* type, GLuint* dimension, unsigned int* size)
{
    switch (elementType)
    {
    case VertexElementType_Float2:
        *type = GL_FLOAT;
        *size = 4;
        *dimension = 2;
        break;
    case VertexElementType_Float3:
        *type = GL_FLOAT;
        *size = 4;
        *dimension = 3;
        break;
    case VertexElementType_Float4:
        *type = GL_FLOAT;
        *size = 4;
        *dimension = 4;
        break;
    default:
        assert(0);
        break;
    }
}

DataHandle GFX_CreateVertexShaderInputLayout(DataHandle shaderHandle, const VertexLayout* layout)
{
    GLVertexLayout vertexLayout = { 0 };

    GLShader* shader = DataCache_GLShader_GetRef(&s_resourceCache.shaders, shaderHandle);
    GLuint shaderProgram = shader->program;

    GLuint offsets[4] = { 0 };

    String elementName = STRING_STACK(256);

    for (unsigned int i = 0; i < layout->elementCount; ++i)
    {
        unsigned int size;
        GetGLVertexElementType(layout->elements[i].type, &vertexLayout.attribType[i], &vertexLayout.attribDimension[i], &size);

        UString_Format(&elementName, "in_%s0", layout->elements[i].name.str);

        vertexLayout.attribLocation[i] = glGetAttribLocation(shaderProgram, elementName.str);
        ASSERT(vertexLayout.attribLocation[i] >= 0, "Failed to get attrib location for %s", elementName);

        vertexLayout.attribOffset[i] = offsets[layout->elements[i].bufferIndex];

        offsets[layout->elements[i].bufferIndex] += vertexLayout.attribDimension[i] * size;
    }
    vertexLayout.vertexLayout = *layout;

    return DataCache_GLVertexLayout_Add(&s_resourceCache.vertexLayouts, vertexLayout);
}

static uint32 enabledVertexAttribArrayCount = {0};
static GLuint enabledVertexAttribArray[16] = {0};
void GFX_SetRenderState(const RenderState* rs)
{
    GLVertexLayout* vertexLayout = DataCache_GLVertexLayout_GetRef(&s_resourceCache.vertexLayouts, rs->vertexLayout);
    
    for (uint32 i = 0; i < enabledVertexAttribArrayCount; ++i)
    {
        glDisableVertexAttribArray(enabledVertexAttribArray[i]);
    }
    enabledVertexAttribArrayCount = 0;

    for (uint32 i = 0; i < vertexLayout->vertexLayout.elementCount; ++i)
    {
        uint32 bufferIndex = vertexLayout->vertexLayout.elements[i].bufferIndex;

        if(bufferIndex >= rs->vertexBufferCount) // This is unsafe
            continue;

        ASSERT(bufferIndex < rs->vertexBufferCount, "Buffer index out of bounds: index %i, count %i", i, rs->vertexBufferCount);

        const VertexBuffer* vertexBuffer = &rs->vertexBuffers[bufferIndex];
        
        ASSERT(vertexBuffer->buffer != 0, "VertexBuffer is null %i", i);

        glBindBuffer(GL_ARRAY_BUFFER, (GLuint)(uintptr_t)vertexBuffer->buffer);
        glEnableVertexAttribArray(vertexLayout->attribLocation[i]);
        glVertexAttribPointer(
            vertexLayout->attribLocation[i],
            vertexLayout->attribDimension[i],
            vertexLayout->attribType[i],
            GL_FALSE,
            vertexBuffer->stride,
            (void*)(uintptr_t)vertexLayout->attribOffset[i]);

        enabledVertexAttribArray[enabledVertexAttribArrayCount++] = vertexLayout->attribLocation[i];
    }
    Debug_AssertErrors("glEnableVertexAttribArray");

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (GLuint)(uintptr_t)rs->indexBuffer);
    Debug_AssertErrors("glBindBuffer");

    GLShader* shader = DataCache_GLShader_GetRef(&s_resourceCache.shaders, rs->shader);
    glUseProgram(shader->program);
    Debug_AssertErrors("glUseProgram");

    if (rs->constantBuffer.version != 0)
    {
        GLConstantBuffer* constantBuffer = DataCache_GLConstantBuffer_GetRef(&s_resourceCache.constantBuffers, rs->constantBuffer);

        for (uint32 i = 0; i < shader->constantCount; ++i)
        {
            if (shader->constantSizeInBuffer[i] == 64)
            {
                glUniformMatrix4fv(shader->constantUniformLocation[i], 1, GL_FALSE, (GLfloat*)(constantBuffer->buffer.data + shader->constantLocationInBuffer[i]));
                Debug_AssertErrors("glUniformMatrix4fv");
            }
            else
                ASSERT(0, "Unknown var size %i", 123);
        }
    }

    for (uint32 i = 0; i < shader->textureCount; ++i)
    {
        switch (i)
        {
        case 0:
            glActiveTexture(GL_TEXTURE0);
            break;
        default:
            ASSERT(0, "Supports only 1 texture");
        }

        if(rs->texture.version !=0)
        {
            GLTexture* tex = DataCache_GLTexture_GetRef(&s_resourceCache.textures, rs->texture);
            glBindTexture(GL_TEXTURE_2D, tex->id);
            Debug_AssertErrors("glBindTexture");
            glUniform1i(shader->textureLocation[i], i);
            Debug_AssertErrors("glUniform1i");
        }
        else
        {
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }

    Debug_AssertErrors("GFX_SetRenderState end");
}

void GFX_Clear(const ClearFlags clearFlags, const Color color)
{
    GLbitfield mask = 0;
    if ((clearFlags & ClearFlags_Color) != 0)
        mask |= GL_COLOR_BUFFER_BIT;
    if ((clearFlags & ClearFlags_Depth) != 0)
        mask |= GL_DEPTH_BUFFER_BIT;

    glClearColor(color.r, color.g, color.b, color.a);
    glClear(mask);
}

static GLenum GetGLBlendFunc(BlendFunc blendFunc)
{
    switch (blendFunc)
    {
        case BlendFunc_Zero: return GL_ZERO;
        case BlendFunc_One: return GL_ONE;
        case BlendFunc_SrcColor: return GL_SRC_COLOR;
        case BlendFunc_OneMinusSrcColor: return GL_ONE_MINUS_SRC_COLOR;
        case BlendFunc_DstColor: return GL_DST_COLOR;
        case BlendFunc_OneMinusDstColor: return GL_ONE_MINUS_DST_COLOR;
        case BlendFunc_SrcAlpha: return GL_SRC_ALPHA;
        case BlendFunc_OneMinusSrcAlpha: return GL_ONE_MINUS_SRC_ALPHA;
        case BlendFunc_DstAlpha: return GL_DST_ALPHA;
        case BlendFunc_OneMinusDstAlpha: return GL_ONE_MINUS_DST_ALPHA;
        case BlendFunc_ConstantColor: return GL_CONSTANT_COLOR;
        case BlendFunc_OneMinusConstantColor: return GL_ONE_MINUS_CONSTANT_COLOR;
        case BlendFunc_ConstantAlpha: return GL_CONSTANT_ALPHA;
        case BlendFunc_OneMinusConstantAlpha: return GL_ONE_MINUS_CONSTANT_ALPHA;
    }
    assert(0 && "Invalid blend func");
}

void GFX_SetBlendFunc(BlendFunc srcBlend, BlendFunc dstBlend)
{
    glEnable(GL_BLEND);
    glBlendFunc(GetGLBlendFunc(srcBlend), GetGLBlendFunc(dstBlend));
}

void GFX_DrawIndexed(unsigned int indexCount, unsigned int startIndex)
{
    Debug_AssertErrors("Before GFX_DrawIndexed");
    glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_SHORT, 0);
    Debug_AssertErrors("GFX_DrawIndexed");
}

void Draw_Text(float x, float y, char* text)
{
    float buf[4*256];

    int quadcount = stb_easy_font_print(x, y, text, 0, buf, 4*256*sizeof(float));

//    Shader_Use(-1);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    int width;
    int height;
    Platform_GetWindowSize(&width, &height);

    glOrtho(0, width, height, 0, -10, 100);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 16, buf);
    glDrawArrays(GL_QUADS, 0, quadcount * 4);
    glDisableClientState(GL_VERTEX_ARRAY);

    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}

// ------------------------------------------------------------------------------------------
// ----------------------------------------  SHADER  ----------------------------------------
// ------------------------------------------------------------------------------------------

int CreateAndCompileShader(GLenum type, GLuint* shaderId, Buffer source)
{
    Debug_AssertErrors("CreateAndCompileShader");

    GLuint shader = glCreateShader(type);
    Debug_AssertErrors("glCreateShader");

    glShaderSource(shader, 1, &source.data, NULL);
    Debug_AssertErrors("glShaderSource");

    glCompileShader(shader);
    Debug_AssertErrors("glCompileShader");

    GLint isCompiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
    Debug_AssertErrors("glGetShaderiv");
    if (isCompiled == GL_FALSE)
    {
        char info[1024];
        GLsizei len;

        glGetShaderInfoLog(shader, 1024, &len, info);
        Debug_AssertErrors("glGetShaderInfoLog");

        DebugLine(source.data);
        DebugLine(info);

        ASSERT(0, "Failed to compile shader"); 
    }
    Debug_AssertErrors("CreateAndCompileShader");

    *shaderId = shader;

    return 1;
}

DataHandle GFX_CreateShader(Shader shaderData)
{
    Debug_AssertErrors("GFX_CreateShader");

    GLShader shader;
    int success;

    success = CreateAndCompileShader(GL_VERTEX_SHADER, &shader.vertexShader, shaderData.vertexShader);
    ASSERT(success, "Vertex Shader compilation failed");

    success = CreateAndCompileShader(GL_FRAGMENT_SHADER, &shader.fragmentShader, shaderData.fragmentShader);
    ASSERT(success, "Fragment Shader compilation failed");

    shader.program = glCreateProgram();

    glAttachShader(shader.program, shader.vertexShader);
    glAttachShader(shader.program, shader.fragmentShader);
    Debug_AssertErrors("glAttachShader");

    glLinkProgram(shader.program);
    Debug_AssertErrors("glLinkProgram");

    glGetProgramiv(shader.program, GL_LINK_STATUS, &success);
    if (!success)
    {
        char info[1024];
        GLsizei len;
        glGetProgramInfoLog(shader.program, 1024, &len, info);
        DebugLine(info);
        ASSERT(0, "Link program failed");
    }

    Debug_AssertErrors("GFX_CreateShader");

    shader.constantCount = shaderData.vertexConstantLayout.varCount;
    for (uint32 i = 0; i < shader.constantCount; ++i)
    {
        shader.constantUniformLocation[i] = glGetUniformLocation(shader.program, shaderData.vertexConstantLayout.vars[i].name);
        shader.constantLocationInBuffer[i] = shaderData.vertexConstantLayout.vars[i].location;
        shader.constantSizeInBuffer[i] = shaderData.vertexConstantLayout.vars[i].size;

        ASSERT(shader.constantUniformLocation[i] >= 0, "Failed to get uniform location for constant %s", shaderData.vertexConstantLayout.vars[i].name);
        //DebugFormat("CONSTANT %i %i %i", shader.constantUniformLocation[i], shaderData.vertexConstantLayout.vars[i].location);
    }

    shader.textureCount = shaderData.fragmentConstantLayout.resourceCount;
    for (uint32 i = 0; i < shaderData.fragmentConstantLayout.resourceCount; ++i)
    {
        shader.textureLocation[i] = glGetUniformLocation(shader.program, shaderData.fragmentConstantLayout.resources[i].name);
        ASSERT(shader.textureLocation[i] >= 0, "Failed to get uniform location for texture %s", shaderData.fragmentConstantLayout.resources[i].name);
    }

    return DataCache_GLShader_Add(&s_resourceCache.shaders, shader);
}

void GFX_DestroyShader(DataHandle shaderHandle)
{
    ASSERT(DataCache_GLShader_Exists(&s_resourceCache.shaders, shaderHandle), "Shader doesn't exist %i", shaderHandle);

    GLShader* shader = DataCache_GLShader_GetRef(&s_resourceCache.shaders, shaderHandle);

    if (shader->vertexShader != 0)
        glDeleteShader(shader->vertexShader);
    if (shader->fragmentShader != 0)
        glDeleteShader(shader->fragmentShader);
    if (shader->program != 0)
        glDeleteProgram(shader->program);

    Debug_AssertErrors("GFX_DestroyShader");

    *shader = (GLShader) { 0 };

    DataCache_GLShader_Remove(&s_resourceCache.shaders, shaderHandle);
}

int GetGLTextureFilter(TextureFilterMode filterMode, int mipmap)
{
    int filter;
    if (mipmap)
    {
        switch (filterMode)
        {
        case TextureFilter_Point:
            filter = GL_NEAREST_MIPMAP_NEAREST;
            break;
        case TextureFilter_Bilinear:
            filter = GL_LINEAR_MIPMAP_NEAREST;
            break;
        case TextureFilter_Trilinear:
            filter = GL_LINEAR_MIPMAP_LINEAR;
            break;
        }
    }
    else
    {
        switch (filterMode)
        {
        case TextureFilter_Point:
            filter = GL_NEAREST;
            break;
        case TextureFilter_Bilinear:
        case TextureFilter_Trilinear:
            filter = GL_LINEAR;
            break;
        }
    }
    return filter;
}

static GLint GetGLTextureFormat(TextureFormat format)
{
    GLint glFormat;
    if (format == TextureFormat_RGB)
        glFormat = GL_RGB;
    else if (format == TextureFormat_RGBA)
        glFormat = GL_RGBA;
    else
        assert(0 && "Invalid format");
    return glFormat;
}

DataHandle GFX_CreateTexture(Texture texture)
{
    int textureId = -1;

    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    int mipmap = 1;
    int glFilter = GetGLTextureFilter(texture.filterMode, mipmap);

    if (mipmap)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
        glTexEnvf(GL_TEXTURE_FILTER_CONTROL, GL_TEXTURE_LOD_BIAS, -1);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
    }
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, texture.filterMode == TextureFilter_Point ? GL_NEAREST : GL_LINEAR);

    GLint glFormat = GetGLTextureFormat(texture.format);

    glTexImage2D(GL_TEXTURE_2D, 0, glFormat, texture.width, texture.height, 0, glFormat, GL_UNSIGNED_BYTE, texture.buffer.data);

    Debug_AssertErrors("Texture_Load");

    DebugFormat("Created texture %i, width: %i, height: %i, format %i, time: %f", textureId, texture.width, texture.height, texture.format);

    GLTexture gltexture = {0};
    gltexture.id = textureId;

    return DataCache_GLTexture_Add(&s_resourceCache.textures, gltexture);
}
