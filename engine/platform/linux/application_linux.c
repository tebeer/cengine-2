#ifdef linux

#include <GLFW/glfw3.h>

#include "application.h"
#include "asset.h"
#include "gfx.h"
#include "renderer_linux.h"

GLFWwindow *window;

void Input_GetMousePosition(float* x, float* y)
{
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    *x = (float)xpos;
    *y = (float)ypos;
}

char mouseButtons[8] = {0, 0, 0, 0, 0, 0, 0, 0};
int Input_GetButtonState(int button)
{
    int state = 0;
    if (button > 0 && button <= 8) {
        return mouseButtons[button-1];
    } else {
        state = glfwGetKey(window, button);
    }
    return (state == GLFW_PRESS)?1:0;
}

void GetScreenSize(int *width, int* height)
{
    glfwGetFramebufferSize(window, width, height);
}

void FrameBufferResized(GLFWwindow *window, int width, int height)
{
    ResizeViewport(width, height);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    mouseButtons[button] = (action == GLFW_PRESS);
}

int main(int argc, char* argv[])
{
    if (!glfwInit())
        return -1;

    window = glfwCreateWindow(640, 480, "C Engine", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    glfwSetFramebufferSizeCallback(window, FrameBufferResized);
    glfwSetMouseButtonCallback(window, mouse_button_callback);

    InitRenderer(window);

    Asset_Register_Default_Path();
    Init();

    int run = 1;
    float deltaTime = 0;
    int targetFPS = 60;
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        ClearScreen();
        Update(deltaTime);
        Draw();
        glfwSwapBuffers(window);

        static double time0 = 0.0;
        double time1 = glfwGetTime();
        deltaTime = time1-time0;

        time0 = glfwGetTime();
    }

    glfwTerminate();

    return 0;
}

#endif
