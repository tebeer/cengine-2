#ifdef linux

#include "platform/gl.h"
#include <GLFW/glfw3.h>
#include "gfx.h"

#include "print.h"

int InitGL()
{
    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    return 1;
}

int InitRenderer(GLFWwindow* window)
{
    glfwMakeContextCurrent(window);

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    ResizeViewport(width, height);

    InitGL();

    GLenum error = glGetError();
    DebugFormat("error: %i\n", error);
}

#endif
