#ifndef _GFX_LINUX_H_
#define _GFX_LINUX_H_

#include <GLFW/glfw3.h>

int InitRenderer(GLFWwindow* window);

#endif
