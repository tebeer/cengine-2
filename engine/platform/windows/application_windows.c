#ifdef _WIN32

//#ifndef UNICODE
//#define UNICODE
//#endif 

#include <windows.h>
#include "gfx.h"
#include "platform.h"
#include "ustring.h"
#include "input.h"
#include "user.h"
#include "print.h"
#include <stdio.h> // freopen_s

#include <intrin.h> // __rdtsc

#pragma comment(lib, "winmm.lib")

static HWND _hwnd;

AppContext _context = { 0 };

void Platform_GetWindowSize(int *width, int* height)
{
    RECT windowRect;

    GetClientRect(_hwnd, &windowRect);

    *width = windowRect.right - windowRect.left;
    *height = windowRect.bottom - windowRect.top;
}

static void _GetMousePosition(float* x, float* y)
{
    POINT cursorPos;
    GetCursorPos(&cursorPos);
    ScreenToClient(_hwnd, &cursorPos);
    *x = (float)cursorPos.x;
    *y = (float)cursorPos.y;
}

//static int _GetButtonState(int button)
//{
//    return ((GetKeyState(button) & 0x8000) != 0) ? 1 : 0;
//}


static void _ReadInput(InputState* input)
{
    _GetMousePosition(&input->mouse_x, &input->mouse_y);
}

static unsigned int GetDisplayHz()
{
    DEVMODE lpDevMode;
    memset(&lpDevMode, 0, sizeof(DEVMODE));
    lpDevMode.dmSize = sizeof(DEVMODE);
    lpDevMode.dmDriverExtra = 0;

    if (EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &lpDevMode) == 0)
    {
        DebugFormat("Failed to get display frequency");
        return 0;
    }

    return lpDevMode.dmDisplayFrequency;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_DESTROY:
        {
            PostQuitMessage(0);
            return 0;
        }
        case WM_SIZE:
        {
            GFX_SetViewport(LOWORD(lParam), HIWORD(lParam));
            return 0;
        }

        case WM_KEYDOWN:
        {
            unsigned char button = (unsigned char)wParam;
            _context.input.buttons[button] = 1;
            return 0;
        }

        case WM_KEYUP:
        {
            unsigned char button = (unsigned char)wParam;
            _context.input.buttons[button] = 0;
            return 0;
        }

        case WM_LBUTTONDOWN:
        case WM_RBUTTONDOWN:
        case WM_LBUTTONUP:
        case WM_RBUTTONUP:
        {
            unsigned char button = (unsigned char)wParam;
            _context.input.buttons[KeyCode_MouseLeft] = (button & MK_LBUTTON) != 0;
            _context.input.buttons[KeyCode_MouseRight] = (button & MK_RBUTTON) != 0;

            // Enable receiving mouse events when cursor is outside of our window
            if (button == 0)
                ReleaseCapture();
            else
                SetCapture(hwnd);
            return 0;
        }

        case WM_KILLFOCUS:
        {
            _context.input = (InputState) { 0 };
            return 0;
        }

        case WM_SETFOCUS:
        {
            return 0;
        }
        default:
            break;
    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE h, PWSTR pCmdLine, int nCmdShow)
{
    // Register the window class.
    UNREFERENCED_PARAMETER(h);
    UNREFERENCED_PARAMETER(pCmdLine);

    WNDCLASS wc = { 0 };

    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = "C Engine";

    RegisterClass(&wc);

    //int screenWidth, screenHeight;
    //GetDesktopResolution(&screenWidth, &screenHeight);

    // Create the window.
    HWND hwnd = CreateWindowEx(
        0,                              // Optional window styles.
        wc.lpszClassName,                     // Window class
        "C Engine",    // Window text
        WS_OVERLAPPED,//WS_POPUP,            // Window style

                                        // Size and position
        200, 200, 960, 540,

        NULL,       // Parent window    
        NULL,       // Menu
        hInstance,  // Instance handle
        NULL        // Additional application data
        );

    if (hwnd == NULL)
    {
        return 1;
    }

    _hwnd = hwnd;

    AllocConsole();
    freopen_s((FILE**)stdout, "CONOUT$", "w", stdout);

    ShowWindow(hwnd, nCmdShow);
    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    HGLRC hglrc;
    if (InitRenderer(hwnd, &hglrc) != 0)
        DebugFormat("Failed to init renderer");

    DebugFormat("GPU Vendor: %s", GFX_GetGPUVendorName().str);
    DebugFormat("GPU Name: %s", GFX_GetGPUName().str);

    int width;
    int height;
    Platform_GetWindowSize(&width, &height);
    GFX_SetViewport(width, height);

    _context.fixedMemorySize = 1024 * 1024;
    _context.cacheSize = 64 * 1024 * 1024;
    _context.fixedMemory = (unsigned char*)VirtualAlloc(0, _context.fixedMemorySize + _context.cacheSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
    _context.cacheMemory = _context.fixedMemory + _context.fixedMemorySize;

    //Asset_Register_Default_Path();

    // TODO: Get the real executable path
    //Asset_Register_Path("H:\\dev\\dev\\cengine2\\assets");
    User_Init(&_context);

    // Run the message loop.

    timeBeginPeriod(1);

    HDC hdc = GetDC(hwnd);
    MSG msg = { 0 };

    int targetFPS = GetDisplayHz();
    if (targetFPS == 0)
        targetFPS = 60;

    double lastFrameBeginTime = Platform_GetTimeMs();
    double lastFrameEndTime = Platform_GetTimeMs();
    double targetFrameEndTime = Platform_GetTimeMs();
    double actualLastFrameTime = 0;

    int run = 1;
    while (run == 1)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                run = 0;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            double targetMs = 1000.0 / targetFPS;
            double frameBeginTime = Platform_GetTimeMs();
            double timeToNextFrameEnd = targetFrameEndTime - frameBeginTime;

            // If not enough time to keep up frame rate, we must skip a frame
            if (timeToNextFrameEnd < actualLastFrameTime)
            {
                targetFrameEndTime = frameBeginTime + targetMs;
            }

            float deltaTime = (float)(0.001f*(frameBeginTime - lastFrameBeginTime));
            lastFrameBeginTime = frameBeginTime;

            _context.currentTime = frameBeginTime;
            _context.deltaTime = deltaTime;

            GFX_ResetVertexCount();

            _ReadInput(&_context.input);

            User_Update(&_context);

            Present(hdc);

            double frameEndTime = Platform_GetTimeMs();
            double actualFrameEndTime = frameEndTime;

            while (frameEndTime < targetFrameEndTime - 1.0f)
            {
                Sleep(1);
                frameEndTime = Platform_GetTimeMs();
            }
            lastFrameEndTime = frameEndTime;

            targetFrameEndTime += targetMs;

            actualLastFrameTime = actualFrameEndTime - frameBeginTime;
        }
    }

    DeinitRenderer(hwnd, hglrc);

    FreeConsole();

    return 0;
}


#endif
