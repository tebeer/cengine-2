#define COBJMACROS
#include <d3d11.h>
#include <dxgi.h>
#include "gfx.h"

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "d3d11.lib")
//#pragma comment(lib, "windowscodecs.lib")
//#pragma comment(lib, "d3dx11.lib")
//#pragma comment(lib, "d3dx10.lib")

typedef struct DX11Shader_t
{
    ID3D11VertexShader* vertexShader;
    ID3D11PixelShader* fragmentShader;
    Buffer vertexShaderCode;
    Buffer fragmentShaderCode;
} DX11Shader;

typedef struct DX11VertexLayout_t
{
    ID3D11InputLayout* inputLayout;
} DX11VertexLayout;

typedef struct DX11Texture_t
{
    ID3D11SamplerState* samplerState;
    ID3D11Texture2D* texture;
    ID3D11ShaderResourceView* textureView;
} DX11Texture;

typedef struct DX11ConstantBuffer_t
{
    ID3D11Buffer* buffer;
} DX11ConstantBuffer;

#define CACHE_TYPE DX11Shader
#include "cache.h"
#define CACHE_TYPE DX11ConstantBuffer
#include "cache.h"
#define CACHE_TYPE DX11Texture
#include "cache.h"
#define CACHE_TYPE DX11VertexLayout
#include "cache.h"

typedef struct DX11ResourceCache_t
{
    DataCache_DX11Shader shaders;
    DataCache_DX11ConstantBuffer constantBuffers;
    DataCache_DX11Texture textures;
    DataCache_DX11VertexLayout vertexLayouts;

} DX11ResourceCache;

static DX11ResourceCache s_resourceCache = { 0 };

IDXGISwapChain* m_swapChain;
ID3D11Device* m_device;
ID3D11DeviceContext* m_deviceContext;
ID3D11RenderTargetView* m_renderTargetView;
ID3D11Texture2D* m_depthStencilBuffer;
ID3D11DepthStencilState* m_depthStencilState;
ID3D11DepthStencilView* m_depthStencilView;
ID3D11RasterizerState* m_rasterState;

String m_gpuVendorName;
String m_gpuName;

int InitRenderer(HWND hwnd, HGLRC* hglrc)
{
    DebugLine("Init DX11 Renderer");

    HRESULT result;
    IDXGIFactory* factory;
    result = CreateDXGIFactory(&IID_IDXGIFactory, (void**)&factory);

    if (FAILED(result))
        return 1;

    // Use the factory to create an adapter for the primary graphics interface (video card).
    IDXGIAdapter* adapter;
    result = IDXGIFactory_EnumAdapters(factory, 0, &adapter);
    if (FAILED(result))
        return 1;

    // Enumerate the primary adapter output (monitor).
    IDXGIOutput* adapterOutput;
    result = IDXGIAdapter_EnumOutputs(adapter, 0, &adapterOutput);
    if (FAILED(result))
        return 1;

    // Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
    unsigned int numModes;
    result = IDXGIOutput_GetDisplayModeList(adapterOutput, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
    if (FAILED(result))
        return 1;

    // Create a list to hold all the possible display modes for this monitor/video card combination.
    DXGI_MODE_DESC* displayModeList = calloc(numModes, sizeof(DXGI_MODE_DESC));
    if (!displayModeList)
        return 1;

    // Now fill the display mode list structures.
    result = IDXGIOutput_GetDisplayModeList(adapterOutput, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
    if (FAILED(result))
        return 1;

    // Now go through all the display modes and find the one that matches the screen width and height.
    // When a match is found store the numerator and denominator of the refresh rate for that monitor.
    int screenWidth, screenHeight;

    Platform_GetWindowSize(&screenWidth, &screenHeight);

    //UINT numerator, denominator;
    //for (unsigned int i = 0; i < numModes; i++)
    //{
    //    if (displayModeList[i].Width == screenWidth && displayModeList[i].Height == screenHeight)
    //    {
    //        numerator = displayModeList[i].RefreshRate.Numerator;
    //        denominator = displayModeList[i].RefreshRate.Denominator;
    //        DebugFormat("%i %i\n", numerator, denominator);
    //    }
    //}

    // Get the adapter (video card) description.
    DXGI_ADAPTER_DESC adapterDesc;
    result = IDXGIAdapter_GetDesc(adapter, &adapterDesc);
    if (FAILED(result))
        return 1;

    // Store the dedicated video card memory in megabytes.
    int videoCardMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

    // Convert the name of the video card to a character array and store it.
    size_t strLength;
    m_gpuName = UString_Create(256);
    int error = wcstombs_s(&strLength, m_gpuName.str, 128, adapterDesc.Description, 128);
    if (error != 0)
        return 1;

    switch (adapterDesc.VendorId)
    {
    case 0x10DE:
        m_gpuVendorName = UString("NVIDIA");
        break;
    case 0x1002:
    case 0x1022:
        m_gpuVendorName = UString("AMD");
        break;
    case 0x163C:
    case 0x8086:
    case 0x8087:
        m_gpuVendorName = UString("Intel");
        break;
    default:
        m_gpuVendorName = UString("Unknown");
        break;
    }

    free(displayModeList);
    displayModeList = 0;

    IDXGIOutput_Release(adapterOutput);
    adapterOutput = 0;

    IDXGIAdapter_Release(adapter);
    adapter = 0;

    IDXGIFactory_Release(factory);
    factory = 0;

    DXGI_SWAP_CHAIN_DESC swapChainDesc = { 0 };
    swapChainDesc.BufferCount = 1;
    swapChainDesc.BufferDesc.Width = screenWidth;
    swapChainDesc.BufferDesc.Height = screenHeight;
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

    //if (m_vsync_enabled)
    //{
    //    swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
    //    swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
    //}
    //else
    {
        swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
        swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
    }

    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.OutputWindow = hwnd;
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;
    swapChainDesc.Windowed = TRUE;

    swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    swapChainDesc.Flags = 0;

    D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;

    UINT flags = D3D11_CREATE_DEVICE_DEBUG;

    result = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, &featureLevel, 1,
        D3D11_SDK_VERSION, &swapChainDesc, &m_swapChain, &m_device, NULL, &m_deviceContext);
    if (FAILED(result))
        return 1;

    // Get the pointer to the back buffer.
    ID3D11Texture2D* backBufferPtr;
    result = IDXGISwapChain_GetBuffer(m_swapChain, 0, &IID_ID3D11Texture2D, (LPVOID*)&backBufferPtr);
    if (FAILED(result))
        return 1;

    // Create the render target view with the back buffer pointer.
    result = ID3D11Device_CreateRenderTargetView(m_device, (ID3D11Resource*)backBufferPtr, NULL, &m_renderTargetView);
    if (FAILED(result))
        return 1;

    // Release pointer to the back buffer as we no longer need it.
    ID3D11Texture2D_Release(backBufferPtr);
    backBufferPtr = 0;

    D3D11_TEXTURE2D_DESC depthBufferDesc = { 0 };
    depthBufferDesc.Width = screenWidth;
    depthBufferDesc.Height = screenHeight;
    depthBufferDesc.MipLevels = 1;
    depthBufferDesc.ArraySize = 1;
    depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthBufferDesc.SampleDesc.Count = 1;
    depthBufferDesc.SampleDesc.Quality = 0;
    depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthBufferDesc.CPUAccessFlags = 0;
    depthBufferDesc.MiscFlags = 0;

    result = ID3D11Device_CreateTexture2D(m_device, &depthBufferDesc, NULL, &m_depthStencilBuffer);
    if (FAILED(result))
        return 1;

    D3D11_DEPTH_STENCIL_DESC depthStencilDesc = { 0 };

    // Set up the description of the stencil state.
    depthStencilDesc.DepthEnable = TRUE;
    depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

    depthStencilDesc.StencilEnable = TRUE;
    depthStencilDesc.StencilReadMask = 0xFF;
    depthStencilDesc.StencilWriteMask = 0xFF;

    // Stencil operations if pixel is front-facing.
    depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
    depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

    // Stencil operations if pixel is back-facing.
    depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
    depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

    // Create the depth stencil state.
    result = ID3D11Device_CreateDepthStencilState(m_device, &depthStencilDesc, &m_depthStencilState);
    if (FAILED(result))
        return 1;

    ID3D11DeviceContext_OMSetDepthStencilState(m_deviceContext, m_depthStencilState, 1);

    // Initialize the depth stencil view.
    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc = { 0 };
    depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depthStencilViewDesc.Texture2D.MipSlice = 0;

    // Create the depth stencil view.
    result = ID3D11Device_CreateDepthStencilView(m_device, (ID3D11Resource*)m_depthStencilBuffer, &depthStencilViewDesc, &m_depthStencilView);
    if (FAILED(result))
        return 1;

    // Bind the render target view and depth stencil buffer to the output render pipeline.
    ID3D11DeviceContext_OMSetRenderTargets(m_deviceContext, 1, &m_renderTargetView, m_depthStencilView);

    // Setup the raster description which will determine how and what polygons will be drawn.
    D3D11_RASTERIZER_DESC rasterDesc = { 0 };
    rasterDesc.AntialiasedLineEnable = FALSE;
    rasterDesc.CullMode = D3D11_CULL_BACK;
    rasterDesc.DepthBias = 0;
    rasterDesc.DepthBiasClamp = 0.0f;
    rasterDesc.DepthClipEnable = TRUE;
    rasterDesc.FillMode = D3D11_FILL_SOLID;
    rasterDesc.FrontCounterClockwise = FALSE;
    rasterDesc.MultisampleEnable = FALSE;
    rasterDesc.ScissorEnable = FALSE;
    rasterDesc.SlopeScaledDepthBias = 0.0f;

    // Create the rasterizer state from the description we just filled out.
    result = ID3D11Device_CreateRasterizerState(m_device, &rasterDesc, &m_rasterState);
    if (FAILED(result))
        return 1;

    // Now set the rasterizer state.
    ID3D11DeviceContext_RSSetState(m_deviceContext, m_rasterState);

    // Setup the viewport for rendering.
    D3D11_VIEWPORT viewport = { 0 };
    viewport.Width = (float)screenWidth;
    viewport.Height = (float)screenHeight;
    viewport.MinDepth = 0.0f;
    viewport.MaxDepth = 1.0f;
    viewport.TopLeftX = 0.0f;
    viewport.TopLeftY = 0.0f;

    // Create the viewport.
    ID3D11DeviceContext_RSSetViewports(m_deviceContext, 1, &viewport);

    //float fieldOfView, screenAspect;
    //// Setup the projection matrix.
    //fieldOfView = (float)D3DX_PI / 4.0f;
    //screenAspect = (float)screenWidth / (float)screenHeight;
    //
    //// Create the projection matrix for 3D rendering.
    //D3DXMatrixPerspectiveFovLH(&m_projectionMatrix, fieldOfView, screenAspect, screenNear, screenDepth);
    //D3DXMatrixIdentity(&m_worldMatrix);


    return 0;
}


void DeinitRenderer(HWND hwnd, HGLRC hglrc)
{

}

void Present(HDC hdc)
{
    //if (m_vsync_enabled)
    //{
    //    // Lock to screen refresh rate.
    //    m_swapChain->Present(1, 0);
    //}
    //else
    //{
        // Present as fast as possible.
        IDXGISwapChain_Present(m_swapChain, 0, 0);
    //}
}

String GFX_GetGPUVendorName()
{
    return m_gpuVendorName;
}

String GFX_GetGPUName()
{
    return m_gpuName;
}

void GFX_AssertErrors(const char* info, const char* file, const uint32 line)
{
}

void GFX_PrintErrors()
{
}

void GFX_SetViewport(int width, int height)
{
}

void GFX_SetVSync(int on)
{
}

void* GFX_CreateBuffer(void* data, unsigned int size, BufferType type)
{
    D3D11_BUFFER_DESC bufferDesc;
    bufferDesc.ByteWidth = size;
    if (type == BufferType_Vertex)
    {
        bufferDesc.Usage = D3D11_USAGE_DEFAULT;
        bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        bufferDesc.CPUAccessFlags = 0;
    }
    else if (type == BufferType_Index)
    {
        bufferDesc.Usage = D3D11_USAGE_DEFAULT;
        bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
        bufferDesc.CPUAccessFlags = 0;
    }

    bufferDesc.MiscFlags = 0;
    bufferDesc.StructureByteStride = 0;

    D3D11_SUBRESOURCE_DATA bufferData;
    bufferData.pSysMem = data;
    bufferData.SysMemPitch = 0;
    bufferData.SysMemSlicePitch = 0;

    ID3D11Buffer* buffer;
    HRESULT result = ID3D11Device_CreateBuffer(m_device, &bufferDesc, &bufferData, &buffer);
    if (FAILED(result))
    {
        DebugFormat("Failed to create GPUBuffer");
        return 0;
    }

    return buffer;
}

void GFX_DeleteBuffer(void* buffer)
{
    ID3D11Buffer* buf = (ID3D11Buffer*)buffer;
    ASSERT(buf != NULL, "GFX_DeleteBuffer Invalid buffer");
    ID3D11Buffer_Release(buf);
}

void GFX_SetBufferData(void* buffer, void* data, unsigned int size, BufferType type)
{
    ID3D11Buffer* buf = (ID3D11Buffer*)buffer;
    ASSERT(buf != NULL, "GFX_SetBufferData Invalid buffer");

    ID3D11DeviceContext_UpdateSubresource(m_deviceContext, (ID3D11Resource*)buf, 0, NULL, data, size, 0);
}

DataHandle GFX_CreateShaderConstantBuffer(uint32 size)
{
    D3D11_BUFFER_DESC bufferDesc;
    bufferDesc.ByteWidth = size;
    bufferDesc.Usage = D3D11_USAGE_DEFAULT;
    bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    bufferDesc.CPUAccessFlags = 0;
    bufferDesc.MiscFlags = 0;
    bufferDesc.StructureByteStride = 0;

    ID3D11Buffer* d3dbuffer;
    HRESULT result = ID3D11Device_CreateBuffer(m_device, &bufferDesc, NULL, &d3dbuffer);
    ASSERT(!FAILED(result), "ID3D11Device_CreateBuffer failed");

    DX11ConstantBuffer buffer = {
        .buffer = d3dbuffer,
    };

    return DataCache_DX11ConstantBuffer_Add(&s_resourceCache.constantBuffers, buffer);
}

void GFX_SetShaderConstantBuffer(DataHandle constantBuffer, Buffer data)
{
    DX11ConstantBuffer* buffer = DataCache_DX11ConstantBuffer_GetRef(&s_resourceCache.constantBuffers, constantBuffer);

    ID3D11DeviceContext_UpdateSubresource(m_deviceContext, (ID3D11Resource*)buffer->buffer, 0, NULL, data.data, data.size, 0);
}

void GetDXGIFormat(const VertexElement* element, DXGI_FORMAT* outFormat, unsigned int* outSize)
{
    if (element->type == VertexElementType_Float2)
    {
        *outFormat = DXGI_FORMAT_R32G32_FLOAT;
        *outSize = 2 * 4;
        return;
    }
    if (element->type == VertexElementType_Float3)
    {
        *outFormat = DXGI_FORMAT_R32G32B32_FLOAT;
        *outSize = 3 * 4;
        return;
    }

    if (element->type == VertexElementType_Float4)
    {
        *outFormat = DXGI_FORMAT_R32G32B32A32_FLOAT;
        *outSize = 4 * 4;
        return;
    }

    ASSERT(0, "Unknown format");
}

DataHandle GFX_CreateVertexShaderInputLayout(DataHandle shaderHandle, const VertexLayout* layout)
{
    ASSERT(DataCache_DX11Shader_Exists(&s_resourceCache.shaders, shaderHandle), "Invalid shaderHandle %i", shaderHandle);

    D3D11_INPUT_ELEMENT_DESC* polygonLayout = _alloca(layout->elementCount * sizeof(D3D11_INPUT_ELEMENT_DESC));

    for (unsigned int i = 0; i < layout->elementCount; ++i)
    {
        polygonLayout[i].SemanticName = layout->elements[i].name.str;
        polygonLayout[i].SemanticIndex = 0;
        polygonLayout[i].InputSlot = layout->elements[i].bufferIndex;
        polygonLayout[i].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        polygonLayout[i].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
        polygonLayout[i].InstanceDataStepRate = 0;

        unsigned int size;
        GetDXGIFormat(&layout->elements[i], &polygonLayout[i].Format, &size);
    }

    DX11Shader* shader = DataCache_DX11Shader_GetRef(&s_resourceCache.shaders, shaderHandle);
    Buffer* vert = &shader->vertexShaderCode;

    DX11VertexLayout dx11VertexLayout = { 0 };

    HRESULT result = ID3D11Device_CreateInputLayout(m_device, polygonLayout, layout->elementCount, (void*)vert->data, (SIZE_T)vert->size, &dx11VertexLayout.inputLayout);

    ASSERT(!FAILED(result), "Failed to CreateInputLayout");

    return DataCache_DX11VertexLayout_Add(&s_resourceCache.vertexLayouts, dx11VertexLayout);
}

void GFX_SetRenderState(const RenderState* rs)
{
    ID3D11Buffer* ibuf = (ID3D11Buffer*)rs->indexBuffer;
    ASSERT(ibuf != 0, "Invalid Index Buffer");

    DX11VertexLayout* vertexLayout = DataCache_DX11VertexLayout_GetRef(&s_resourceCache.vertexLayouts, rs->vertexLayout);

    for (uint32 i = 0; i < rs->vertexBufferCount; ++i)
    {
        ID3D11Buffer* vbuf = (ID3D11Buffer*)rs->vertexBuffers[i].buffer;
        ASSERT(vbuf != 0, "Invalid Vertex Buffer %i", i);
        ID3D11DeviceContext_IASetVertexBuffers(m_deviceContext, i, 1, &vbuf, &rs->vertexBuffers[i].stride, &rs->vertexBuffers[i].offset);
    }

    ID3D11DeviceContext_IASetIndexBuffer(m_deviceContext, ibuf, DXGI_FORMAT_R16_UINT, 0);
    ID3D11DeviceContext_IASetPrimitiveTopology(m_deviceContext, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    DX11Shader* shader = DataCache_DX11Shader_GetRef(&s_resourceCache.shaders, rs->shader);

    ID3D11DeviceContext_IASetInputLayout(m_deviceContext, vertexLayout->inputLayout);

    if(rs->constantBuffer.version != 0)
    {
        DX11ConstantBuffer* constantBuffer = DataCache_DX11ConstantBuffer_GetRef(&s_resourceCache.constantBuffers, rs->constantBuffer);
        ID3D11DeviceContext_VSSetConstantBuffers(m_deviceContext, 0, 1, &constantBuffer->buffer);
    }
    ID3D11DeviceContext_VSSetShader(m_deviceContext, shader->vertexShader, NULL, 0);
    ID3D11DeviceContext_PSSetShader(m_deviceContext, shader->fragmentShader, NULL, 0);

    if(rs->texture.version != 0)
    {
        DX11Texture* tex = DataCache_DX11Texture_GetRef(&s_resourceCache.textures, rs->texture);
        ID3D11DeviceContext_PSSetSamplers(m_deviceContext, 0, 1, &tex->samplerState);
        ID3D11DeviceContext_PSSetShaderResources(m_deviceContext, 0, 1, &tex->textureView);
    }
}

int GFX_GetVertexCount()
{
    return 0;
}

void GFX_ResetVertexCount()
{
}

void GFX_Clear(const ClearFlags clearFlags, const Color color)
{
    float c[4];

    // Setup the color to clear the buffer to.
    c[0] = color.r;
    c[1] = color.g;
    c[2] = color.b;
    c[3] = color.a;

    // Clear the back buffer.
    if ((clearFlags & ClearFlags_Color) != 0)
        ID3D11DeviceContext_ClearRenderTargetView(m_deviceContext, m_renderTargetView, c);

    // Clear the depth buffer.
    if ((clearFlags & ClearFlags_Depth) != 0)
        ID3D11DeviceContext_ClearDepthStencilView(m_deviceContext, m_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void Draw_Quad(float x, float y, float width, float height)
{
}

void Draw_Text(float x, float y, char* text)
{
}

void GFX_SetBlendFunc(BlendFunc srcBlend, BlendFunc dstBlend)
{
}

void GFX_DrawIndexed(unsigned int indexCount, unsigned int startIndex)
{
    ID3D11DeviceContext_DrawIndexed(m_deviceContext, indexCount, startIndex, 0);
}

// ------------------------------------------------------------------------------------------
// ----------------------------------------  SHADER  ----------------------------------------
// ------------------------------------------------------------------------------------------

DataHandle GFX_CreateShader(Shader shaderData)
{
    HRESULT result;
    ID3D11VertexShader* vertexShader;
    result = ID3D11Device_CreateVertexShader(m_device, (void*)shaderData.vertexShader.data, (SIZE_T)shaderData.vertexShader.size, NULL, &vertexShader);
    ASSERT(!FAILED(result), "Failed to CreateVertexShader");

    ID3D11PixelShader* pixelShader;
    result = ID3D11Device_CreatePixelShader(m_device, (void*)shaderData.fragmentShader.data, (SIZE_T)shaderData.fragmentShader.size, NULL, &pixelShader);
    ASSERT(!FAILED(result), "Failed to CreatePixelShader");


    DX11Shader shader;
    shader.vertexShader = vertexShader;
    shader.fragmentShader = pixelShader;
    shader.vertexShaderCode = shaderData.vertexShader;
    shader.fragmentShaderCode = shaderData.fragmentShader;
    
    return DataCache_DX11Shader_Add(&s_resourceCache.shaders, shader);
}

void GFX_DestroyShader(DataHandle shaderHandle)
{
    ASSERT(0, "GFX_DestroyShader NOT IMPLEMENTED");
}

DataHandle GFX_CreateTexture(Texture texture)
{
    D3D11_TEXTURE2D_DESC desc = { 0 };
    desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc.Width = texture.width;
    desc.Height = texture.height;
    desc.MipLevels = 1;
    desc.ArraySize = 1;
    desc.SampleDesc.Count = 1;
    desc.SampleDesc.Quality = 0;
    desc.Usage = D3D11_USAGE_DEFAULT;
    desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    desc.CPUAccessFlags = 0;
    desc.MiscFlags = 0;

    D3D11_SUBRESOURCE_DATA subData = { 0 };
    subData.pSysMem = texture.buffer.data;
    subData.SysMemPitch = 4 * texture.width;
    subData.SysMemSlicePitch = 0;

    DX11Texture tex;

    HRESULT result = ID3D11Device_CreateTexture2D(m_device, &desc, &subData, &tex.texture);
    ASSERT(!FAILED(result), "ID3D11Device_CreateTexture2D failed");

    //ID3D11DeviceContext_UpdateSubresource(m_deviceContext, (ID3D11Resource*)texture, 0, NULL, data, 4 * width, 0);
    //ASSERT(!FAILED(result), "ID3D11DeviceContext_UpdateSubresource failed");

    D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc = { 0 };
    viewDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    viewDesc.Texture2D.MipLevels = 1;

    result = ID3D11Device_CreateShaderResourceView(m_device, (ID3D11Resource*)tex.texture, &viewDesc, &tex.textureView);
    ASSERT(!FAILED(result), "ID3D11Device_CreateShaderResourceView failed");

    D3D11_SAMPLER_DESC samplerDesc = { 0 };
    samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.MipLODBias = 0.0f;
    samplerDesc.MaxAnisotropy = 1;
    samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
    samplerDesc.BorderColor[0] = 0;
    samplerDesc.BorderColor[1] = 0;
    samplerDesc.BorderColor[2] = 0;
    samplerDesc.BorderColor[3] = 0;
    samplerDesc.MinLOD = 0;
    samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

    result = ID3D11Device_CreateSamplerState(m_device, &samplerDesc, &tex.samplerState);
    ASSERT(!FAILED(result), "ID3D11Device_CreateSamplerState failed");

    ID3D11DeviceContext_PSSetSamplers(m_deviceContext, 0, 1, &tex.samplerState);
    ID3D11DeviceContext_PSSetShaderResources(m_deviceContext, 0, 1, &tex.textureView);

    return DataCache_DX11Texture_Add(&s_resourceCache.textures, tex);
}