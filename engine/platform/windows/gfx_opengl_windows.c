#ifdef _WIN32
#include <windows.h>
#include "../gl.h"

#include "gfx.h"
#include "print.h"

#pragma comment(lib, "opengl32.lib")

typedef BOOL(WINAPI * PFNWGLSWAPINTERVALEXTPROC) (int interval);
PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT;

void GFX_SetVSync(int on)
{
    wglSwapIntervalEXT(on ? 1 : 0);
}

void Present(HDC hdc)
{
    SwapBuffers(hdc);
}

// RENDERER_WINDOWS

int InitRenderer(HWND hwnd, HGLRC* hglrc)
{
    DebugLine("CreateGLContext");

    PIXELFORMATDESCRIPTOR pfd =
    {
        sizeof(PIXELFORMATDESCRIPTOR),
        1,
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    // Flags
        PFD_TYPE_RGBA,        // The kind of framebuffer. RGBA or palette.
        32,                   // Colordepth of the framebuffer.
        0, 0, 0, 0, 0, 0,
        0,
        0,
        0,
        0, 0, 0, 0,
        24,                   // Number of bits for the depthbuffer
        8,                    // Number of bits for the stencilbuffer
        0,                    // Number of Aux buffers in the framebuffer.
        PFD_MAIN_PLANE,
        0,
        0, 0, 0
    };

    HDC hdc = GetDC(hwnd);

    int pixelFormat = ChoosePixelFormat(hdc, &pfd);

    if (pixelFormat == 0)
    {
        DebugLine("Failed to choose pixel format");
        return 1; // TODO: Error codes
    }

    if (SetPixelFormat(hdc, pixelFormat, &pfd) == 0)
    {
        DebugLine("Failed to set pixel format");
        return 1;
    }

    DebugFormat("Pixel format: %i\n", pixelFormat);

    *hglrc = wglCreateContext(hdc);
    if (hglrc == 0)
    {
        DebugLine("Failed to create GL context");
        return 1;
    }

    if (wglMakeCurrent(hdc, *hglrc) == 0)
    {
        DebugLine("wglMakeCurrent failed");
        return 1;
    }

    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    Debug_AssertErrors("InitRenderer");

    wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");

    return 0;
}

void DeinitRenderer(HWND hwnd, HGLRC hglrc)
{
    DebugLine("DeleteGLContext");

    HDC hdc = GetDC(hwnd);

    wglMakeCurrent(hdc, NULL);
    wglDeleteContext(hglrc);
}
#endif
