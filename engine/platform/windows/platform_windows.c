#include <Windows.h>
#include "util.h"

static int initialized = 0;
static LARGE_INTEGER _tickRate;

void Platform_GetResolution(int* horizontal, int* vertical)
{
    RECT desktop;
    const HWND hDesktop = GetDesktopWindow();
    GetWindowRect(hDesktop, &desktop);
    *horizontal = desktop.right;
    *vertical = desktop.bottom;
}

double Platform_GetTimeMs()
{
    LARGE_INTEGER tick;
    QueryPerformanceCounter(&tick);

    if (initialized == 0)
    {
        QueryPerformanceFrequency(&_tickRate);
        initialized = 1;
    }

    return 1000 * (double)(tick.QuadPart) / _tickRate.QuadPart;
}

uint64 Platform_GetCPUCycles()
{
    return __rdtsc();
}
