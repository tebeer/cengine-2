#include "print.h"

#include <stdio.h>
#include <stdarg.h>

#ifdef _WIN32
#pragma warning( disable : 4996 ) // disable warning from vsprintf
#endif

void DebugLine(const char* str)
{
    printf(str);
    printf("\n");
    fflush(stdout);
}

void DebugFormat(const char* format, ...)
{
    va_list arglist;
    va_start(arglist, format);
    vprintf(format, arglist);
    va_end(arglist);
    printf("\n");
    fflush(stdout);
}

void PrintBuffer(Buffer buffer)
{
    for (uint32 i = 0; i < buffer.size; ++i)
        printf("%c", buffer.data[i]);
    printf("\n");
    fflush(stdout);
}

static char format_string[1024] = { 0 };

// NOTE: this is not thread safe
char* FormatString(const char* format, ...)
{
    va_list arglist;
    va_start(arglist, format);
    vsprintf(format_string, format, arglist);
    va_end(arglist);
    return format_string;
}