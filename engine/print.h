#ifndef _PRINT_H_
#define _PRINT_H_

#include "memory.h"

void PrintBuffer(Buffer buffer);
void DebugLine(const char* str);
void DebugFormat(const char* string, ...);
char* FormatString(const char* format, ...);

#endif
