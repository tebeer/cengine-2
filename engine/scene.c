#include "scene.h"
#include "asset.h"
#include "print.h"
#include "image.h"
#include "texture.h"
#include "mesh.h"
#include "gfx.h"
#include "shader.h"
#include "util.h"

int GetTextureIndex(Scene* scene, String name)
{
    for (unsigned int i = 0; i < scene->textureCount; ++i)
    {
        if (UString_Equals(scene->textures.data[i].name, name))
        {
            //DebugFormat("Texture %s index %i id %i", name, i, scene->textures[i].textureId);
            return scene->textures.data[i].textureId;
        }
    }
    return -1;
}

int GetMeshIndex(Scene* scene, String name)
{
    for (unsigned int i = 0; i < scene->meshCount; ++i)
    {
        if (UString_Equals(scene->meshes.data[i].name, name))
            return scene->meshes.data[i].meshId;
    }
    return -1;
}

int GetShaderIndex(Scene* scene, String name)
{
    for (unsigned int i = 0; i < scene->shaderCount; ++i)
    {
        if (UString_Equals(scene->shaders.data[i].name, name))
            return scene->shaders.data[i].shaderId;
    }
    return -1;
}

int GetObjectIndex(Scene* scene, String name)
{
    for (unsigned int i = 0; i < scene->objectCount; ++i)
    {
        if (UString_Equals(scene->objects.data[i].name, name))
            return i;
    }
    return - 1;
}


float ReadFloat(int assetId)
{
    char buffer[16];
    Asset_ReadString(assetId, buffer);
    return ParseFloat(buffer);
}

Vector3 ReadVector3(int assetId)
{
    Vector3 v;
    v.x = ReadFloat(assetId);
    v.y = ReadFloat(assetId);
    v.z = ReadFloat(assetId);
    return v;
}

Quaternion ReadQuaternion(int assetId)
{
    Quaternion q;
    q.x = ReadFloat(assetId);
    q.y = ReadFloat(assetId);
    q.z = ReadFloat(assetId);
    q.w = ReadFloat(assetId);
    return q;
}

int CompareInstances(Scene* scene, int i1, int i2)
{
    SceneObject o1 = scene->objects.data[scene->instances.data[i1].objectId];
    SceneObject o2 = scene->objects.data[scene->instances.data[i2].objectId];
    if (o1.shaderId < o2.shaderId)
        return -1;
    if (o1.shaderId > o2.shaderId)
        return 1;
    if (o1.textureId < o2.textureId)
        return -1;
    if (o1.textureId > o2.textureId)
        return 1;
    if (o1.meshId < o2.meshId)
        return -1;
    if (o1.meshId > o2.meshId)
        return 1;
    return 0;
}

void SortInstances(Scene* scene)
{
    for (unsigned int i = 1; i < scene->instanceCount; ++i)
    {
        unsigned int j = i;
        while (j > 0 && CompareInstances(scene, j, j - 1) > 0)
        {
            SceneInstance temp = scene->instances.data[j - 1];
            scene->instances.data[j - 1] = scene->instances.data[j];
            scene->instances.data[j] = temp;
            j--;
        }
    }
}

Scene Scene_Load(const char* name)
{
    int asset = Asset_Open(name);

    Scene scene = { 0 };

    if (asset < 0)
    {
        DebugFormat("Failed to open scene %s", name);
        return scene;
    }

    scene.shaders = (ShaderArray)Array_Init(SceneShader);
    scene.textures = (TextureArray)Array_Init(SceneTexture);
    scene.meshes = (SceneMeshArray)Array_Init(SceneMesh);
    scene.objects = (ObjectArray)Array_Init(SceneObject);
    scene.instances = (InstanceArray)Array_Init(SceneInstance);

    String tempstr = UString_Fixed(256);

    int texturesCapacity = 0;
    int meshesCapacity = 0;
    int objectsCapacity = 0;
    int instancesCapacity = 0;

    int currentShader = 0;
    int currentObj = 0;
    int currentInstance = 0;

    while (Asset_ReadUString(asset, &tempstr) > 0)
    {
        if (UString_Equals(tempstr, UString("shader")))
        {
            // Read name
            Asset_ReadUString(asset, &tempstr);

            if (scene.shaderCount >= scene.shaders.length)
            {
                int newCapacity = scene.shaders.length == 0 ? 2 : scene.shaders.length * 2;
                Array_Resize(scene.shaders, newCapacity);
            }

            currentShader = scene.shaderCount++;
            scene.shaders.data[currentShader].name = UString_Duplicate(tempstr);

            String vertName = UString_Fixed(64);
            String fragName = UString_Fixed(64);

            Asset_ReadUString(asset, &tempstr);
            if (UString_Equals(tempstr, UString("vert")))
                Asset_ReadUString(asset, &vertName);

            Asset_ReadUString(asset, &tempstr);
            if (UString_Equals(tempstr, UString("frag")))
                Asset_ReadUString(asset, &fragName);

            Buffer vert, frag;
            assert(Asset_ReadAll(vertName, &vert) && "Could not load vertex shader");
            assert(Asset_ReadAll(fragName, &frag) && "Could not load fragment shader");

            int shaderId = GFX_CreateShader(vert, frag);

            Buffer_Free(&vert);
            Buffer_Free(&frag);

            scene.shaders.data[currentShader].shaderId = shaderId;
            scene.shaders.data[currentShader].mainTexProp = Shader_GetPropertyID(shaderId, "_MainTex");
        }
        else if (UString_Equals(tempstr, UString("texture")))
        {
            // Read texture name
            Asset_ReadUString(asset, &tempstr);

            if (scene.textureCount >= scene.textures.length)
            {
                int newCapacity = scene.textures.length == 0 ? 2 : scene.textures.length * 2;
                Array_Resize(scene.textures, newCapacity);
            }

            scene.textures.data[scene.textureCount].name = UString_Duplicate(tempstr);

            // Texture file name
            Asset_ReadUString(asset, &tempstr);

            Buffer imgData;

            if (Asset_ReadAll(tempstr, &imgData) != 0)
            {
                Image image;
                if (Image_LoadZPNG(imgData, &image) != 0)
                {
                    TextureFormat format;
                    if (image.channels == 3)
                        format = TextureFormat_RGB;
                    else if (image.channels == 4)
                        format = TextureFormat_RGBA;
                    else
                        assert(0 && "Invalid channel count");

                    int textureId = GFX_CreateTexture(image.width, image.height, format, image.buffer.data);
                    if (textureId != 0)
                    {
                        scene.textures.data[scene.textureCount].textureId = textureId;
                    }

                    Buffer_Free(&image.buffer);
                }
                Buffer_Free(&imgData);
            }

            scene.textureCount++;
        }
        else if (UString_Equals(tempstr, UString("mesh")))
        {
            // Read mesh name
            Asset_ReadUString(asset, &tempstr);

            if (scene.meshCount >= scene.meshes.length)
            {
                int newCapacity = scene.meshes.length == 0 ? 2 : scene.meshes.length * 2;
                Array_Resize(scene.meshes, newCapacity);
            }

            scene.meshes.data[scene.meshCount].name = UString_Duplicate(tempstr);

            // Mesh file path
            Asset_ReadUString(asset, &tempstr);
            
            int meshId = Mesh_Load(tempstr, UString(""));
            scene.meshes.data[scene.meshCount].meshId = meshId;

            if (meshId != -1)
            {
                Mesh_UploadToGPU(meshId);
            }

            scene.meshCount++;
        }
        else if (UString_Equals(tempstr, UString("object")))
        {
            if (scene.objectCount >= scene.objects.length)
            {
                int newCapacity = scene.objects.length == 0 ? 2 : scene.objects.length * 2;
                Array_Resize(scene.objects, newCapacity);
            }

            currentObj = scene.objectCount++;

            // Read name
            Asset_ReadUString(asset, &tempstr);
            scene.objects.data[currentObj].name = UString_Duplicate(tempstr);
        }
        else if (UString_Equals(tempstr, UString("shaderref")))
        {
            Asset_ReadUString(asset, &tempstr);
            scene.objects.data[currentObj].shaderId = GetShaderIndex(&scene, tempstr);
        }
        else if (UString_Equals(tempstr, UString("textureref")))
        {
            Asset_ReadUString(asset, &tempstr);
            scene.objects.data[currentObj].textureId = GetTextureIndex(&scene, tempstr);
        }
        else if (UString_Equals(tempstr, UString("meshref")))
        {
            Asset_ReadUString(asset, &tempstr);
            scene.objects.data[currentObj].meshId = GetMeshIndex(&scene, tempstr);
        }
        else if (UString_Equals(tempstr, UString("instance")))
        {
            // Read name
            Asset_ReadUString(asset, &tempstr);

            int objectIndex = GetObjectIndex(&scene, tempstr);
            if (objectIndex != -1)
            {
                if (scene.instanceCount >= scene.instances.length)
                {
                    int newCapacity = scene.instances.length == 0 ? 2 : scene.instances.length * 2;
                    Array_Resize(scene.instances, newCapacity);
                }

                currentInstance = scene.instanceCount++;
                scene.instances.data[currentInstance].objectId = objectIndex;
            }
            else
            {
                DebugFormat("Object ID not found for %s", tempstr.buffer.data);
                continue;
            }

            Transform t = { 0 };
            t.position = (Vector3) { 0 };
            t.rotation = QuaternionIdentity();
            t.scale = (Vector3) { 1, 1, 1 };

            scene.instances.data[currentInstance].transform = t;
        }
        else if (UString_Equals(tempstr, UString("position")))
        {
            Vector3 pos = ReadVector3(asset);
            scene.instances.data[currentInstance].transform.position = pos;
        }
        else if (UString_Equals(tempstr, UString("rotation")))
        {
            Quaternion q = ReadQuaternion(asset);
            scene.instances.data[currentInstance].transform.rotation = q;
        }
        else if (UString_Equals(tempstr, UString("scale")))
        {
            Vector3 scale = ReadVector3(asset);
            scene.instances.data[currentInstance].transform.scale = scale;
        }
        else
        {
            if (tempstr.length > 0)
                DebugFormat("Unknown identifier %s", tempstr.buffer.data);
            break;
        }
    }

    Asset_Close(asset);

    SortInstances(&scene);

    return scene;
}

void Scene_Draw(Scene* scene)
{
    int shaderChanges = 0;
    int textureChanges = 0;
    int meshChanges = 0;
    int lastShader = -1;
    int lastTex = -1;
    int lastMesh = -1;

    for (unsigned int i = 0; i < scene->instanceCount; ++i)
    {
        SceneInstance inst = scene->instances.data[i];
        SceneObject o = scene->objects.data[inst.objectId];
        SceneShader shader = scene->shaders.data[o.shaderId];

        if (o.shaderId != lastShader)
        {
            Shader_Use(shader.shaderId);
            Shader_UpdateVPMatrix();
            lastShader = shader.shaderId;
            shaderChanges++;
        }

        if (o.textureId == 0)
            continue;

        if (o.textureId != lastTex)
        {
            lastTex = o.textureId;
            Shader_SetTextureID(shader.mainTexProp, o.textureId);
            textureChanges++;
        }

        if (o.meshId != lastMesh)
        {
            lastMesh = o.meshId;
            Mesh_Use(o.meshId);
            meshChanges++;
        }

        Draw_Mesh(inst.transform);
    }
   
    //DebugFormat("ShaderChanges: %i TextureChanges: %i MeshChanges: %i", shaderChanges, textureChanges, meshChanges);

    Mesh_Unbind();
}