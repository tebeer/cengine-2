#ifndef _SCENE_H_
#define _SCENE_H_

#include "math2.h"
#include "ustring.h"
#include "array.h"

typedef struct SceneShader
{
    int shaderId;
    unsigned int mainTexProp;
    String name;
} SceneShader;

typedef struct SceneTexture
{
    int textureId;
    String name;
} SceneTexture;

typedef struct SceneMesh
{
    int meshId;
    String name;
} SceneMesh;

typedef struct SceneObject
{
    int meshId;
    int textureId;
    int shaderId;
    String name;
} SceneObject;

typedef struct SceneInstance
{
    Transform transform;
    int objectId;
} SceneInstance;

typedef ArrayType(SceneShader) ShaderArray;
typedef ArrayType(SceneTexture) TextureArray;
typedef ArrayType(SceneMesh) SceneMeshArray;
typedef ArrayType(SceneObject) ObjectArray;
typedef ArrayType(SceneInstance) InstanceArray;

typedef struct Scene
{
    ShaderArray shaders;
    unsigned int shaderCount;
    TextureArray textures;
    unsigned int textureCount;
    SceneMeshArray meshes;
    unsigned int meshCount;

    ObjectArray objects;
    unsigned int objectCount;
    InstanceArray instances;
    unsigned int instanceCount;
} Scene;

Scene Scene_Load(const char* name);
void Scene_Draw();

#endif