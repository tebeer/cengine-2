#ifndef _SHADER_H_
#define _SHADER_H_

#include "util.h"
#include "memory.h"

typedef struct ShaderConstant_t
{
    char name[32];
    uint8 location;
    uint8 size;
} ShaderConstant;

typedef struct ShaderResourceBinding_t
{
    char name[32];
    uint8 location;
} ShaderResourceBinding;

typedef struct ShaderConstantLayout_t
{
    ShaderConstant vars[16];
    uint8 varCount;
    ShaderResourceBinding resources[16];
    uint8 resourceCount;
} ShaderConstantLayout;

typedef struct Shader_t
{
    Buffer vertexShader;
    ShaderConstantLayout vertexConstantLayout;
    Buffer fragmentShader;
    ShaderConstantLayout fragmentConstantLayout;
} Shader;

#endif