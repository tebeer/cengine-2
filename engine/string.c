#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "ustring.h"
#include "memory.h"
#include <stdlib.h>
#include <assert.h>
#include "print.h"
#include "util.h"

//===========================
// Custom string type
//===========================

//String UString(unsigned char cstring[])
//{
//    return (String) {
//        .buffer.data = cstring,
//        .buffer.size = strlen(cstring),
//        .length = strlen(cstring),
//    };
//}

String UString_Create(unsigned int length)
{
    String str;
    str.buffer = Buffer_Create(GetAllocInfo(length + 1)); // Add null terminator
    str.length = length;
    return str;
}

void UString_Set(String* str1, String str2)
{
    ASSERT(str1->buffer.size >= str2.length + 1, "UString_Set: Buffer overrun %i %i", str1->buffer.size, str2.length + 1);

    Memory_Copy(str2.buffer.data, str1->buffer.data, str2.length + 1);
    str1->length = str2.length;
}

void UString_Append(String* str1, String str2)
{
    ASSERT(str1->buffer.size >= str1->length + str2.length + 1, "UString_Append: Buffer overrun %i %i", str1->buffer.size, str1->length + str2.length + 1);

    Memory_Copy(str2.buffer.data, str1->buffer.data + str1->length, str2.length + 1);
    str1->length += str2.length;
}

void UString_Format(String* str, const char* format, ...)
{
    va_list arglist;
    va_start(arglist, format);
    vsnprintf(str->buffer.data, str->buffer.size, format, arglist);
    va_end(arglist);
}

uint32 GetFormattedLength(const char* format, va_list arglist)
{
    return vsnprintf(NULL, 0, format, arglist);
}

String UString_CreateFormat(const char* format, ...)
{
    va_list arglist;
    va_start(arglist, format);
    String str = UString_Create(GetFormattedLength(format, arglist));
    vsnprintf(str.buffer.data, str.buffer.size, format, arglist);
    va_end(arglist);
    return str;
}

String UString_Concatenate(String str1, String str2)
{
    unsigned int totalLength = str1.length + str2.length;

    String str = UString_Create(totalLength);
    Memory_Copy(str1.buffer.data, str.buffer.data, str1.length);
    Memory_Copy(str2.buffer.data, str.buffer.data + str1.length, str2.length);
    str.buffer.data[totalLength] = '\0';

    return str;
}

int UString_Equals(String str1, String str2)
{
    if (str1.length != str2.length)
        return 0;

    for (unsigned int i = 0; i < str1.length; ++i)
    {
        if (str1.buffer.data[i] != str2.buffer.data[i])
            return 0;
    }

    return 1;
}

String UString_Duplicate(String str)
{
    String newStr = UString_Create(str.length);
    Memory_Copy(str.buffer.data, newStr.buffer.data, str.length);
    return newStr;
}

String UString_FromBuffer(Buffer buffer)
{
    String str;
    str.buffer = buffer;
    str.length = buffer.size - 1;
    assert(str.buffer.data[str.length] == '\0' && "Buffer is not null-terminated");

    return str;
}

void UString_Replace(String str, char c1, char c2)
{
    for (unsigned int i = 0; i < str.length; ++i)
        if (str.str[i] == c1)
            str.str[i] = c2;
}

void UString_Free(String* str)
{
    Buffer_Free(&str->buffer);
    *str = (String) { 0 };
}

String Path_ChangeExtension(String fname, String ext)
{
    int32 lastDotIndex = -1;
    for (uint32 i = 0; i < fname.length; ++i)
    {
        if (fname.str[i] == '.')
            lastDotIndex = (int32)i;
    }

    ASSERT(lastDotIndex >= 0, "Dot index not found %s %i", fname.str, fname.length);

    String newStr = UString_Create(lastDotIndex + 1 + ext.length);
    Memory_Copy(fname.str, newStr.str, lastDotIndex);
    Memory_Copy(ext.str, newStr.str + lastDotIndex + 1, ext.length);
    newStr.str[lastDotIndex] = '.';

    return newStr;
}

String Path_GetDirectory(String fname)
{
    uint32 i;
    for (i = fname.length - 1; i >= 0; --i)
        if (fname.str[i] == '\\' || fname.str[i] == '/')
            break;

    ASSERT(i > 0, "Path_GetDirectory failed: %s", fname.str);

    String dir = UString_Create(i+1);
    Memory_Copy(fname.str, dir.str, dir.length);
    return dir;
}

String Path_GetExtension(String fname)
{
    unsigned int i = 0;
    unsigned int lastDotIndex = -1;
    while (i < fname.length && fname.str[i] != '\0')
    {
        if (fname.str[i] == '.')
            lastDotIndex = i;
        i++;
    }

    if (lastDotIndex == -1)
        return (String){ 0 };

    lastDotIndex++;

    String ext = UString_Create(fname.length - lastDotIndex);
    for (i = lastDotIndex; i < fname.length; ++i)
        ext.str[i - lastDotIndex] = fname.str[i];
    ext.str[ext.length] = '\0';

    return ext;
}