#ifdef TEMPLATE_START
#error TEMPLATE_START already defined
#endif

#define TEMPLATE_START

#define CAT3(x, y, z) _CAT3(x, y, z)
#define TO_STRING(d) _TO_STRING(d)
#define _CAT3(x, y, z) x ## y ## z
#define _TO_STRING(d) #d

#define Template(type, func) CAT3(type, _, func)
