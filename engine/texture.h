#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "util.h"
#include "memory.h"

typedef enum TextureFilterMode_t
{
    TextureFilter_Point,
    TextureFilter_Bilinear,
    TextureFilter_Trilinear,
} TextureFilterMode;

typedef enum TextureFormat_t
{
    TextureFormat_RGB,
    TextureFormat_RGBA,
} TextureFormat;

typedef struct Texture_t
{
    uint16 width;
    uint16 height;
    TextureFormat format;
    TextureFilterMode filterMode;
    Buffer buffer;
} Texture;

#endif