#ifndef _USER_H_
#define _USER_H_

#include "input.h"

typedef struct AppContext_t
{
    unsigned char* fixedMemory;
    unsigned int fixedMemorySize;
    unsigned char* cacheMemory;
    unsigned int cacheSize;
    double deltaTime;
    double currentTime;
    struct InputState_t input;
} AppContext;

void User_Init(AppContext* ctx);
void User_Update(AppContext* ctx);

#endif