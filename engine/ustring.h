#ifndef _USTRING_H_
#define _USTRING_H_

#include "memory.h"
#include <string.h>

struct String
{
    union StringBuffer
    {
        Buffer buffer;
        char* str;
    };
    unsigned int length;
};

typedef struct String String;

//String UString(unsigned char cstring[]);
String UString_Create(unsigned int length);
void UString_Set(String* str1, String str2);
void UString_Format(String* str, const char* format, ...);
String UString_CreateFormat(const char* format, ...);
String UString_StackFormat(const char* format, ...);
String UString_Concatenate(String str1, String str2);
int UString_Equals(String str1, String str2);
String UString_Duplicate(String str);
String UString_FromBuffer(Buffer buffer);
void UString_Replace(String str, char c1, char c2);
void UString_Free(String* str);
String Path_GetDirectory(String fname);
String Path_ChangeExtension(String fname, String ext);
String Path_GetExtension(String fname);

#define UString(str) (String) { .buffer.data = str, .buffer.size = (unsigned int)sizeof(str), .length = (unsigned int)sizeof(str) - 1 }
#define UString_Const(str) { .buffer.data = str, .buffer.size = (unsigned int)sizeof(str), .length = (unsigned int)sizeof(str) - 1 }
#define UString_Cstr(str) (String) { .buffer.data = str, .buffer.size = (unsigned int)strlen(str) + 1, .length = (unsigned int)strlen(str) }
#define STRING_STACK(_length) (String) { .buffer.data = STACK_ALLOC(_length+1), .buffer.size = _length+1, .length = _length }

#endif