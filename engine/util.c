#include "util.h"
#include <string.h>
#include <stdlib.h>

#ifdef _WIN32
#pragma warning( disable : 4996 )
#endif

float ParseFloat(unsigned char* str) 
{
    return (float)atof(str);
}

int ParseInt(unsigned char* str)
{
    return (int)atoi(str);
}

float Random()
{
    return (float)rand() / RAND_MAX;
}

float RandomFloat(float min, float max)
{
    return min + (float)rand() / RAND_MAX * (max - min);
}

int RandomInt(int min, int max)
{
    return min + rand() % (max - min);
}

size_t Assert(size_t cond, const char* file, long line, const char* format, ...)
{
    if (cond == 0)
    {
        printf("ASSERTION FAILED\n");
        printf(" => %s : %d\n => ", file, line);
        va_list arglist;
        va_start(arglist, format);
        vprintf(format, arglist);
        va_end(arglist);
        printf("\n");
        fflush(stdout);
        abort();
    }
    return cond;
}