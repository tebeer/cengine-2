#include "asset.h"

#ifdef _WIN32
#include <windows.h>
#pragma warning( disable : 4996 )
#elif linux
#include <libgen.h>
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "print.h"
#include "ustring.h"

// TODO: support for "resource packs"

static FILE **_asset_files = NULL;
static int _asset_count = 0;

typedef struct assetPath_t
{
    const char *path;
    struct assetPath_t *next;
} AssetPath;
AssetPath *_asset_paths = NULL;

const char pathSeparator =
#ifdef _WIN32
    '\\';
#else
    '/';
#endif

#define MIN(a,b) ((a) < (b) ? (a) : (b))

size_t _Path_Join(const unsigned char *a, const unsigned char *b, unsigned char *buf, size_t buflen)
{
    size_t alen = strlen(a);
    size_t blen = strlen(b);
    size_t len = alen + blen + 2;
    if (len > buflen) return 0;
    memcpy(buf, a, alen);
    buf[alen] = pathSeparator;
    memcpy(buf+alen+1, b, blen);
    buf[len-1] = '\0';
    return len;
}

int Asset_GetFullPath(const char* assetname, unsigned char* fullpath)
{
    DebugFormat("Finding asset %s", assetname);

    AssetPath *p = _asset_paths;
    while (p)
    {
        size_t pathlen = strlen(p->path);
        size_t namelen = strlen(assetname);
        size_t fullen = pathlen + 1 + namelen + 1;

        fullen = _Path_Join(p->path, assetname, fullpath, fullen);
#ifdef _WIN32
        if (GetFileAttributes(fullpath) != INVALID_FILE_ATTRIBUTES)
        {
            break;
        }
#else
        if (access(name, F_OK) != -1) {
            break;
        }
#endif

        p = p->next;
    }

    if (!p)
        return 0;

    return 1;
}

int Asset_Open(const char *_name)
{
    if (!_name)
        return -1;

    char fullpath[1024];

    // Was not found in asset paths
    if (Asset_GetFullPath(_name, fullpath) == 0)
    {
        DebugFormat("Not found in any asset path %s", _name);
        return -1;
    }

    DebugFormat("Opening asset from %s", fullpath);

    // TODO: find file
    FILE *f = fopen(fullpath, "rb");
    if (!f)
    {
        DebugFormat("Failed to read asset from %s", fullpath);
        return -1;
    }

    _asset_count++;
    if (!_asset_files)
        _asset_files = malloc(sizeof(FILE*));
    else
        _asset_files = realloc(_asset_files, sizeof(FILE*)*_asset_count);

    _asset_files[_asset_count-1] = f;

    return _asset_count-1;
}

int Asset_Valid(int asset)
{
    return (asset >= 0 && asset < _asset_count);
}

unsigned int Asset_Size(int asset)
{
    if (!Asset_Valid(asset)) return 0;
    FILE *f = _asset_files[asset];
    long pos = ftell(f);
    fseek(f, 0L, SEEK_END);
    long ret = ftell(f);
    fseek(f, pos, SEEK_SET);
    return ret;
}

unsigned int Asset_Read(int asset, void *ptr, size_t size, size_t nmemb)
{
    if (!Asset_Valid(asset)) return 0;
    FILE *f = _asset_files[asset];
    if (!f) return 0;
    return fread(ptr, size, nmemb, f);
}

void SkipWhiteSpace(FILE* file)
{
    int c = fgetc(file);

    while (c != EOF && isspace(c))
    {
        c = fgetc(file);
    }
    ungetc(c, file);
}

unsigned int Asset_ReadString(int asset, char* buffer)
{
    if (!Asset_Valid(asset)) return 0;
    FILE *file = _asset_files[asset];
    SkipWhiteSpace(file);

    int c = fgetc(file);
    int pos = 0;

    while (c != EOF && !isspace(c))//(char)c != ' ')
    {
        buffer[pos++] = (char)c;
        c = fgetc(file);
    }
    buffer[pos] = 0;

    //printf("ReadString %s\n", buffer);

    return pos;
}

int Asset_ReadUString(int asset, String* str)
{
    if (!Asset_Valid(asset))
        return 0;

    FILE *file = _asset_files[asset];
    SkipWhiteSpace(file);

    int c = fgetc(file);
    str->length = 0;

    while (c != EOF && !isspace(c))
    {
        // Fixed buffer size
        if (str->length < str->buffer.size)
            str->buffer.data[str->length++] = (char)c;

        c = fgetc(file);
    }

    str->buffer.data[str->length] = '\0';

    return 1;
}

void Asset_Close(int asset)
{
    if (!Asset_Valid(asset)) return;
    FILE *f = _asset_files[asset];
    fclose(f);
    _asset_files[asset] = NULL;
}

int Asset_ReadAll(String name, Buffer* out)
{
    int asset = Asset_Open(name.buffer.data);

    if (!Asset_Valid(asset))
        return 0;

    size_t size = Asset_Size(asset);
    *out = Buffer_Create(GetAllocInfo(size));
    Asset_Read(asset, out->data, 1, size);

    return 1;
}

String Asset_ReadAllText(String name)
{
    int asset = Asset_Open(name.buffer.data);

    if (!Asset_Valid(asset))
        return (String) { 0 };

    size_t size = Asset_Size(asset);
    Buffer buffer = Buffer_Create(GetAllocInfo(size + 1));
    Asset_Read(asset, buffer.data, 1, size);

    // Add a null terminator
    buffer.data[size] = 0;
    return UString_FromBuffer(buffer);
}

void Asset_Register_Path(const char *path)
{
    if (!path) return;
    DebugFormat("Registering asset path %s", path);
    AssetPath *next = _asset_paths;
    _asset_paths = malloc(sizeof(AssetPath));
    char *buf = malloc(strlen(path)+1);
    strcpy(buf, path);
    _asset_paths->path = buf;
    _asset_paths->next = next;
}

int _getexedir(char *buf, size_t buflen)
{
#ifdef _WIN32
    int bytes = GetModuleFileName(NULL, buf, buflen);
    if (bytes == 0)
        return -1;
    // TODO: split path to get dirname
#define DRIVELEN 8
    char drive[DRIVELEN];
#define DIRLEN 256
    char dir[DIRLEN];
    int r = _splitpath_s(buf, drive, DRIVELEN, dir, DIRLEN, NULL, 0, NULL, 0);
    if (r != 0) return -1;
    int drivelen = strlen(drive);
    int dirlen = strlen(dir);
    strncpy(buf, drive, drivelen);
    strncpy(buf + drivelen, dir, dirlen);
    buf[drivelen + dirlen - 1] = '\0'; // Ignore trailing backslash
    return drivelen + dirlen;
#elif linux
    char szTmp[32];
    sprintf(szTmp, "/proc/%d/exe", getpid());
    int bytes = MIN(readlink(szTmp, buf, buflen), buflen - 1);
    if (bytes >= 0)
        buf[bytes] = '\0';
    buf = dirname(buf);
    return strlen(buf);
#else
    return -1; // Platform not supported
#endif
}

void Asset_Register_Default_Path()
{
#define BUFLEN 256
    char exedir[BUFLEN];
    int len = _getexedir(exedir, BUFLEN);
    if (len > 0) {
        len = _Path_Join(exedir, "assets", exedir, BUFLEN);
        if (len > 0)
            Asset_Register_Path(exedir);
    }
}

void Asset_Write(String fname, unsigned char* buffer, unsigned int size)
{
    FILE* file = fopen(fname.buffer.data, "wb");

    if (file != 0)
    {

        fwrite(buffer, 1, size, file);
        fclose(file);
    }
}
