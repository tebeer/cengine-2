#ifndef _ASSET_H_
#define _ASSET_H_

#include "ustring.h"
#include "memory.h"

int Asset_GetFullPath(const char* assetname, unsigned char* fullpath);
int Asset_Open(const char *name);
int Asset_Valid(int asset);
unsigned int Asset_Size(int asset);
unsigned int Asset_Read(int asset, void *ptr, unsigned int size, unsigned int nmemb);
unsigned int Asset_ReadString(int asset, char* buffer);
int Asset_ReadUString(int asset, String* str);
void Asset_Close(int asset);
void Asset_Register_Path(const char *path);
void Asset_Register_Default_Path();
int Asset_ReadAll(String name, Buffer* out);
String Asset_ReadAllText(String name);
void Asset_Write(String fname, unsigned char* buffer, unsigned int size);

#endif // _ASSET_H_
