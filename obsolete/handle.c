#include "print.h"
#include "memory.h"
#include "handle.h"
#include <assert.h>

#define INDEX_SIZE 12
#define VERSION_SIZE 15
#define TYPE_SIZE 32 - INDEX_SIZE - VERSION_SIZE
#define INDEX_MAX 1 << INDEX_SIZE
#define VERSION_MAX 1 << VERSION_SIZE
#define TYPE_MAX 1 << (TYPE_SIZE)

struct Handle
{
    unsigned int index : INDEX_SIZE;
    unsigned int version : VERSION_SIZE;
    unsigned int type : TYPE_SIZE;
};

struct Entry
{
    unsigned int nextFreeIndex : INDEX_SIZE;
    unsigned int version : VERSION_SIZE;
    unsigned int active : 1;
    //unsigned int end : 1;
};

struct Roster
{
    struct Entry* entries;
    DataType* data;
    unsigned int totalCount;
    unsigned int activeCount;
    unsigned int freeIndex : INDEX_SIZE;
    unsigned int type : TYPE_SIZE;
};

void Roster_Resize(struct Roster* roster, unsigned int newCount)
{
    assert(newCount <= INDEX_MAX);
    DebugFormat("Roster_Resize: %i => %i", roster->totalCount, newCount);

    if (roster->totalCount == 0)
    {
        roster->entries = Memory_Alloc(newCount * sizeof(struct Entry));
        roster->data = Memory_Alloc(newCount * sizeof(DataType));
    }
    else
    {
        roster->entries = Memory_Realloc(roster->entries, newCount * sizeof(struct Entry));
        roster->data = Memory_Realloc(roster->data, newCount * sizeof(DataType));
    }

    for (unsigned int i = roster->totalCount; i < newCount; ++i)
    {
        roster->entries[i] = (struct Entry){ 0 };
        roster->entries[i].nextFreeIndex = i + 1;
        roster->data[i] = (DataType){ 0 };
    }
    //m_entries[MaxEntries - 1] = HandleEntry();
    //m_entries[MaxEntries - 1].m_endOfList = true;

    roster->totalCount = newCount;
}

struct Roster Roster_Create(unsigned int initialCount, unsigned int type)
{
    DebugFormat("Roster_Create: %i", initialCount);
    struct Roster roster = { 0 };
    //roster.elementSize = elementSize;

    roster.entries = 0;
    roster.freeIndex = 0;
    roster.type = type;

    if(initialCount > 0)
        Roster_Resize(&roster, initialCount);

    return roster;
}

struct Handle Roster_Add(struct Roster* roster, DataType data)
{
    assert(roster->activeCount < INDEX_MAX);
    assert(roster->activeCount < roster->totalCount);
    roster->activeCount++;
    //if (roster->activeCount > roster->totalCount)
    //{
    //   /unsigned int newCount;
    //   /if (roster->totalCount == 0)
    //   /    newCount = 2;
    //   /else
    //   /    newCount = roster->totalCount * 2;
    //   /if (newCount > INDEX_MAX)
    //   /    newCount = INDEX_MAX;
    //   /Roster_Resize(roster, newCount);
    //}

    unsigned int i = roster->freeIndex;
    assert(i < INDEX_MAX);
    assert(roster->entries[i].active == 0);
    //assert(roster->entries[i].end == 0);

    roster->freeIndex = roster->entries[i].nextFreeIndex;

    roster->entries[i].nextFreeIndex = -1;
    roster->entries[i].version++;
    roster->entries[i].active = 1;

    roster->data[i] = data;

    struct Handle handle;
    handle.index = i;
    handle.type = roster->type;
    handle.version = roster->entries[i].version;
    return handle;
}

DataType Roster_Get(struct Roster roster, struct Handle handle)
{
    struct Entry entry = roster.entries[handle.index];

    assert(entry.active != 0 && "Roster_Get: Inactive handle");
    assert(entry.version == handle.version && "Roster_Get: Obsolete handle");
    assert(roster.type == handle.type && "Roster_Get: Type mismatch");

    return roster.data[handle.index];
}

void Roster_Remove(struct Roster* roster, struct Handle handle)
{
    unsigned int index = handle.index;
    assert(roster->entries[index].active != 0 && "Roster_Remove: Inactive handle");
    assert(roster->entries[index].version == handle.version && "Roster_Remove: Obsolete handle");
    assert(roster->type == handle.type && "Roster_Remove: Type mismatch");

    roster->entries[index].nextFreeIndex = roster->freeIndex;
    roster->entries[index].active = 0;
    roster->freeIndex = index;

    roster->activeCount--;
}

void Roster_Debug()
{
    struct Roster roster = Roster_Create(0, 1);

    struct Handle handle1 = Roster_Add(&roster, 1);
    struct Handle handle2 = Roster_Add(&roster, 2);
    struct Handle handle3 = Roster_Add(&roster, 3);
    struct Handle handle4 = Roster_Add(&roster, 5);

    //Roster_Remove(&roster, handle2);
    handle3 = Roster_Add(&roster, 7);

    DebugFormat("%i %i", handle1.index, Roster_Get(roster, handle1));
    DebugFormat("%i %i", handle2.index, Roster_Get(roster, handle2));
    DebugFormat("%i %i", handle3.index, Roster_Get(roster, handle3));
    DebugFormat("%i %i", handle4.index, Roster_Get(roster, handle4));
}