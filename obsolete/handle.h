#ifndef _HANDLE_H_
#define _HANDLE_H_

struct Roster;
struct Handle;

typedef int DataType;

struct Roster Roster_Create(unsigned int initialCount, unsigned int type);
struct Handle Roster_Add(struct Roster* roster, DataType data);
DataType Roster_Get(struct Roster roster, struct Handle handle);
void Roster_Remove(struct Roster* roster, struct Handle handle);
void Roster_Debug();

#endif