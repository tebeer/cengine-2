
struct ListNode
{
    struct ListNode* prev;
    struct ListNode* next;
};

void List_Create(struct ListNode* node)
{
    node->prev = node;
    node->next = node;
}

void List_Add(struct ListNode* node, struct ListNode* prev, struct ListNode* next)
{
    node->next = next;
    node->prev = prev;
    next->prev = node;
    prev->next = node;
}

void List_Remove(struct ListNode* node)
{
    node->next->prev = node->prev;
    node->prev->next = node->next;
    node->next = 0;
    node->prev = 0;
}

void List_AddHead(struct ListNode* list, struct ListNode* node)
{
    List_Add(node, list, list->next);
}
