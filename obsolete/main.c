#include "gfx.h"
#include "application.h"
#include "mesh.h"
#include "texture.h"
#include "print.h"
#include "input.h"
#include "math2.h"
#include "shader.h"

#define CAR_COUNT 100

float x = -1;
float z = 0;

int car1mesh;
int car2mesh;
int characterMesh;

int car1tex;
int car2tex;
int grassTex;
int characterTex;

unsigned int mainTexProp;

int shaderID;

int groundMesh;

float fpsTime = 0;
int frames = 0;
float fps = 0;

float mouse_x = 0;
float mouse_y = 0;

Transform cameraTransform;
Transform groundTransform;
Transform characterTransform;

typedef struct Car
{
    Transform transform;
    Quaternion localRotation;
    int id;
} Car;

Car cars[CAR_COUNT];

void Init()
{
    car1mesh = Mesh_Load("car-1.mesh", 0);
    car2mesh = Mesh_Load("car-2.mesh", 0);
    characterMesh = Mesh_Load("bomber.mesh", "bomber_LOD0");

    car1tex = Texture_Load("car-1.png");
    car2tex = Texture_Load("car-2.png");
    grassTex = Texture_Load("grass-1.png");
    characterTex = Texture_Load("bomber.png");

    Mesh_UploadToGPU(car1mesh);
    Mesh_UploadToGPU(car2mesh);
    Mesh_UploadToGPU(characterMesh);

    groundMesh = Mesh_Create();
    
    Mesh_SetVertexCount(groundMesh, 4);

    Mesh_SetVertex(groundMesh, 0, (Vertex) {-1, 0, 1 });
    Mesh_SetVertex(groundMesh, 1, (Vertex) { 1, 0, 1 });
    Mesh_SetVertex(groundMesh, 2, (Vertex) { 1, 0,-1 });
    Mesh_SetVertex(groundMesh, 3, (Vertex) {-1, 0,-1 });

    Mesh_EnableUVs(groundMesh);
    Mesh_SetUV(groundMesh, 0, (UV) { 0, 0 });
    Mesh_SetUV(groundMesh, 1, (UV) {10, 0 });
    Mesh_SetUV(groundMesh, 2, (UV) {10, 10});
    Mesh_SetUV(groundMesh, 3, (UV) { 0, 10});

    Mesh_SetIndexCount(groundMesh, 6);
    Mesh_SetIndex(groundMesh, 0, 0);
    Mesh_SetIndex(groundMesh, 1, 1);
    Mesh_SetIndex(groundMesh, 2, 2);
    Mesh_SetIndex(groundMesh, 3, 2);
    Mesh_SetIndex(groundMesh, 4, 3);
    Mesh_SetIndex(groundMesh, 5, 0);

    Mesh_UploadToGPU(groundMesh);

    groundTransform.position = (Vector3) { 0, -10, 0 };
    groundTransform.scale = (Vector3) { 200, 200, 200 };
    groundTransform.rotation = QuaternionIdentity();

    cameraTransform.rotation = QuaternionFromEuler(45, 0, 0);
    cameraTransform.scale = (Vector3) { 1, 1, 1 };
    cameraTransform.position = (Vector3) { 0, 150, 180 };

    characterTransform.position = (Vector3) { 0, 0, 0 };
    characterTransform.scale = (Vector3) { 10, 10, 10 };
    characterTransform.rotation = QuaternionIdentity();

    Input_GetMousePosition(&mouse_x, &mouse_y);

    for (int i = 0; i < CAR_COUNT; ++i)
    {
        Car* car = &cars[i];
        Transform* t = &car->transform;
        t->scale = (Vector3) { .05f, .05f, .05f };
        t->position.y = -10;
        t->position.x = -100 + 200 * (float)(i % 10) / 10;
        t->position.z = -100 + 200 * (float)(i / 10) / 10;

        car->id = RandomInt(0, 2);

        t->rotation = QuaternionFromEuler(0, RandomFloat(0, 360), 0);
        if (car->id == 0)
        {
            car->localRotation = QuaternionFromEuler(90, 0, 0);
        }
        else
        {
            car->localRotation = QuaternionFromEuler(90, 0, 180);
        }
    }

    shaderID = Shader_Load("shaders_gl/vert.shader", "shaders_gl/frag.shader");

    mainTexProp = Shader_GetPropertyID(shaderID, "_MainTex");
}

void Update(float deltaTime)
{
    frames++;

    fpsTime += deltaTime;
    if (fpsTime >= 1.0f)
    {
        fps = (float)frames / fpsTime;
        DebugFormat("fps: %f", fps);
        fpsTime -= 1.0f;
        frames = 0;
    }

    x += deltaTime * 90;
    z += 0.1f;

    float prev_x = mouse_x;
    float prev_y = mouse_y;
    Input_GetMousePosition(&mouse_x, &mouse_y);

    Vector3 move = { 0 };

    if (Input_GetButtonState((int)'W'))
        move.z -= 1;
    if (Input_GetButtonState((int)'S'))
        move.z += 1;
    if (Input_GetButtonState((int)'A'))
        move.x -= 1;
    if (Input_GetButtonState((int)'D'))
        move.x += 1;

    move.x *= 100 * deltaTime;
    move.z *= 100 * deltaTime;

    if (Input_GetButtonState(0x01))
    {
        Vector3 rotate = { 0 };
        rotate.x = .2f * (mouse_y - prev_y);
        rotate.y = .2f * (mouse_x - prev_x);

        cameraTransform.rotation = QuaternionMultiply(
            cameraTransform.rotation,
            QuaternionFromEuler(0, rotate.y, 0));
        cameraTransform.rotation = QuaternionMultiply(
            QuaternionFromEuler(rotate.x, 0, 0),
            cameraTransform.rotation);
    }

    Quaternion rot = cameraTransform.rotation;
    move = Vector3RotateByQuaternion(move, rot);

    cameraTransform.position = Vector3Add(
        cameraTransform.position,
        move);


    for (int i = 0; i < CAR_COUNT; ++i)
    {
        Car* car = &cars[i];
        Transform* t = &car->transform;

        float rotate = deltaTime * 40 * (1.0f + 1.0f / (1 + i));
        t->rotation = QuaternionMultiply(t->rotation, QuaternionFromEuler(0, rotate, 0));
        t->position = Vector3Add(t->position, Vector3RotateByQuaternion((Vector3) { 0, 0, deltaTime * 10 }, t->rotation));
    }
}

void Draw()
{
    Shader_SetViewMatrix(cameraTransform);

    Shader_Use(shaderID);

    for (int i = 0; i < CAR_COUNT; ++i)
    {
        Car* car = &cars[i];
    
        Quaternion rotation = car->transform.rotation;
    
        car->transform.rotation = QuaternionMultiply(car->localRotation, rotation);
    
        if (car->id == 0)
        {
            Shader_SetTextureID(mainTexProp, car1tex);
            Draw_Mesh(car1mesh, shaderID, car->transform);
        }
        else
        {
            Shader_SetTextureID(mainTexProp, car2tex);
            Draw_Mesh(car2mesh, shaderID, car->transform);
        }
    
        car->transform.rotation = rotation;
    }
    
    Shader_SetTextureID(mainTexProp, grassTex);
    Draw_Mesh(groundMesh, shaderID, groundTransform);

    Shader_SetTextureID(mainTexProp, characterTex);
    Draw_Mesh(characterMesh, shaderID, characterTransform);

    Draw_Text(10, 10, FormatString("FPS: %i", (int)(fps + 0.5f)));
}
