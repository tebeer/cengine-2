#ifndef _MATERIAL_H_
#define _MATERIAL_H_

int Material_Create(const char* source);
void Material_Destroy(int id);

#endif