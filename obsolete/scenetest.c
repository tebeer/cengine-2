#include "../engine/main.h"
#include "../engine/engine.c"

typedef struct
{
    float fpsTime;
    int frames;
    float fps;
} Clock;

struct Memory_t
{
    Clock clock;

    InputState prevInput;

    Transform cameraTransform;

    Scene scene;
};

typedef struct Memory_t Memory;

void User_Init(AppContext* ctx)
{
    Memory* data = (Memory*)ctx->fixedMemory;

    //cameraTransform.position = (Vector3) { -59, 22, 94 };
    //cameraTransform.rotation = QuaternionFromEuler(10, 135, 0);
    //Roster_Debug();
    Scene scene = data->scene = Scene_Load("plaza/scene.txt");;

    Vector3 avg = Vector3Zero();
    for (unsigned int i = 0; i < scene.instanceCount; ++i)
    {
        avg = Vector3Add(avg, scene.instances.data[i].transform.position);
    }
    
    avg.x /= scene.instanceCount;
    avg.y /= scene.instanceCount;
    avg.z /= scene.instanceCount;

    data->cameraTransform.position = (Vector3) { avg.x + 0, avg.y + 10, avg.z -10 };
    data->cameraTransform.rotation = QuaternionFromEuler(45, 0, 0);
    data->cameraTransform.scale = (Vector3) { 1, 1, 1 };
}

void CalculateFps(Clock* clock, float deltaTime)
{
    clock->frames++;

    clock->fpsTime += deltaTime;
    if (clock->fpsTime >= 1.0f)
    {
        clock->fps = (float)clock->frames / clock->fpsTime;
        //DebugFormat("fps: %f", fps);
        clock->fpsTime -= 1.0f;
        clock->frames = 0;
    }
}

void User_Update(AppContext* ctx)
{
    GFX_Clear(ClearFlags_ColorDepth, (Color) { 0.0f, 0.0f, 0.0f, 0.0f });

    Memory* data = (Memory*)ctx->fixedMemory;

    float deltaTime = (float)ctx->deltaTime;
    CalculateFps(&data->clock, deltaTime);
    
    Vector3 move = { 0 };

    InputState input = ctx->input;

    if (input.buttons[(int)'W'])
        move.z += 1;
    if (input.buttons[(int)'S'])
        move.z -= 1;
    if (input.buttons[(int)'A'])
        move.x -= 1;
    if (input.buttons[(int)'D'])
        move.x += 1;

    move.x *= 10 * deltaTime;
    move.z *= 10 * deltaTime;

    if (input.buttons[0x01])
    {
        Vector3 rotate = { 0 };
        rotate.x = .2f * (input.mouse_y - data->prevInput.mouse_y);
        rotate.y = .2f * (input.mouse_x - data->prevInput.mouse_x);

        data->cameraTransform.rotation = QuaternionMultiply(
            QuaternionFromEuler(0, rotate.y, 0),
            data->cameraTransform.rotation
            );
        data->cameraTransform.rotation = QuaternionMultiply(
            data->cameraTransform.rotation,
            QuaternionFromEuler(rotate.x, 0, 0)
            );
    }

    Quaternion rot = data->cameraTransform.rotation;
    move = Vector3RotateByQuaternion(move, rot);

    data->cameraTransform.position = Vector3Add(
        data->cameraTransform.position,
        move);

    float rotspd = 90;
    //scene.instances.data[3].transform.rotation = QuaternionMultiply(
    //    scene.instances.data[3].transform.rotation,
    //    QuaternionFromEuler(deltaTime * rotspd, 0, 0)
    //    );
    //scene.instances[1].transform.rotation = QuaternionMultiply(
    //    QuaternionFromEuler(0, deltaTime * rotspd, 0),
    //    scene.instances[1].transform.rotation
    //    );
    //scene.instances[0].transform.rotation = QuaternionMultiply(
    //    QuaternionFromEuler(0, 0, deltaTime * rotspd),
    //    scene.instances[0].transform.rotation
    //    );

    data->prevInput = input;

    // Draw
    {
        Shader_SetViewMatrix(data->cameraTransform);

        Scene_Draw(&data->scene);

        //Shader_SetTextureID(mainTexProp, grassTex);
        //Draw_Mesh(groundMesh, shaderID, groundTransform);

        Shader_Use(-1);
        Texture_Unbind();

        Draw_Text(10, 10, FormatString("FPS: %i", (int)(data->clock.fps + 0.5f)));
        Draw_Text(10, 20, FormatString("Vertices: %i", GFX_GetVertexCount()));
    }

    //double t = Platform_GetTimeMs();
    //uint64 c = Platform_GetCPUCycles();
    //
    //for (int i = 0; i < 1; ++i)
    //{
    //    *(int*)(ctx->cacheMemory + 100) = i;
    //}
    //
    //t = Platform_GetTimeMs() - t;
    //uint64 c2 = Platform_GetCPUCycles();
    //DebugFormat("i %i", c2 - c);
    //DebugFormat("f %f", t);
}


