#include "../engine/main.h"
#include "../assets/test/model.h"

RenderState rs = { 0 };
Mesh mesh;

typedef struct ShaderMatrix_t
{
    Matrix worldMatrix;
    Matrix viewMatrix;
    Matrix projectionMatrix;
} ShaderMatrix;

ShaderMatrix matrices;

void User_Init(AppContext* ctx)
{
    String assetDB = UString("../assets/test/model.zstd");
    AssetDBHeader header = AssetDB_LoadHeader(assetDB);

    rs.texture = GFX_CreateTexture(AssetDB_LoadTexture(assetDB, header.assets[Asset_ak47_texture_png]));
    rs.shader = GFX_CreateShader(AssetDB_LoadShader(assetDB, header.assets[Asset_diffuse_shader]));
    rs.vertexLayout = GFX_CreateVertexShaderInputLayout(rs.shader, &(VertexLayout)
    {
        .elements =
        {
            {.name = UString("V_POSITION"),.type = VertexElementType_Float3, .bufferIndex = 0 },
            {.name = UString("V_UV"),.type = VertexElementType_Float2,.bufferIndex = 1 },
            {.name = UString("V_NORMAL"),.type = VertexElementType_Float3,.bufferIndex = 2 },
        },
        .elementCount = 3,
    });
    rs.constantBuffer = GFX_CreateShaderConstantBuffer(sizeof(ShaderMatrix));

    mesh = AssetDB_LoadMesh(assetDB, header.assets[Asset_ak47_fbx]);
    rs.indexBuffer = GFX_CreateBuffer(mesh.indexBuffer.data, mesh.indexBuffer.size, BufferType_Index);
    rs.vertexBuffers[0].buffer = GFX_CreateBuffer(mesh.vertexBuffer.data, mesh.vertexBuffer.size, BufferType_Vertex);
    rs.vertexBuffers[0].stride = 12;
    rs.vertexBuffers[1].buffer = GFX_CreateBuffer(mesh.uv1Buffer.data, mesh.uv1Buffer.size, BufferType_Vertex);
    rs.vertexBuffers[1].stride = 8;
    rs.vertexBuffers[2].buffer = GFX_CreateBuffer(mesh.normalBuffer.data, mesh.normalBuffer.size, BufferType_Vertex);
    rs.vertexBuffers[2].stride = 12;
    rs.vertexBufferCount = 3;

    int width, height;
    Platform_GetResolution(&width, &height);
    matrices.worldMatrix = MatrixIdentity();
    matrices.viewMatrix = MatrixInvert(MatrixTranslate(0, 0, 0));
    matrices.projectionMatrix = MatrixPerspective(DEG2RAD*65.0f, (double)width / height, 0.1f, 100.0f);
}

float rotate = 0;

void User_Update(AppContext* ctx)
{
    GFX_Clear(ClearFlags_ColorDepth, (Color){ 0.1f, 0.2f, 0.3f, 0.0f });
    
    rotate += (float)(DEG2RAD * 90 * ctx->deltaTime);
    matrices.worldMatrix = MatrixTranslate(0, 0, 4);
    matrices.worldMatrix = MatrixMultiply(matrices.worldMatrix, MatrixRotateY(rotate));
    matrices.worldMatrix = MatrixMultiply(matrices.worldMatrix, MatrixScale(0.05f, 0.05f, 0.05f));
    GFX_SetShaderConstantBuffer(rs.constantBuffer, ToBuffer(matrices));
    GFX_SetRenderState(&rs);
    GFX_DrawIndexed(mesh.indexCount, 0);
}
