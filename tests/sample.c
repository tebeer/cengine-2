#include "../engine/main.h"
#include "../assets/test/sample.h"

extern void Nim_User_Update(AppContext* ctx, RenderState* rs);
extern void Nim_User_Init(void);
extern void NimMain(void);

RenderState rs = { 0 };
typedef struct ShaderMatrix_t
{
    Matrix worldMatrix;
    Matrix viewMatrix;
    Matrix projectionMatrix;
} ShaderMatrix;

typedef struct GFXMesh_t
{
    uint32 indexCount;
    void* indexBuffer;
    VertexBuffer vertexBuffers[4];
    uint32 vertexBufferCount;
} GFXMesh;

ShaderMatrix matrices = {0};
GFXMesh ak47 = {0};
GFXMesh quad = {0};


GFXMesh GFXMesh_Create(const Mesh mesh)
{
    GFXMesh m = {0};

    ASSERT(mesh.vertexCount * sizeof(*mesh.vertices) == mesh.vertexBuffer.size, "GFXMesh_Create: VertexBuffer.Size != VertexCount");
    ASSERT(mesh.indexCount * sizeof(*mesh.indices) == mesh.indexBuffer.size, "GFXMesh_Create: IndexBuffer.Size != indexCount");

    m.indexCount = mesh.indexCount;
    m.indexBuffer = GFX_CreateBuffer(mesh.indexBuffer.data, mesh.indexBuffer.size, BufferType_Index);
    m.vertexBuffers[m.vertexBufferCount].buffer = GFX_CreateBuffer(mesh.vertexBuffer.data, mesh.vertexBuffer.size, BufferType_Vertex);
    m.vertexBuffers[m.vertexBufferCount].stride = sizeof(*mesh.vertices);
    m.vertexBufferCount++;

    if(mesh.uv1 !=0 )
    {
        m.vertexBuffers[m.vertexBufferCount].buffer = GFX_CreateBuffer(mesh.uv1Buffer.data, mesh.uv1Buffer.size, BufferType_Vertex);
        m.vertexBuffers[m.vertexBufferCount].stride = sizeof(*mesh.uv1);
        m.vertexBufferCount++;
    }

    if(mesh.normals !=0 )
    {
        m.vertexBuffers[m.vertexBufferCount].buffer = GFX_CreateBuffer(mesh.normalBuffer.data, mesh.normalBuffer.size, BufferType_Vertex);
        m.vertexBuffers[m.vertexBufferCount].stride = sizeof(*mesh.normals);
        m.vertexBufferCount++;
    }

    return m;
}

GFXMesh CreateQuadMesh()
{
    Vector3 vertices[] = {
            {-.5f, .5f, 0},
            { .5f, .5f, 0},
            { .5f,-.5f, 0},
            {-.5f,-.5f, 0},
        };

    uint16 indices[] = {
            0, 1, 2, 2, 3, 0
        };

    Mesh m = {
        .vertexBuffer.size = sizeof(vertices),
        .indexBuffer.size = sizeof(indices),
        .vertexBuffer.data = vertices,
        .indexBuffer.data = indices,
        .vertexCount =  ARRAY_SIZE(vertices),
        .indexCount =  ARRAY_SIZE(indices),
    };

    return GFXMesh_Create(m);
}

void User_Init(AppContext* ctx)
{
    NimMain();
    Nim_User_Init();

    String assetDB = UString("../assets/test/sample.zstd");
    AssetDBHeader header = AssetDB_LoadHeader(assetDB);

    //rs.texture = GFX_CreateTexture(AssetDB_LoadTexture(assetDB, header.assets[Asset_ak47_texture_png]));
    rs.shader = GFX_CreateShader(AssetDB_LoadShader(assetDB, header.assets[Asset_diffuse_shader]));
    rs.vertexLayout = GFX_CreateVertexShaderInputLayout(rs.shader, &(VertexLayout)
    {
        .elements =
        {
            {.name = UString("V_POSITION"),.type = VertexElementType_Float3, .bufferIndex = 0 },
            {.name = UString("V_UV"),.type = VertexElementType_Float2,.bufferIndex = 1 },
            {.name = UString("V_NORMAL"),.type = VertexElementType_Float3,.bufferIndex = 2 },
        },
        .elementCount = 3,
    });
    rs.constantBuffer = GFX_CreateShaderConstantBuffer(sizeof(ShaderMatrix));

    Mesh ak47Mesh = AssetDB_LoadMesh(assetDB, header.assets[Asset_ak47_fbx]);

    ak47 = GFXMesh_Create(ak47Mesh);
    
    Mesh cubeMesh = AssetDB_LoadMesh(assetDB, header.assets[Asset_cube_fbx]);
    //quad = GFXMesh_Create(cubeMesh);
    quad = CreateQuadMesh();

 
    int width, height;
    Platform_GetResolution(&width, &height);
    matrices.worldMatrix = MatrixIdentity();
    matrices.viewMatrix = MatrixInvert(MatrixTranslate(0, 0, 0));
    matrices.projectionMatrix = MatrixPerspective(DEG2RAD*65.0f, (double)width / height, 0.1f, 100.0f);
}

void SetMesh(RenderState* rs, const GFXMesh mesh)
{
    rs->indexBuffer = mesh.indexBuffer;
    rs->vertexBufferCount = mesh.vertexBufferCount;
    for(uint32 i = 0; i < mesh.vertexBufferCount; ++i)
        rs->vertexBuffers[i] = mesh.vertexBuffers[i];
}

void DrawMesh(const GFXMesh mesh)
{
    SetMesh(&rs, mesh);
    GFX_SetRenderState(&rs);
    GFX_DrawIndexed(mesh.indexCount, 0);
}

float rotate = 0;
void User_Update(AppContext* ctx)
{
//    GFX_Clear(ClearFlags_ColorDepth, (Color){ 0.1f, 0.2f, 0.3f, 0.0f });
//
//    rotate += (float)(DEG2RAD * 90 * ctx->deltaTime);
//    matrices.worldMatrix = MatrixTranslate(2, 0, 4);
//    matrices.worldMatrix = MatrixMultiply(matrices.worldMatrix, MatrixRotateY(rotate));
//    matrices.worldMatrix = MatrixMultiply(matrices.worldMatrix, MatrixScale(0.05f, 0.05f, 0.05f));
//    GFX_SetShaderConstantBuffer(rs.constantBuffer, ToBuffer(matrices));
//    DrawMesh(ak47);
//
//    matrices.worldMatrix = MatrixTranslate(-1, 0, 4);
//    matrices.worldMatrix = MatrixMultiply(matrices.worldMatrix, MatrixRotateY(rotate));
//    matrices.worldMatrix = MatrixMultiply(matrices.worldMatrix, MatrixScale(1, 1, 1));
//
//    GFX_SetShaderConstantBuffer(rs.constantBuffer, ToBuffer(matrices));
//    DrawMesh(quad);

    Nim_User_Update(ctx, &rs);
}
