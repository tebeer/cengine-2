import math
import ../engine/cengine
import ../engine/math2

type
    CBuffer {.bycopy.} = object 
        worldMatrix: Matrix
        viewMatrix: Matrix
        projectionMatrix: Matrix
type
    GFXMesh = object
        indexCount: uint32
        indexBuffer: pointer
        vertexBuffers: array[4, VertexBuffer]
        vertexBufferCount: uint32

proc GFXMesh_Create(mesh: Mesh): GFXMesh =
    var m: GFXMesh

    #ASSERT(mesh.vertexCount * sizeof(*mesh.vertices) == mesh.vertexBuffer.size, "GFXMesh_Create: VertexBuffer.Size != VertexCount");
    #ASSERT(mesh.indexCount * sizeof(*mesh.indices) == mesh.indexBuffer.size, "GFXMesh_Create: IndexBuffer.Size != indexCount");

    m.indexCount = mesh.indexCount
    m.indexBuffer = GFX_CreateBuffer(mesh.indexBuffer.data, mesh.indexBuffer.size, BufferType.Index)
    m.vertexBuffers[m.vertexBufferCount].buffer = GFX_CreateBuffer(mesh.vertexBuffer.data, mesh.vertexBuffer.size, BufferType.Vertex);
    m.vertexBuffers[m.vertexBufferCount].stride = uint32(sizeof(Vector3));
    m.vertexBufferCount += 1

    if mesh.uv1Buffer.data != nil:
        m.vertexBuffers[m.vertexBufferCount].buffer = GFX_CreateBuffer(mesh.uv1Buffer.data, mesh.uv1Buffer.size, BufferType.Vertex);
        m.vertexBuffers[m.vertexBufferCount].stride = uint32(sizeof(Vector2));
        m.vertexBufferCount += 1;

    if mesh.normalBuffer.data != nil:
        m.vertexBuffers[m.vertexBufferCount].buffer = GFX_CreateBuffer(mesh.normalBuffer.data, mesh.normalBuffer.size, BufferType.Vertex);
        m.vertexBuffers[m.vertexBufferCount].stride = uint32(sizeof(Vector3));
        m.vertexBufferCount += 1;

    return m;

proc CreateQuadMesh(): GFXMesh = 

    var vertices = [
        Vector3(x: -0.5, y:  0.5, z: 0),
        Vector3(x:  0.5, y:  0.5, z: 0),
        Vector3(x:  0.5, y: -0.5, z: 0),
        Vector3(x: -0.5, y: -0.5, z: 0),
    ]

    var indices = [0'u16, 1, 2, 2, 3, 0]

    var m:Mesh = Mesh(
        vertexBuffer: Buffer(size: sizeof(vertices).uint32, data: vertices.unsafeAddr),
        indexBuffer: Buffer(size: sizeof(indices).uint32, data: indices.unsafeAddr),
        vertexCount: vertices.len.uint16,
        indexCount: indices.len.uint16,
    )

    result = GFXMesh_Create(m)

proc SetMesh(rs: var RenderState, mesh: GFXMesh) =
    rs.indexBuffer = mesh.indexBuffer;
    rs.vertexBufferCount = mesh.vertexBufferCount;
    for i in 0..<mesh.vertexBufferCount:
        rs.vertexBuffers[i] = mesh.vertexBuffers[i];
    
var rotate:float = 0
var matrices:CBuffer
var ak47Mesh:GFXMesh    
var ak47Texture:DataHandle    
var quadMesh:GFXMesh    
var cameraPos:Vector3
var cameraRot:Vector3

var mouse_x_prev, mouse_y_prev:float = -1

proc Nim_User_Init() {.cdecl, exportc} =

    echo "Nim_User_Init"

    var db = AssetDB_Load("../assets/test/sample.zstd")
    var ak47MeshAsset = db.LoadMesh(0)
    ak47Mesh = GFXMesh_Create(ak47MeshAsset)
    #quad = CreateQuadMesh()

    var ak47TextureAsset = db.LoadTexture(1)
    ak47Texture = GFX_CreateTexture(ak47TextureAsset)

    cameraPos.z = -10
    #matrices.viewMatrix = MatrixInvert(MatrixTranslate(0, 0, -10));
    matrices.projectionMatrix = MatrixPerspective(degToRad(65.0), 16.0 / 9.0, 0.1, 100.0);


proc Nim_User_Update(ctx: AppContext, rs: var RenderState) {.cdecl, exportc} =

    GFX_Clear(ClearFlags.ColorAndDepth, Color(r : 0.1, g : 0.4, b : 0.3))

    var mouse_dx = ctx.input.mouse_x - mouse_x_prev
    var mouse_dy = ctx.input.mouse_y - mouse_y_prev

    if mouse_x_prev > 0:
        cameraRot.x += 0.2*(mouse_dy)
        cameraRot.y += 0.2*(mouse_dx)

    mouse_x_prev = ctx.input.mouse_x
    mouse_y_prev = ctx.input.mouse_y

    var move:Vector3

    if ctx.input.buttons[ord('A')] == 1:
        move.x -= 1
    if ctx.input.buttons[ord('D')] == 1:
        move.x += 1
    if ctx.input.buttons[ord('W')] == 1:
        move.z += 1
    if ctx.input.buttons[ord('S')] == 1:
        move.z -= 1

    var cameraRotQ = QuaternionFromEuler(cameraRot.x, cameraRot.y, cameraRot.z)
#    var cameraRotMatrix = MatrixRotateX(cameraRot.x) * MatrixRotateY(cameraRot.y)

    cameraPos = cameraPos + cameraRotQ * move * ctx.deltaTime * 5
    
    matrices.viewMatrix = MatrixInvert(MatrixTranslate(cameraPos.x, cameraPos.y, cameraPos.z) * QuaternionToMatrix(cameraRotQ));

    rotate += degToRad(90 * ctx.deltaTime);
    matrices.worldMatrix = MatrixTranslate(0, 0, 0) * MatrixRotateY(rotate) * MatrixScale(0.05, 0.05, 0.05)

    rs.texture = ak47Texture

    GFX_SetShaderConstantBuffer(rs.constantBuffer, matrices)
    SetMesh(rs, ak47Mesh)
    GFX_SetRenderState(addr rs)
    GFX_DrawIndexed(ak47Mesh.indexCount, 0);


