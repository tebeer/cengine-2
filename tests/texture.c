#include "../engine/main.h"
#include "../assets/test/texture.h"

typedef struct TVertex_t
{
    Vector3 pos;
    Vector2 uv;
} TVertex;

RenderState rs = { 0 };

void User_Init(AppContext* ctx)
{
    String assetDB = UString("../assets/test/texture.zstd");

    AssetDBHeader header = AssetDB_LoadHeader(assetDB);

    rs.shader = GFX_CreateShader(AssetDB_LoadShader(assetDB, header.assets[Asset_texture_shader]));

    unsigned short indices[6] = { 0, 1, 2, 2, 3, 0 };
    TVertex vertices[] = {
        {.pos = {-0.5f, 0.5f, 0.5f },.uv = { 0.0f, 0.0f } },
        {.pos = { 0.5f, 0.5f, 0.5f },.uv = { 1.0f, 0.0f } },
        {.pos = { 0.5f,-0.5f, 0.5f },.uv = { 1.0f, 1.0f } },
        {.pos = {-0.5f,-0.5f, 0.5f },.uv = { 0.0f, 1.0f } },
    };

    rs.indexBuffer = GFX_CreateBuffer(indices, sizeof(unsigned short) * 6, BufferType_Index);
    rs.vertexBuffers[0].buffer = GFX_CreateBuffer(vertices, sizeof(TVertex) * 4, BufferType_Vertex);
    rs.vertexBuffers[0].stride = sizeof(TVertex);
    rs.vertexBufferCount = 1;

    rs.vertexLayout = GFX_CreateVertexShaderInputLayout(rs.shader, &(VertexLayout)
    {
        .elements =
        {
            {.name = UString("V_POSITION"),.type = VertexElementType_Float3 },
            {.name = UString("V_UV"),.type = VertexElementType_Float2 },
        },
        .elementCount = 2,
    });

    rs.texture = GFX_CreateTexture(AssetDB_LoadTexture(assetDB, header.assets[Asset_texture_png]));
}

void User_Update(AppContext* ctx)
{
    GFX_Clear(ClearFlags_ColorDepth, (Color){ 0.1f, 0.2f, 0.3f, 0.0f });
    GFX_SetRenderState(&rs);
    GFX_DrawIndexed(6, 0);
}
