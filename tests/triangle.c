#include "../engine/main.h"
#include "../assets/test/triangle.h"

typedef struct TVertex_t
{
    Vector3 pos;
    Color color;
} TVertex;

RenderState rs = { 0 };

void User_Init(AppContext* ctx)
{
    STATIC_ASSERT_STMT(sizeof(size_t) == 8);

    String assetDB = UString("../assets/test/triangle.zstd");

    AssetDBHeader header = AssetDB_LoadHeader(assetDB);

    rs.shader = GFX_CreateShader(AssetDB_LoadShader(assetDB, header.assets[Asset_vertexcolor_shader]));

    unsigned short indices[6] = { 0, 1, 2 };
    TVertex vertices[] = {
        {.pos = {-0.5f,-0.5f, 0.5f },.color = { 1.0f, 0.0f, 0.0f, 1.0f } },
        {.pos = { 0.0f, 0.5f, 0.5f },.color = { 0.0f, 1.0f, 0.0f, 1.0f } },
        {.pos = { 0.5f,-0.5f, 0.5f },.color = { 0.0f, 0.0f, 1.0f, 1.0f } },
    };

    rs.indexBuffer = GFX_CreateBuffer(indices, sizeof(unsigned short) * 3, BufferType_Index);
    rs.vertexBuffers[0].buffer = GFX_CreateBuffer(vertices, sizeof(TVertex) * 3, BufferType_Vertex);
    rs.vertexBuffers[0].stride = sizeof(TVertex);
    rs.vertexBufferCount = 1;

    rs.vertexLayout = GFX_CreateVertexShaderInputLayout(rs.shader, &(VertexLayout)
    {
        .elements =
        {
            {.name = UString("V_POSITION"),.type = VertexElementType_Float3 },
            {.name = UString("V_COLOR"),.type = VertexElementType_Float4 },
        },
        .elementCount = 2,
    });   
}

void User_Update(AppContext* ctx)
{
    GFX_Clear(ClearFlags_ColorDepth, (Color){ 0.1f, 0.2f, 0.3f, 0.0f });
    GFX_SetRenderState(&rs);
    GFX_DrawIndexed(3, 0);
}
